import jwt
from config import config
from werkzeug.exceptions import HTTPException, NotFound
from helpers.db.user import User
from helpers.myexceptions import *
from datetime import datetime

class PostHandler:
    def __init__(self):
        self.m_urlMap = None

    def handle_request(self, o_request):
        if self.m_urlMap:
            o_adapter = self.m_urlMap.bind_to_environ(o_request.environ)
            try:
                o_session = o_request.environ['beaker.session']
                s_endpoint, values = o_adapter.match()
                return getattr(self, f'on_{s_endpoint}')(o_request, o_session, **values)
            except HTTPException as e:
                return e

    def getUserFromReq(self, o_request, b_throwException = False):
        s_token = self.getTokenFromRequest(o_request)
        if s_token:
            o_user = self.getUserFromToken(s_token)
            return o_user
        if b_throwException == True:
            raise NotLoggedInException
        return None

    def getTokenFromRequest(self, o_request):
        if not 'Authorization' in o_request.headers:
            return None
        s_auth = o_request.headers['Authorization']
        if s_auth:
            s_token = s_auth.split()[1]
            return s_token
        return None

    def getUserFromToken(self, s_token):
        o_user = jwt.decode(s_token, config['secret'], algorithms=["HS256"])
        if o_user:
            return User.get_user(o_user['email'])
        return None

    def refineDbResult(self, ao_dbRes):
        for o_row in ao_dbRes:
            self.refineDbResultEach( o_row)
        return ao_dbRes

    def refineDbResultEach(self, o_row):
        for s_key in o_row.keys():
            o_field = o_row[s_key]
            if isinstance(o_field, datetime):
                o_row[s_key] = o_field.strftime('%Y-%m-%d %H:%M:%S')
        return o_row

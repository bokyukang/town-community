import requests
from osmtogeojson import osmtogeojson

#api = overpass.API(endpoint="http://localhost:1001/api/interpreter", timeout=600)
s_url="http://localhost:1001/api/interpreter"
s_query = '''
        [out:json];
        relation(37.4999209,126.9494247,37.6289158,127.0386887)[boundary=administrative][name='서울'];
        (._;>;);
        out geom;
        '''

o_res = requests.get(s_url, params={'data': s_query})

o_geojson = osmtogeojson.process_osm_json(o_res.json())

print(type(o_geojson))

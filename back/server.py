import os
import urllib.parse
from http.server import BaseHTTPRequestHandler, HTTPServer
from managers.db_manager import DbManager
from managers.log_manager import LogManager
import cgi
import time
import json
import sys
import requests
import re
from handlers.map_handler import MapHandler
from handlers.account_handler import AccountHandler
from handlers.forum_handler import ForumHandler
from handlers.town_handler import TownHandler
from handlers.chat_handler import ChatHandler
from handlers.friends_handler import FriendsHandler
from handlers.event_handler import EventHandler
from beaker.middleware import SessionMiddleware
from handlers.common_handler import CommonHandler
from handlers.note_message_handler import NoteMessageHandler

from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.middleware.shared_data import SharedDataMiddleware
from werkzeug.utils import redirect
from jinja2 import Environment, FileSystemLoader
from mysocket.socket_manager import SocketManager
import multiprocessing
from helpers.myexceptions import *


o_LM = LogManager()
o_LM.Init('server')

hostName = "0.0.0.0"


class Server(object):

    
    def __init__(self, o_config):
        s_templatePath = os.path.join(os.path.dirname(__file__), 'templates')
        self.o_jinjaEnv = Environment(loader=FileSystemLoader(s_templatePath),
                                        autoescape=True)

        self.m_handlers = {
            'account' : AccountHandler(),
            'map' : MapHandler.instance(),
            'forum' : ForumHandler(),
            'town' : TownHandler(),
            'chat' : ChatHandler(),
            'friends' : FriendsHandler(),
            'event' : EventHandler(),
            'common' : CommonHandler(),
            'note_message' : NoteMessageHandler(),
        }

    #def on_get_boundaries(self, o_request):
    #    print('get_boundaries', o_request.form)

    #def on_check_signed_in(self, o_request):
    #    print('check signed in', o_request.form)


    #def render_template(self, s_templateName, **o_context):
    #    o_t = self.o_jinjaEnv.get_template(s_templateName)
    #    return Response(o_t.render(o_context), mimetype='text/html')

    def dispatch_request(self, o_request):
        #print('request.base_url', o_request.base_url)
        #print('request.path', o_request.path)
        #print('request.form', o_request.form)
        #print('request.json', o_request.get_json() )
        #print('request.files', o_request.files )
        #print('request.headers', o_request.headers)


        a_pathParts = o_request.path.split('/')
        if len(a_pathParts) >= 3:
            s_handler = a_pathParts[2]

            if s_handler in self.m_handlers:
                return self.m_handlers[s_handler].handle_request(o_request)
        return None

    def wsgi_app(self, environ, start_response):

        o_request = Request(environ)
        try:
            o_response = self.dispatch_request(o_request)
        except NotLoggedInException:
            # user is not logged in
            o_response = Response(None, status=401)

        if o_response:
            return o_response(environ, start_response)

        # not found
        return Response(status=404)(environ, start_response)

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)


o_socketThread = None
def startSocket():
    global o_socketManager, o_socketThread
    print('starting socket')
    o_socketManager = SocketManager()
    if o_socketThread is None:
        o_socketThread = multiprocessing.Process(target=o_socketManager.run, args=())
        o_socketThread.start()
    #self.m_socketManager.run()

def create_app(b_withStatic=True, b_withSession=True):
    print('on create app')

    o_app = Server({})


    if b_withStatic:
        o_app.wsgi_app = SharedDataMiddleware(o_app.wsgi_app, {
            '/static': os.path.join(os.path.dirname(__file__), 'static')
        })

    if b_withSession:
        session_opts = {
            'session.data_dir': '/tmp/cache/data/town',
            'session.lock_dir': '/tmp/cache/lock/town',
            'session.type': 'file',
            'session.cookie_expires': True,
            'session.auto':True,
        }

        o_app.wsgi_app = SessionMiddleware(o_app.wsgi_app, session_opts)

    return o_app

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('usage: python server.py [port]')
        exit(1)
    from werkzeug.serving import run_simple
    o_app = create_app()
    run_simple('0.0.0.0', int(sys.argv[1]), o_app, use_debugger=True, use_reloader=True, threaded=True)

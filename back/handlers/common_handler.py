
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import re
import json 

from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.middleware.shared_data import SharedDataMiddleware
from werkzeug.utils import redirect
from jinja2 import Environment, FileSystemLoader

from post_handler import PostHandler
from managers.file_manager import FileManager
from managers.session_manager import SessionManager
from helpers.db.user_variables import UserVariables
from helpers.db.participants import Participants
from helpers.db.scores import Scores

class CommonHandler(PostHandler):
    def __init__(self):
        self.m_urlMap = Map([
            Rule('/api/common/upload_img', endpoint='upload_img'),
            Rule('/api/common/del_img', endpoint='del_img'),
            Rule('/api/common/check_user_variable', endpoint='check_user_variable'),
            Rule('/api/common/decr_user_variable', endpoint='decr_user_variable'),
            Rule('/api/common/set_user_variable', endpoint='set_user_variable'),
            Rule('/api/common/get_participants', endpoint='get_participants'),
            Rule('/api/common/participate', endpoint='participate'),
            Rule('/api/common/unparticipate', endpoint='unparticipate'),
            Rule('/api/common/get_score', endpoint='get_score'),
            ])

    def on_upload_img(self, o_request, o_session):
        print('o_request', o_request.values['type'])
        a_fileNames = FileManager.instance().handle_save_req(o_request, 'uploaded_imgs/' + o_request.values['type'])
        return Response(json.dumps(a_fileNames), status=200)

    def on_del_img(self, o_request, o_session):
        s_file = o_request.values['file']
        a_folders = ['uploaded_imgs', 'profile_imgs']
        b_proceed = False
        for s_folder in a_folders:
            if re.search('^static/'+s_folder + '/', s_file):
                b_proceed = True

        if b_proceed:
            FileManager.instance().delete_file(o_request, 'uploaded_imgs/' + s_file)
            return Response(json.dumps(True), status=200)
        return Response(json.dumps(False), status=500)

    def on_check_user_variable(self, o_request, o_session):
        m_args = o_request.get_json()
        o_user = self.getUserFromReq(o_request)
        if o_user is not None:
            b_result = UserVariables.check_val(o_user['id'], m_args['name'], m_args['val'])
        else:
            b_result = None
        return Response(json.dumps(b_result), status=200)

    def on_set_user_variable(self, o_request, o_session):
        m_args = o_request.get_json()
        o_user = self.getUserFromReq(o_request)
        b_result = UserVariables.set_val(o_user['id'], m_args['name'], m_args['val'])
        return Response(json.dumps(b_result), status=200)

    def on_decr_user_variable(self, o_request, o_session):
        m_args = o_request.get_json()
        o_user = self.getUserFromReq(o_request)
        b_result = UserVariables.decr_val(o_user['id'], m_args['name'], m_args['initial_val'])
        return Response(json.dumps(b_result), status=200)

    def on_participate(self, o_request, o_session):
        m_args = o_request.get_json()
        o_user = self.getUserFromReq(o_request)
        b_result = Participants.add_participant( o_user['id'], m_args['type'], m_args['target_id'] )
        return Response(json.dumps(b_result), status=200)

    def on_unparticipate(self, o_request, o_session):
        m_args = o_request.get_json()
        o_user = self.getUserFromReq(o_request)
        b_result = Participants.remove_participant( o_user['id'], m_args['type'], m_args['target_id'] )
        return Response(json.dumps(b_result), status=200)

    def on_get_participants(self, o_request, o_session):
        m_args = o_request.get_json()
        o_user = self.getUserFromReq(o_request)
        ao_result = self.refineDbResult(Participants.get_participants( m_args['type'], m_args['target_id'] ))
        return Response(json.dumps(ao_result), status=200)

    def on_get_score(self, o_request, o_session):
        m_args = o_request.get_json()
        d_score = Scores.get_score( m_args.get('type'), m_args.get('target_id'), Scores.get_base(m_args.get('type')))
        return Response(json.dumps(d_score), status=200)


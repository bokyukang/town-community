
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import json 

from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.middleware.shared_data import SharedDataMiddleware
from werkzeug.utils import redirect
from jinja2 import Environment, FileSystemLoader

from post_handler import PostHandler
from managers.file_manager import FileManager
from helpers.db.forum_articles import ForumArticles
from helpers.db.forum_list import ForumList
from helpers.db.chat_list import ChatList
from helpers.db.towns import Towns
from helpers.db.town_chats import TownChats
from managers.db_manager import DbManager

class TownHandler(PostHandler):
    def __init__(self):
        self.m_urlMap = Map([
            Rule('/api/town/on_load', endpoint='load'),
            Rule('/api/town/on_ilivehere', endpoint='ilivehere'),
            ])

    #                   #
    # handler functions # 
    #                   #

    def on_load(self, o_request, o_session):
        o_json = o_request.get_json()
        print('on_load', o_json)
        
        o_properties = o_json['properties']

        #d_sig_cd = o_properties['SIG_CD']
        #s_eng_nm = o_properties['SIG_ENG_NM']
        #s_kor_nm = o_properties['SIG_KOR_NM']

        #a_fields = ['ID_0', 'COUNTRY', 'NAME_1', 'NL_NAME_1', 'ID_2', 'NAME_2', 'NL_NAME_2', 'TYPE_2', 'ENGTYPE_2']
        a_fields = ['SIG_CD','SIG_ENG_NM','SIG_KOR_NM']

        d_town_id = Towns.create_if_not(a_fields, o_properties)

        ForumList.get_default_forum('town', d_town_id, True)
        a_town_forums = ForumList.get_town_forums(d_town_id)

        TownChats.get_default_town_chat(d_town_id)
        a_town_chats = ChatList.get_town_chats(d_town_id)

        return Response(json.dumps(
            { 'town_id': d_town_id, 
                'town_forums' : a_town_forums,
                'town_chats' : a_town_chats
                } ), status=200, content_type='application/json')

        #return Response(json.dumps({ 'id': d_town_id}), status=200)

    def on_ilivehere(self, o_request, o_session):
        o_json = o_request.get_json()
        d_townId = o_json['town_id']

        # update
        s_sql = 'update users set town_id = %s'
        DbManager.instance().Execute(s_sql, (d_townId,))
        return Response(status=200, content_type='application/json')


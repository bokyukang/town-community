import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import json 

from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.middleware.shared_data import SharedDataMiddleware
from werkzeug.utils import redirect
from jinja2 import Environment, FileSystemLoader

from post_handler import PostHandler
from managers.file_manager import FileManager
from helpers.db.forum_articles import ForumArticles
from helpers.db.forum_list import ForumList
from helpers.db.towns import Towns
from managers.session_manager import SessionManager
from helpers.db.chat_list import ChatList

class ChatHandler(PostHandler):
    def __init__(self):
        self.m_urlMap = Map([
            Rule('/api/chat/upload_img', endpoint='upload_img'),
            Rule('/api/chat/create_chatroom', endpoint='create_chatroom'),
            Rule('/api/chat/get_chatrooms', endpoint='get_chatrooms'),
            Rule('/api/chat/get_default_chatroom', endpoint='get_default_chatroom'),
            ])



    #                   #
    # handler functions # 
    #                   #

    def on_upload_img(self, o_request, o_session):
        a_fileNames = FileManager.instance().handle_save_req(o_request)
        return Response(json.dumps(a_fileNames), status=200)

    def on_create_chatroom(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        print('o_json', o_json);
        ChatList.create_new(
                o_json.get('title'), 
                o_json.get('type'),
                o_json.get('type_id'),
                o_json.get('tag'),
                o_user.get('id'),
                **o_json)
        return Response('', status=200)

    def on_get_chatrooms(self, o_request, o_session):
        o_json = o_request.get_json()
        ao_chatRooms = ChatList.get_chatrooms(
                o_json.get('type'),
                o_json.get('type_id'),
                o_json.get('tag'),
                False
                )
        return Response(json.dumps(ao_chatRooms), status=200)

    def on_get_default_chatroom(self, o_request, o_session):
        o_json = o_request.get_json()
        ao_chatRooms = ChatList.get_chatrooms(
                o_json.get('type'),
                o_json.get('type_id'),
                'default',
                True
                )
        return Response(json.dumps(ao_chatRooms[0]), status=200)


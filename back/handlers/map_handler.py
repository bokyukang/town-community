
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from osmtogeojson import osmtogeojson
from managers.db_manager import DbManager
from managers.log_manager import LogManager
from helpers.boundary_helper import BoundaryHelper
from post_handler import PostHandler
import re

o_LM = LogManager()
o_LM.Init('map')


class MapHandler(PostHandler):

    _instance = None

    @staticmethod
    def instance():
        if MapHandler._instance is None:
            MapHandler._instance = MapHandler()
        return MapHandler._instance

    def do_POST(self, s_path, m_postvars, o_result, o_session, a_responseHeaders):
        if s_path == '/map/boundaries':
            o_result['boundaries'] = BoundaryHelper.instance().getBoundaries(m_postvars['bounds'], m_postvars['zoom_level'])
            return '200 OK'


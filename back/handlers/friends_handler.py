import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.middleware.shared_data import SharedDataMiddleware
from werkzeug.utils import redirect
from jinja2 import Environment, FileSystemLoader
from managers.db_manager import DbManager
from managers.log_manager import LogManager

from post_handler import PostHandler
import re

import json 
from helpers.db.friends import Friends
from helpers.db.behaviors import Behaviors

class FriendsHandler(PostHandler):
    def __init__(self):
        self.m_urlMap = Map([
            Rule('/api/friends/request_friend', endpoint='request_friend'),
            Rule('/api/friends/get_status', endpoint='get_status'),
            Rule('/api/friends/accept_request', endpoint='accept_request'),
            Rule('/api/friends/unfriend', endpoint='unfriend'),
            Rule('/api/friends/cancel_request', endpoint='cancel_request'),
            Rule('/api/friends/get_requests', endpoint='get_requests'),
            Rule('/api/friends/get_voters', endpoint='get_voters'),
            Rule('/api/friends/get_myvoters', endpoint='get_myvoters'),
            Rule('/api/friends/deny_request', endpoint='deny_request'),
            Rule('/api/friends/get_friends', endpoint='get_friends'),
            ])

    # handler functions #

    def on_request_friend(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        b_result = Friends.request_friend(o_user['id'], o_json['friend_id'])
        if b_result:
            return Response('', status=200)
        return Response('duplicate', status=500)

    def on_get_status(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        s_status = Friends.get_status(o_user['id'], o_json['friend_id'])
        return Response(json.dumps(s_status), status=200)

    def on_accept_request(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        b_result = Friends.accept_request(o_user['id'], o_json['friend_id'])
        if b_result:
            return Response('', status=200)
        return Response('', status=500)

    def on_unfriend(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        b_return = Friends.unfriend(o_user['id'], o_json['friend_id'])
        if b_return:
            return Response('', status=200)
        return Response('', status=500)

    def on_cancel_request(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        b_return = Friends.cancel_request(o_user['id'], o_json['friend_id'])
        if b_return:
            return Response('', status=200)
        return Response('', status=500)

    def on_get_friends(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        if o_user:
            return Response(json.dumps(Friends.get_friends(o_user['id'])), status=200)
        return Response('', status=500)

    def on_get_requests(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        if o_user:
            return Response(json.dumps(Friends.get_requests(o_user['id'])), status=200)
        return Response('', status=500)

    def on_get_voters(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        if o_user:
            return Response(json.dumps(Behaviors.get_vals('user-vote', 'user_id', o_user['id'], None)), status=200)
        return Response(None, status=200)

    def on_get_myvoters(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        if o_user:
            return Response(json.dumps(Behaviors.get_vals('user-vote', 'target_id', None, o_user['id'])), status=200)
        return Response(None, status=200)

    def on_deny_request(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        b_return = Friends.deny_request(o_user['id'], o_json['friend_id'])
        if b_return:
            return Response('', status=200)
        return Response('', status=500)


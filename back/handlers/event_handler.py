import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.middleware.shared_data import SharedDataMiddleware
from werkzeug.utils import redirect
from jinja2 import Environment, FileSystemLoader
from managers.db_manager import DbManager
from managers.log_manager import LogManager

from post_handler import PostHandler
import re
import json 
from helpers.db.events import Events, Bounds
from helpers.general_helper import GeneralHelper

class EventHandler(PostHandler):
    def __init__(self):
        self.m_urlMap = Map([
            Rule('/api/event/add_event', endpoint='add_event'),
            Rule('/api/event/get_events', endpoint='get_events'),
            Rule('/api/event/get_myevents', endpoint='get_myevents'),
            Rule('/api/event/get_event', endpoint='get_event'),
            ])

    def on_add_event(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request, True)
        o_json = o_request.get_json()
        o_event = o_json['event']
        s_type = o_json.get('type')
        print(o_event)

        a_requiredFields = ['title', 'type', 'lat', 'lng', 'description']
        if s_type == 'meetup':
            a_requiredFields += ['at','address', ]
        elif s_type == 'debate':
            a_requiredFields += ['at', ]
        elif s_type == 'group':
            a_requiredFields += []
            
        for s_field in a_requiredFields:
            if s_field not in o_event or o_event[s_field] is None or str(o_event[s_field]).strip() == '':
                return Response('required field %s'%(s_field,), status=400)

        d_rowId = Events.create_new( o_event['title'], o_event['type'], o_user['id'], o_event['lat'], \
                o_event['lng'], o_event['at'], description=o_event['description'], \
                address=o_event['address'])
        return Response(json.dumps(d_rowId), status=200)

    def on_get_events(self, o_request, o_session):
        o_json = o_request.get_json()
        o_bounds = o_json['bounds']

        a_events = Events.get_events( Bounds( o_bounds['left'], o_bounds['bottom'], o_bounds['right'], o_bounds['top'] ), o_json['types'], o_json.get('dateFrom'), o_json.get('dateTo'))
        return Response(json.dumps(a_events), status=200)

    def on_get_myevents(self, o_request, o_session):
        o_json = o_request.get_json()
        o_user = self.getUserFromReq(o_request, True)

        a_events = Events.get_events(None, None, GeneralHelper.getTodayStr(), None, user_id = o_user['id'] )
        return Response(json.dumps(a_events), status=200)

    def on_get_event(self, o_request, o_session):
        o_json = o_request.get_json()
        d_id = o_json['id']
        o_event = self.refineDbResultEach(Events.get_by_id(d_id))
        
        return Response(json.dumps(o_event), status=200)

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.middleware.shared_data import SharedDataMiddleware
from werkzeug.utils import redirect
from jinja2 import Environment, FileSystemLoader
from managers.db_manager import DbManager
from managers.log_manager import LogManager

from post_handler import PostHandler
import re

import json 
from helpers.db.note_message import NoteMessage

class NoteMessageHandler(PostHandler):
    def __init__(self):
        self.m_urlMap = Map([
            Rule('/api/note_message/send_msg', endpoint='send_msg'),
            Rule('/api/note_message/get_msgs', endpoint='get_msgs'),
            Rule('/api/note_message/get_msg', endpoint='get_msg'),
            ])

    def on_send_msg(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        NoteMessage.send_msg(o_user['id'], o_json['receiver_id'], o_json['content'])
        return Response('', status=200)

    def on_get_msgs(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        if o_user:
            o_json = o_request.get_json()
            a_msgs = NoteMessage.get_msgs(**o_json, receiver_id=o_user['id'])
            d_count = NoteMessage.get_msgs(**o_json, receiver_id=o_user['id'], count=True)
            return Response(json.dumps({ 'msgs': a_msgs, 'totalRecords': d_count}), status=200)
        return Response('', status=200)

    def on_get_msg(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        a_msgs = NoteMessage.get_msgs(**o_json, receiver_id=o_user['id'], shorten=False)

        if len(a_msgs) == 1:
            return Response(json.dumps(a_msgs[0]), status=200)
        return Response('', status=500)


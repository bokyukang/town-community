import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import random
from managers.db_manager import DbManager
from managers.log_manager import LogManager
from post_handler import PostHandler
from managers.email_manager import EmailManager
import re
from config import config
import json

from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.middleware.shared_data import SharedDataMiddleware
from werkzeug.utils import redirect
from jinja2 import Environment, FileSystemLoader

from managers.file_manager import FileManager
from managers.session_manager import SessionManager
from helpers.db.user_pics import UserPics
from helpers.db.user import User
from helpers.db.forum_articles import ForumArticles
import jwt
from helpers.myexceptions import *
from helpers.db.scores import Scores
from helpers.db.behaviors import Behaviors


o_LM = LogManager()
o_LM.Init('account')

class AccountHandler(PostHandler):

    def __init__(self):
        self.s_token = ''
        self.m_urlMap = Map([
                Rule('/api/account/signup', endpoint='sign_up'),
                Rule('/api/account/signin', endpoint='sign_in'),
                Rule('/api/account/signout', endpoint='sign_out'),
                Rule('/api/account/test', endpoint='test'),
                Rule('/api/account/validate_email', endpoint='validate_email'),
                Rule('/api/account/check_signed_in', endpoint='check_signed_in'),
                Rule('/api/account/reload_user', endpoint='reload_user'),
                Rule('/api/account/upload_img', endpoint='upload_img'),
                Rule('/api/account/get_profile_imgs', endpoint='get_profile_imgs'),
                Rule('/api/account/set_main_img', endpoint='set_main_img'),
                Rule('/api/account/del_img', endpoint='del_img'),
                Rule('/api/account/get_user_profile', endpoint='get_user_profile'),
                Rule('/api/account/get_my_profile', endpoint='get_my_profile'),
                Rule('/api/account/update_field', endpoint='update_field'),
                Rule('/api/account/get_user', endpoint='get_user'),
                Rule('/api/account/vote', endpoint='vote'),
                Rule('/api/account/pwdreset', endpoint='pwdreset'),
                Rule('/api/account/pwdreset_confirm', endpoint='pwdreset_confirm'),
        ])

    def checkEmailExist(self, s_email):
        s_sql = '''select count(*) from users where email = %s'''
        d_count = DbManager.instance().GetCount(s_sql, [s_email,])
        if d_count >= 1:
            return True

        return False


    def test(self, m_postvars, o_result, o_session, a_responseHeaders):
        s_prev = ''
        if 'token' in o_session:
            s_prev = o_session['token']
        o_session['token'] = self.generateToken(100)
        print('tokens', s_prev, o_session['token'])

        return '200 OK'

    def checkPwd(self, s_email, s_pwd, a_msg = []):
        if not self.checkEmailExist(s_email):
            a_msg.append('no_id')
            return False

        s_sql = '''select count(*) from users where email = %s and md5(%s) = pwd'''
        d_count = DbManager.instance().GetCount(s_sql, [s_email, s_pwd])
        if d_count > 0:
            return True

        return False

    def generateToken(self, n_len):
        return "".join(str(random.randint(1,9)) for _ in range(n_len))

    def getUser(self, s_email):
        return User.get_user(s_email)

    def sendValidateEmail(self, s_email, s_token):
        s_url = config['domain'] + '/api/account/validate_email?token=' + s_token
        EmailManager.instance().sendEmail('이메일 인증', s_email, 'mail_templates/validate_email.html', {
            '<<url>>' : s_url
            });

    def sendPwdresetEmail(self, s_email, s_token):
        s_url = config['domain'] + '/pwdreset/' + s_token
        EmailManager.instance().sendEmail('비밀번호 재설정', s_email, 'mail_templates/pwd_reset.html', {
            '<<url>>' : s_url
            });




    #                   #
    # handler functions # 
    #                   #

    def on_sign_in(self, o_request, o_session):
        a_msg = []

        m_args = o_request.get_json()

        s_email = m_args['email']
        s_pwd = m_args['pwd']

        b_exists = self.checkEmailExist(s_email)
        b_checkPwd = self.checkPwd(s_email, s_pwd, a_msg)

        if not b_exists:
            print('account does not exist')
            return Response('NOT_EXIST', status=500)

        o_result = {}
        if b_checkPwd:
            o_user = self.getUser(s_email)
            if o_user['validated'] == 1:
                s_token = jwt.encode(o_user, config['secret'], algorithm="HS256")
                print('token', s_token)
                o_result['token'] = s_token
                o_result['user'] = o_user
                o_session['user'] = o_user
                print(o_result)
                return Response(json.dumps(o_result), status=200, content_type='application/json')
            # resend validation email.
            s_token = User.gen_token(s_email)
            self.sendValidateEmail(s_email, s_token)
            return Response('NOT_VALIDATED', status=500)
        else:
            return Response('PWD_WRONG', status=500)

    def on_check_signed_in(self, o_request, o_session):
        o_result = {}
        s_token = self.getTokenFromRequest(o_request)
        if s_token:
            o_user = self.getUserFromToken(s_token)
            o_result['user'] = o_user

        return Response(json.dumps(o_result), status=200)

    def on_reload_user(self, o_request, o_session):
        o_result = {}
        o_user = self.getUserFromReq(o_request)
        if o_user:
            o_result['user'] = o_user
            o_session['user'] = o_user
        return Response(json.dumps(o_result), status=200)

    def on_pwdreset(self, o_request, o_session):
        m_args = o_request.get_json()
        s_email = m_args['email']
        s_token = User.gen_token(s_email)
        if s_token:
            self.sendPwdresetEmail(s_email, s_token)
            return Response(json.dumps('OK'), status=200)
        else:
            return Response(json.dumps('FAIL'), status=500)

    def on_pwdreset_confirm(self, o_request, o_session):
        m_args = o_request.get_json()
        s_token = m_args['token']
        s_pwd = m_args['pwd']
        s_email = User.get_email_by_token(s_token)
        if s_email is None:
            return Response(json.dumps('FAIL'), status=500)
        b_res = User.update_pwd(s_email, s_pwd)
        if not b_res:
            return Response(json.dumps('FAIL'), status=500)
        return Response(json.dumps('OK'), status=200)


    # returns True when success
    def on_sign_up(self, o_request, o_session):
        m_args = o_request.get_json()
        s_email = m_args['email']
        s_pwd = m_args['pwd']
        s_userName = m_args['userName']
        s_birthDate = m_args['birthDate']

        o_result = {}
        if self.checkEmailExist(s_email):
            o_result['statusText'] = 'DUPLICATE'
            return Response(json.dumps(o_result), status=500)

        s_sql = '''insert into users(email, pwd, name, birth_date) values(%s, md5(%s), %s, %s)'''
        b_res = DbManager.instance().Execute(s_sql, [s_email, s_pwd, s_userName, s_birthDate], 'town_community', True)

        if b_res == True:
            s_token = User.gen_token(s_email)
            self.sendValidateEmail(s_email, s_token)
            return Response(json.dumps('OK'), status=200)

        return Response(json.dumps('FAIL'), status=500)

    def on_sign_out(self, o_request, o_session):#m_postvars, o_result, o_session, a_responseHeaders):
        o_session['user'] = None;
        o_result = 'OK'
        return Response(json.dumps(o_result), status=200)

    def on_validate_email(self, o_request, o_session):
        m_args = o_request.args
        print('validate', m_args)
        s_token = m_args['token']
        print('token',s_token)
        d_id = DbManager.instance().GetSingleValue('users', 'id', ['validation_token', 'validated'], [s_token, False])

        if d_id:
            DbManager.instance().UpdateField('users', 'validated', 1, ['validation_token'], [s_token])
            return Response(headers=[('Location', '/validated')], status=303)
        else:
            return Response(headers=[('Location', '/validated')], status=303)

    def on_upload_img(self, o_request, o_session):
        a_fileNames = FileManager.instance().handle_save_req(o_request, 'profile_imgs')
        if len(a_fileNames) > 0:
            for s_fileName in a_fileNames:
                UserPics.save_pic(SessionManager.get_id(o_session), s_fileName)
            return Response(json.dumps(a_fileNames), status=200)
        return Response(status=500)

    def on_get_profile_imgs(self, o_request, o_session):
        a_picNames = UserPics.get_pic_names(SessionManager.get_id(o_session))
        return Response(json.dumps(a_picNames), status=200)

    def on_set_main_img(self, o_request, o_session):
        m_args = o_request.get_json()
        d_user_id = SessionManager.get_id(o_session)
        UserPics.set_main( d_user_id, m_args['img_id'] )
        return Response(status=200)

    def on_del_img(self, o_request, o_session):
        m_args = o_request.get_json()
        UserPics.delete_pic(m_args['img_id'])
        return Response(status=200) 

    def on_get_my_profile(self, o_request, o_session):
        m_args = o_request.get_json()
        d_user_id = SessionManager.get_id(o_session)
        a_picNames = UserPics.get_pic_names(d_user_id)
        print('get myprofile', d_user_id)
        s_sql = '''
            select u.name, u.email, u.intro, t.sig_cd as town_sig_id, 
                    t.sig_kor_nm as town_sig_kor_nm, u.town_id 
                from users u
                    left join towns t 
                        on u.town_id = t.id
                where u.id = %s
                '''

        a_result = DbManager.instance().GetDbResult(s_sql, (d_user_id,), True)
        if len(a_result) > 0:
            a_result[0]['imgs'] = a_picNames
            return Response(json.dumps(a_result[0]), status=200)
        return Response(json.dumps('NO_USER_FOUND'), status=500)

    def on_get_user(self, o_request, o_session):
        m_args = o_request.get_json()
        d_user_id = m_args['user_id']
        s_sql = '''
            select u.id, u.name, u.email, town_id, t.sig_cd as town_sig_id, up.name as profile_path, t.sig_kor_nm as town_sig_kor_nm
            from users u 
                left join towns t on t.id = u.town_id 
                left join user_pics up on up.user_id = u.id 
                    and up.is_main = 1
                where u.id = %s
                '''
        a_result = DbManager.instance().GetDbResult(s_sql, (d_user_id,), True)
        if len(a_result) == 0:
            raise NoDataFound()
        return Response(json.dumps(a_result[0]), status=200)

    def on_get_user_profile(self, o_request, o_session):
        m_args = o_request.get_json()
        d_targetUserId = m_args['user_id']
        d_userId = SessionManager.get_id(o_session)
        a_picNames = UserPics.get_pic_names(d_targetUserId)
        s_sql = '''
            select u.name, u.email, u.intro, t.sig_cd as town_sig_id, 
                    t.sig_kor_nm as town_sig_kor_nm, u.town_id ,
                    b.val as vote
                from users u
                    left join towns t 
                        on u.town_id = t.id
                    left join behaviors b
                        on type = 'user-vote'
                            and target_id = u.id
                            and user_id = %s
                where u.id = %s
                '''

        a_result = DbManager.instance().GetDbResult(s_sql, ( d_userId, d_targetUserId, ), True)

        if len(a_result) > 0:
            a_result[0]['imgs'] = a_picNames
            return Response(json.dumps(a_result[0]), status=200)
        return Response(json.dumps('NO_USER_FOUND'), status=500)

    def on_update_field(self, o_request, o_session):
        m_args = o_request.get_json()
        o_user = self.getUserFromReq(o_request)
        d_user_id = SessionManager.get_id(o_session)
        s_name = m_args['name']
        s_value = m_args['value']
        s_sql = 'update users set '+s_name+' = %s where id = %s'
        DbManager.instance().Execute(s_sql, (s_value, d_user_id))
        return Response(json.dumps('OK'), status=200)

    def on_vote(self, o_request, o_session):
        MAX_COUNT = 5
        m_args = o_request.get_json()

        d_userId = SessionManager.get_id(o_session)
        d_targetUserId = m_args.get('user_id')
        b_isUp = m_args.get('is_up')

        d_totalCount = Behaviors.get_count('user-vote', None, d_userId)
        d_count = Behaviors.get_count('user-vote', d_targetUserId, d_userId)

        if b_isUp:
            if d_totalCount >= MAX_COUNT:
                return Response(json.dumps('max-count-exceed'), status=400)

            if d_count != 0:
                return Response(json.dumps('alread-voted'), status=400)
        else:
            if d_count == 0:
                return Response(json.dumps('not-voted'), status=400)

        if b_isUp:
            Behaviors.add_behavior(10, 'user-vote', d_targetUserId, d_userId)
        else:
            Behaviors.remove_behavior('user-vote', d_targetUserId, d_userId)

        Scores.calc_from_behaviors('user-vote', d_targetUserId)
        return Response(json.dumps('OK'), status=200)



if __name__ == '__main__':

    o_manager = AccountHandler()
    a_msg = []
    #b_res = o_manager.checkPwd('vince81@naver.com', 'test2', a_msg)

    print(re.search("test", "test"))


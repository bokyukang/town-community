
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import json 

from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.middleware.shared_data import SharedDataMiddleware
from werkzeug.utils import redirect
from jinja2 import Environment, FileSystemLoader

from post_handler import PostHandler
from managers.file_manager import FileManager
from helpers.db.forum_articles import ForumArticles
from helpers.db.forum_list import ForumList
from helpers.db.towns import Towns
from helpers.db.votes import Votes
from helpers.db.article_imgs import ArticleImgs
from helpers.db.comments import Comments
from managers.session_manager import SessionManager
from helpers.image_helper import ImageHelper
from helpers.db.behaviors import Behaviors
from helpers.db.scores import Scores

MAX_IMG_SIZE = 1024

class ForumHandler(PostHandler):
    def __init__(self):
        self.m_urlMap = Map([
            Rule('/api/forum/upload_img', endpoint='upload_img'),
            Rule('/api/forum/del_img', endpoint='del_img'),
            Rule('/api/forum/save_contents', endpoint='save_contents'),
            Rule('/api/forum/get_forum_articles', endpoint='get_forum_articles'),
            Rule('/api/forum/get_article', endpoint='get_article'),
            Rule('/api/forum/get_town_forums', endpoint='get_town_forums'),
            Rule('/api/forum/get_user_articles', endpoint='get_user_articles'),
            Rule('/api/forum/create_forum', endpoint='create_forum'),
            Rule('/api/forum/delete', endpoint='delete'),
            Rule('/api/forum/vote', endpoint='vote'),
            Rule('/api/forum/add_comment', endpoint='add_comment'),
            Rule('/api/forum/del_comment', endpoint='del_comment'),
            Rule('/api/forum/get_comments', endpoint='get_comments'),
            Rule('/api/forum/get_forums', endpoint='get_forums'),
            Rule('/api/forum/get_default_forum', endpoint='get_default_forum'),
            ])


    #                   #
    # handler functions # 
    #                   #

    def on_upload_img(self, o_request, o_session):
        if len(o_request.files) > 0 :
            a_fileNames = FileManager.instance().handle_save_req(o_request)
            s_fileName = a_fileNames[0]
            ImageHelper.resize(MAX_IMG_SIZE, s_fileName)
            s_url = s_fileName
            s_type = 'pic'
        else:
            s_url = o_request.values['url']
            s_type = o_request.values['type']
        d_lastRow = ArticleImgs.save_img(-1, s_url, s_type)
        return Response(json.dumps({'id':d_lastRow, 'img_url':s_url, 'type':s_type}), status=200)

    def on_del_img(self, o_request, o_session):
        o_json = o_request.get_json()
        d_id = o_json['id']
        o_img = ArticleImgs.get_by_id(d_id)
        b_return = True
        if o_img['type'] == 'img':
            if not FileManager.instance().delete_file(o_img['img_url']):
                b_return = False
        ArticleImgs.del_img(d_id)
        return Response(json.dumps(b_return), status=200)



    def on_save_contents(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        if o_user:
            o_json = o_request.get_json()
            d_id = o_json['id']
            d_forum_list_id = o_json['forum_list_id']
            s_title = o_json['title']
            s_contents = o_json['contents']
            s_summary = o_json['summary']
            s_user_name = o_json['user_name']
            a_imgs = o_json['imgs']
            if (not d_id) or (d_id == -1):
                d_id = ForumArticles.add_new_article(
                        d_forum_list_id, o_user['id'], s_title, s_contents, s_summary, s_user_name, a_imgs)
            else:
                ForumArticles.save_article(d_id, s_title, s_contents, s_summary, o_user['id'], s_user_name, a_imgs)
            return Response( json.dumps({'id':d_id}), status=200, content_type='application/json')
        return Response(status=500)

    def on_get_forum_articles(self, o_request, o_session):
        o_json = o_request.get_json()
        s_orderBy = 'created_at desc'
        s_sort = o_json['sort']
        if s_sort == 'auto':
            s_orderBy = 'f.recent_reputation desc, f.reputation desc, f.created_at desc'
        elif s_sort == 'popular':
            s_orderBy = 'f.reputation desc, f.created_at desc'
        a_forumArticles = ForumArticles.get_forum_articles(o_json['forum_list_id'], o_json['start_idx'], o_json['rows'], s_orderBy)
        d_forumArticlesCount = ForumArticles.get_forum_articles_count(o_json['forum_list_id'])
        o_result = {
                'articles' : a_forumArticles,
                'totalRecords' : d_forumArticlesCount
                }
        return Response(json.dumps(o_result), status=200, content_type='application/json')

    def on_get_article(self, o_request, o_session):
        o_json = o_request.get_json()
        d_articleId = o_json['forum_article_id']
        o_article = ForumArticles.get_article(d_articleId)
        if o_article:
            o_user = self.getUserFromReq(o_request)
            if o_user:
                #o_article['vote'] = Votes.get_vote(o_user['id'], 'article', d_articleId)
                o_article['vote'] = Behaviors.get_val( 'article', d_articleId, o_user['id'])
            #comments

            return Response(json.dumps(o_article), status=200, content_type='application/json')
        return Response('NOT_EXIST', status=500)

    #def on_get_town_forums(self, o_request, o_session):
    #    o_json = o_request.get_json()
    #    d_town_id = o_json['town_id']
    #    TownForums.get_default_town_forum(d_town_id)
    #    a_town_forums = ForumList.get_town_forums(d_town_id)
    #    return Response(json.dumps(a_town_forums), status=200, content_type='application/json')

    def on_get_user_articles(self, o_request, o_session):
        o_json = o_request.get_json()
        if 'user_id' in o_json and o_json['user_id']:
            d_user_id = o_json['user_id']
        else:
            d_user_id = SessionManager.get_id(o_session)
        print('user articles user_id', d_user_id)
        a_userArticles, d_count= ForumArticles.get_user_articles( d_user_id, o_json['start_idx'], o_json['rows'])
        o_result = {
                'articles' : a_userArticles,
                'totalRecords' : d_count
                }
        return Response(json.dumps(o_result), status=200, content_type='application/json')

    def on_create_forum(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        print('o_json', o_json)
        ForumList.create_new(
                o_json.get('title'),
                o_json.get('type'),
                o_json.get('type_id'),
                o_json.get('tag'),
                o_user.get('id'),
                **o_json)
        return Response('', status=200)

    def on_delete(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        b_result = ForumArticles.delete_article(o_json['id'], o_user['id'])
        return Response(json.dumps(b_result), status=200, content_type='application/json')

    def on_vote(self, o_request, o_session):
        VOTE_COUNT = 100
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        if o_user:
            d_dir = o_json.get('dir')
            d_voteChange = Behaviors.add_behavior(
                    d_dir,
                    o_json['type'],
                    o_json['ref_id'],
                    o_user['id'],
                    )
            Behaviors.remove_from_n(o_json['type'], o_user['id'], VOTE_COUNT)
            if o_json['type'] == 'article':
                d_reputChange = ForumArticles.update_reputation(
                    o_user['id'], o_json['ref_id'], d_voteChange)
            elif o_json['type'] == 'comment':
                d_reputChange = Comments.update_reputation(
                        o_user['id'], o_json['ref_id'], d_voteChange)

            return Response(json.dumps(d_reputChange),
                    status=200,
                    content_type='application/json')
        return Response(None, status=500, content_type='application/json')

    def on_del_comment(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        d_id = o_json['id']
        Comments.del_comment(d_id, o_user['id'])
        return Response(json.dumps(''), status=200, content_type='application/json')

    def on_add_comment(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        o_comment = o_json['comment']
        d_id = Comments.add_comment(o_comment['type'], o_user['id'], o_comment['ref_id'], o_comment['content'])
        return Response(json.dumps(d_id), status=200, content_type='application/json')

    def on_get_comments(self, o_request, o_session):
        o_user = self.getUserFromReq(o_request)
        o_json = o_request.get_json()
        s_sort = o_json['sort']
        am_comments = Comments.get_comments(o_json['type'], o_json['article_id'], s_sort, o_user.get('id'))
        return Response(json.dumps(am_comments), status=200, content_type='application/json')

    def on_get_forums(self, o_request, o_session):
        o_json = o_request.get_json()
        am_forums = ForumList.get_forums(o_json.get('type'), o_json.get('type_id'), o_json.get('tag'))
        return Response(json.dumps(am_forums), status=200, content_type='application/json')

    def on_get_default_forum(self, o_request, o_session):
        o_json = o_request.get_json()
        m_forum = ForumList.get_default_forum(o_json.get('type'), o_json.get('type_id'), True)
        return Response(json.dumps(m_forum), status=200, content_type='application/json')



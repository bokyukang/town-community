
class SessionManager:

    @staticmethod
    def is_logged_in(o_session):
        if o_session.get('user') is not None  and o_session['user'].get('id') is not None:
            return True
        return False

    @staticmethod
    def get_id(o_session):
        if not SessionManager.is_logged_in(o_session):
            return None

        return o_session['user']['id']

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from config import config

class EmailManager:
    _instance = None

    @staticmethod
    def instance():
        if EmailManager._instance is None:
            EmailManager._instance = EmailManager()
        return EmailManager._instance

    def sendEmail(self, s_subject, s_to, s_contentFile, m_replace = {}):
        s_html = ''
        s_from = config['sender_email']
        with open(s_contentFile, encoding='utf-8') as fp:
            s_html = fp.read()
            for s_key in m_replace:
                s_html = s_html.replace(s_key, m_replace[s_key])

        o_msg = MIMEMultipart('alternative')
        o_msg['Subject'] = s_subject
        o_msg['From'] = s_from
        o_msg['To'] = s_to
        o_part2 = MIMEText(s_html, 'html')
        o_msg.attach(o_part2)

        o_s = smtplib.SMTP_SSL('smtp.gmail.com')
        o_s.login(s_from, config['sender_pwd'])
        o_s.sendmail(s_from, s_to, o_msg.as_string())
        o_s.quit()

if __name__ == '__main__':
    EmailManager.instance().sendEmail('test', 'bokyukang.kr@gmail.com', '../mail_templates/test.html', {})

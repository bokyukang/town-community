import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import mysql.connector
import threading 
from config import config


class DbManager:
    _instance = None

    def __init__(self):

        self.other_dbs = dict()
        self.o_lock = threading.Lock()

    @staticmethod
    def instance():
        if DbManager._instance  == None:
            DbManager._instance = DbManager()
        return DbManager._instance

    # local 서버 (windows)
    def Connect(self, s_dbname = 'town_community'):
        if config['run_mode'] == 'dev':
            s_realDbName = s_dbname + '_dev'
        else:
            s_realDbName = s_dbname
        if s_dbname in self.other_dbs and self.other_dbs[s_dbname].is_connected():
            return
        self.other_dbs[s_dbname] = mysql.connector.connect(
            host = config['db_host'],
            user = config['db_user'],
            passwd = config['db_passwd'],
            database = s_realDbName,
            autocommit = True
        )

    def GetDb(self, s_dbname = 'town_community'):
        try:
            return self.other_dbs[s_dbname]
        except:
            return None

    def GetCursor(self, b_is_dict = False, s_dbname = 'town_community'):
        db = self.GetDb(s_dbname)
        if ((db is None) or (not db.is_connected())):
            print('connecting to ' , s_dbname)
            self.Connect(s_dbname)
            db = self.GetDb(s_dbname)
        print('selected db', db)
        if b_is_dict:
            return db.cursor(dictionary=True, buffered=True)
        else:
            return db.cursor(buffered=True)
            
    def Commit(self, s_dbname = 'town_community'):
        db = self.GetDb(s_dbname)
        db.commit()

    def GetDbResult(self, s_sql, a_args, b_isMap = False, s_dbName = 'town_community'):
        self.o_lock.acquire()
        try:
            o_cursor = self.GetCursor(b_isMap, s_dbName)
            o_cursor.execute(s_sql, a_args)
            a_result = o_cursor.fetchall()
            self.GetDb(s_dbName).commit()
            o_cursor.close()
            return a_result
        finally:
            self.o_lock.release()

    def GetCount(self, s_sql, a_args, s_dbname = 'town_community'):
        aa_result = DbManager.instance().GetDbResult(s_sql, a_args, False, s_dbname)
        if len(aa_result) > 0:
            return aa_result[0][0]

        return None

    def Execute(self, s_sql, a_args, s_dbName = 'town_community', b_commit = True, a_msg = [], b_returnLastId = False, a_affectedRows = None):
        self.o_lock.acquire()
        try:
            o_cursor = self.GetCursor(False, s_dbName)
            o_cursor.execute(s_sql, a_args)
            returnVal = None
            if b_returnLastId:
                returnVal = o_cursor.lastrowid

            if a_affectedRows is not None:
                a_affectedRows[0] = o_cursor.rowcount

            self.GetDb(s_dbName).commit()
            o_cursor.close()
        except mysql.connector.IntegrityError as err:
            a_msg.append( 'IntegrityError')
            return False
        finally:
            self.o_lock.release()

        if returnVal:
            return returnVal
        return True

    def UpdateField(self, s_table, s_field, value, a_whereKeys, a_whereVals, s_dbName='town_community'):
        s_sql = 'update ' + s_table + ' set ' + s_field + ' = %s '
        s_sql += 'where ' + ' and '.join( [ s_key + ' = %s' for s_key in a_whereKeys ] )
        return self.Execute(s_sql, [value] + a_whereVals, s_dbName, True)

    def GetSingleValue(self, s_table, s_field, a_whereKeys, a_whereVals, s_dbName='town_community'):
        s_sql = 'select ' + s_field + ' from ' + s_table 
        s_sql += ' where ' + ' and '.join( [ s_key + ' = %s' for s_key in a_whereKeys ] )
        a_result = self.GetDbResult(s_sql, a_whereVals, False, s_dbName)
        if len(a_result) > 0:
            return a_result[0][0]
        return None

if __name__ == '__main__':
    DbManager.instance().UpdateField('users', 'validated', 1, ['id'], [1])

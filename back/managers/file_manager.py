import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import re 
from helpers.general_helper import GeneralHelper
import os

class FileManager:

    _instance = None

    @staticmethod
    def instance():
        if FileManager._instance == None:
            FileManager._instance = FileManager()
        return FileManager._instance

    def __init__(self):
        pass

    @staticmethod
    def get_file_extension(s_filename):
        m = re.search('.*\.([^.]+)', s_filename)
        if m:
            return m.group(1)
        return ''
        

    # b_isRandomName : 파일명을 렌덤하게 재생성.
    @staticmethod
    def save_file( o_fileStorage, s_path, b_isRandomName = True):
        
        s_name = o_fileStorage.filename
        if b_isRandomName:
            s_extension = FileManager.get_file_extension(o_fileStorage.filename)
            s_name = GeneralHelper.generate_random_str(40) + '.' + s_extension

        print('s_path ', s_path)
        if os.path.exists(s_path) == False:
            print('doesnt exist')
            os.makedirs(s_path)
        o_fileStorage.save(s_path + '/' + s_name )
        return s_name

    @staticmethod
    def delete_file( s_path):
        print('deleting ', s_path)
        try:
            os.remove(s_path)
        except Exception as e:
            print(e)
            return False
        return True


    # saves files as randome names
    # @returns : list of saved file names.
    def handle_save_req(self, o_request, s_imgPath = 'uploaded_imgs'):
        a_fileNames = []
        s_imgPath = 'static/' + s_imgPath
        for s_fileKey in o_request.files:
            o_fileStorage = o_request.files[s_fileKey]
            s_filename = FileManager.save_file( o_fileStorage, s_imgPath, True)
            a_fileNames.append(s_imgPath + '/' + s_filename)
        return a_fileNames

    
if __name__ == '__main__':
    print( FileManager.get_file_extension('abcd'))

from datetime import datetime
import inspect
import pytz

class LogManager:

    _instance = None
    LOG_FOLDER = './logs/'
    PRINT = True


    def __init__(self):

        self._curdate = ''
        self._timezone = pytz.timezone('Asia/Seoul')
        self._s_file_prefix = ''

    def Init(self, s_file_prefix = ''):
        self._s_file_prefix = s_file_prefix

    def Log(self, s_title, s_msg):
        curframe = inspect.currentframe()
        calframe = inspect.getouterframes(curframe, 2)
        s_call_file = str(calframe[1][1])
        d_str_idx = s_call_file.rfind('\\') + 1
        if d_str_idx >=0:
            s_call_file = s_call_file[d_str_idx:]
        s_caller = s_call_file + ':' + str(calframe[1][2] )+ ':' + calframe[1][3]

        self.CheckFile()
        s_content = datetime.now().astimezone(self._timezone).strftime('%Y-%m-%d %H:%M:%S') + ' ['+s_title+'] (' + s_caller + ') ' + str(s_msg)
        if LogManager.PRINT:
            print(s_content, flush=True)
        self._file.write(s_content + "\n")
        self._file.flush()

    def CheckFile(self):
        s_newdate = datetime.today().strftime('%Y%m%d')
        if ( self._curdate != s_newdate ):
            self._curdate = s_newdate
            if self._file:
                self._file.close()
            self._file = open(LogManager.LOG_FOLDER + self._s_file_prefix + self._curdate, 'a', encoding='utf-8')

    def __del__(self):
        if self._file:
            self._file.close()



if __name__ == "__main__":
    tmp = 1122
    tmp2 = 'tmp2 = %s' % tmp
    print(tmp2)
    LogManager.instance().Log('err', u"테스트")
    LogManager.instance().Log('err', "테스트")

PYTHON=/usr/bin/python3
PORT=5001
EXE_DIR=/home/bk/workspace/town-community/back
PID=$(ps -ef | grep "town-community/back/server" | grep -v "grep" | awk '{print $2}') 
echo "PID is ${PID}"
if [ "$PID" != "" ];then
    sudo kill -9 $PID
fi;
sudo $PYTHON ${EXE_DIR}/server.py $PORT

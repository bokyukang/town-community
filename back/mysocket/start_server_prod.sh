#!/bin/sh
PYTHON=/usr/bin/python3
PORT=5010
EXE_DIR=/home/bk/production/town-community/back
PID=$(ps -ef | grep "town-community/back/mysocket/socket_manager.py ${PORT}" | grep -v "grep" | awk '{print $2}') 
echo "PID is ${PID}"
if [ "$PID" != "" ];then
    sudo kill -9 $PID
fi;
sudo $PYTHON ${EXE_DIR}/mysocket/socket_manager.py $PORT

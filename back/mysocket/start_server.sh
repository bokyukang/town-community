PYTHON=/usr/bin/python3
PORT=$1
EXE_DIR=/home/bk/workspace/town-community/back
PID=$(ps -ef | grep "town-community/back/mysocket/server" | grep -v "grep" | awk '{print $2}') 
echo "PID is ${PID}"
if [ "$PID" != "" ];then
    sudo kill -9 $PID
fi;
sudo $PYTHON ${EXE_DIR}/mysocket/socket_manager.py $PORT

import jwt
from helpers.db.user import User
from config import config

class SocketHandler:
    def handle_msg(self, o_socket, o_msg):
        pass

    def getUserFromToken(self, s_token):
        o_user = jwt.decode(s_token, config['secret'], algorithms=["HS256"])
        if o_user:
            return o_user
        return None


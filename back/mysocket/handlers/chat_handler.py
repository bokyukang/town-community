import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from mysocket.handlers.socket_handler import SocketHandler 
import asyncio
from helpers.db.chat_entries import ChatEntries 
from helpers.db.user import User
import websockets
import json
import time
from threading import Thread, Lock
from managers.log_manager import LogManager

class Room:
    def __init__(self):
        self.m_sockets = {}
        self.m_users = {}
        self.o_lock = Lock()

    def add(self, o_socket, o_user):
        self.o_lock.acquire()

        print('adding')
        self.m_sockets[o_user['id']] = o_socket
        self.m_users[ o_user['id'] ] =  o_user

        self.o_lock.release()

    def remove(self, d_userId):
        self.o_lock.acquire()
        try:
            o_user = self.m_users[d_userId]
            del self.m_sockets[o_user['id']]
            del self.m_users[d_userId]
            return True
        except ValueError:
            return False
        finally:
            self.o_lock.release()

    def checkDisconnect(self):
        d_count = 0
        self.o_lock.acquire()
        for s_key in self.m_sockets.keys():
            try:
                o_socket = self.m_sockets[s_key]
                o_socket.send(json.dumps({'op':'check'}))
            except websockets.ConnectionClosed as exc:
                d_count += 1
                print('connection closed', o_socket, exc)
                del self.m_sockets[s_key]

        self.o_lock.release()
        LogManager.instance().Log('CheckDisconnect', 'removed %d sockets'%( d_count, ))

class Rooms:
    def __init__(self):
        self.m_rooms = {}

    def add(self, o_socket, d_chatId, o_user):
        if d_chatId not in self.m_rooms:
            self.m_rooms[d_chatId] = Room()
        self.m_rooms[d_chatId].add(o_socket, o_user)

    def remove(self, d_chatId, d_userId):
        try:
            return self.m_rooms[d_chatId].remove(d_userId)
        except KeyError:
            return False

    def broadcast(self, d_chatId, o_msg):
        try:
            websockets.broadcast(self.m_rooms[d_chatId].m_sockets.values(), json.dumps(o_msg))
        except KeyError:
            return False
        return True

    def get_room(self, d_chatId):
        return self.m_rooms[d_chatId]

    def get_entry_users(self, d_chatId):
        a_entryUsers = ChatEntries.get_users(d_chatId)
        m_return = {}
        for o_entryUser in a_entryUsers:
            m_return[o_entryUser['id']] = o_entryUser
        return m_return 

    def get_entries(self,d_chatId):
        return ChatEntries.get_entries(d_chatId)

    def checkDisconnect(self):
        for o_room in self.m_rooms.values():
            o_room.checkDisconnect()

class ChatBackground(Thread):
    def __init__(self, o_rooms):
        self.o_rooms = o_rooms
        super().__init__()

    def run(self):
        while True:
            time.sleep(60)
            self.checkDisconnect()

    def checkDisconnect(self):
        self.o_rooms.checkDisconnect()

class ChatHandler(SocketHandler):
    def __init__(self):
        self.o_rooms = Rooms()
        self.o_background = ChatBackground(self.o_rooms)
        self.o_background.start()

    def checkDisconnect(self):
        self.o_rooms.checkDisconnect()

    def return_msg(self, o_origMsg, o_msg):
        o_msg['chat_id'] = o_origMsg['chat_id']
        o_msg['op'] = 'chat'
        return json.dumps(o_msg)

    def handle_msg(self, o_socket, o_msg):

        o_user = self.getUserFromToken( o_msg['token'] )
        o_msg['user'] = o_user
        o_msg['user_id'] = o_user['id']

        if o_msg['chat_op'] == 'enter':
            print('user entering ', o_msg)
            self.o_rooms.add(o_socket, o_msg['chat_id'], o_user)
            o_room = self.o_rooms.get_room(o_msg['chat_id'])
            ChatEntries.create_new(o_msg['chat_id'], o_user['id'], 'info', 'enter', [])
            o_msg['type'] = 'info'
            o_msg['content'] = 'enter'

            self.o_rooms.broadcast(o_msg['chat_id'], o_msg)

            return self.return_msg(o_msg, 
                {
                    'chat_op': 'init',
                    'users': o_room.m_users,
                    'entry_users' : self.o_rooms.get_entry_users(o_msg['chat_id']),
                    'entries' : self.o_rooms.get_entries(o_msg['chat_id'])
                })

        elif o_msg['chat_op'] == 'exit':
            print('user exiting ', o_msg)
            o_msg['type'] = 'info'
            self.o_rooms.remove(o_msg['chat_id'], o_user['id'])
            ChatEntries.create_new(o_msg['chat_id'], o_user['id'], 'info', 'exit', [])
            o_msg['content'] = 'exit'

            self.o_rooms.broadcast(o_msg['chat_id'], o_msg)

        elif o_msg['chat_op'] == 'content': 
            print('user broadcast ', o_msg)
            o_msg['type'] = 'msg'
            ChatEntries.create_new(o_msg['chat_id'], o_user['id'], 'msg', o_msg['content'], o_msg['imgs'])
            self.o_rooms.broadcast(o_msg['chat_id'], o_msg)

        return None


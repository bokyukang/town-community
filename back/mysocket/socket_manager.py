import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import asyncio
import websockets
from mysocket.handlers.chat_handler import ChatHandler
import json


class SocketManager:

    def __init__(self, n_port):
        self.n_port = n_port

    async def worker(self,websocket):
        async for s_msg in websocket:
            o_msg = json.loads(s_msg)
            if o_msg['op'] in self.o_handlers:
                s_return = self.o_handlers[o_msg['op']].handle_msg(websocket, o_msg)
                if s_return:
                    print('returning ', s_return)
                    await websocket.send(s_return)

    async def main(self):
        self.o_handlers = {
            'chat' : ChatHandler(),
        }

        async with websockets.serve(self.worker, '0.0.0.0', self.n_port):
            await asyncio.Future()

    def run(self):
        asyncio.run(self.main())

if __name__ == '__main__':
    print(sys.argv)
    if len(sys.argv) < 2:
        print('usage: python socket_manager.py [port]')
        exit(1)

    n_port = sys.argv[1]
    socketManager = SocketManager( n_port)
    socketManager.run()

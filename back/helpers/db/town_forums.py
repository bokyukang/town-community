import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
from managers.db_manager import DbManager
from helpers.db.towns import Towns
from helpers.db.forum_list import ForumList

class TownForums(DbTable):
    @staticmethod
    def get_default_town_forum(d_town_id, b_create_if_not = True):

        s_sql = '''select
                        id
                    from forum_list
                    where type_id = %s
                        and type = 'town'
                        and tag = 'default'
                        '''
        a_result = DbManager.instance().GetDbResult(s_sql, [d_town_id,],False )
        if len(a_result) == 0 and b_create_if_not:
            # doesn't exist, create
            d_forum_list_id = ForumList.create_new(Towns.get_town_name(d_town_id), 'town', d_town_id, 'default')
            #TownForums.create_new(d_town_id, d_forum_list_id, 'default')
            
        s_sql = '''select 
                        t.id as town_id, fl.id as forum_list_id, fl.tag as type, fl.title, fl.user_id 
                    from forum_list fl
                        left join towns t
                            on fl.type_id = t.id
                    where t.id = %s'''
        return DbManager.instance().GetDbResult(s_sql, [d_town_id,], True)

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
from managers.db_manager import DbManager
from helpers.general_helper import GeneralHelper
from helpers.myexceptions import *

class Bounds:
    def __init__(self, f_left, f_bottom, f_right, f_top):
        self.f_left = f_left
        self.f_bottom = f_bottom
        self.f_right = f_right
        self.f_top = f_top


class Events(DbTable):

    @staticmethod
    def create_new(s_title, s_type, d_userId, f_lat, f_lng, o_at, **kwargs):
        m_fields = {'title':s_title, 'type':s_type, 'user_id':d_userId, 'lat':f_lat, 'lng':f_lng, 'at':o_at}
        m_fields = {**m_fields, **kwargs}
        m_fieldsUnprocess = {'created_at': 'now()'}

        s_sql = '''insert into events(''' + \
            ','.join(list(m_fields.keys()) + list(m_fieldsUnprocess.keys())) + '''
            ) values(''' + \
            ','.join(['%s' for item in m_fields.keys()] + list(m_fieldsUnprocess.values())) + ')'

        d_lastId = DbManager.instance().Execute(s_sql, list(m_fields.values()), 'town_community', True, [], True)
        return d_lastId

    @staticmethod
    def get_by_id(d_id):
        s_sql = '''select * from events where id = %s'''
        o_event = DbManager.instance().GetDbResult(s_sql, (d_id,), True)
        if len(o_event) == 0:
            raise NoDataFound()
        return o_event[0]

    @staticmethod
    def get_events(o_bounds, a_types = None, s_dateFrom = None, s_dateTo = None, **kwargs):
        m_wheres = {}
        if o_bounds:
            m_wheres = {
                    'lng': ['>', o_bounds.f_left],
                    'lng': ['<', o_bounds.f_right],
                    'lat': ['>', o_bounds.f_bottom],
                    'lat': ['<', o_bounds.f_top],
                    }
        for s_key in kwargs:
            if kwargs[s_key] is not None:
                m_wheres[s_key] = ['=', kwargs[s_key]]

        as_wheres = []
        if a_types and len(a_types) > 0:
            as_wheres.append("type in ('" + "','".join( a_types ) + "')")

        if s_dateFrom and s_dateFrom != '':
            as_wheres.append("(at is null or DATE(at) >= '" + s_dateFrom + "')")

        if s_dateTo and s_dateTo != '':
            as_wheres.append("(at is null or DATE(at) <= '" + s_dateTo + "')")


        s_where = ' and '.join( 
                [ key + value[0] + '%s' for key, value in m_wheres.items() ] + \
                        as_wheres
        )
        print(s_dateFrom, s_dateTo)
        print(s_where)
        a_args = [item[1] for item in m_wheres.values()]
        s_sql = 'select * from events where ' + s_where

        print('sql ' , s_sql)
        print('args ' , list(a_args))
        a_result = DbManager.instance().GetDbResult(s_sql, a_args, True)
        for m_entry in a_result:
            m_entry['at'] = GeneralHelper.datetimeToStr(m_entry['at'])
            m_entry['created_at'] = GeneralHelper.datetimeToStr(m_entry['created_at'])
        return a_result




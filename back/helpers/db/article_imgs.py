import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
import itertools

class ArticleImgs(DbTable):
    @staticmethod
    def update_imgs( d_articleId, a_imgs ):
        if a_imgs is not None and len(a_imgs) > 0:
            for o_img in a_imgs:
                s_sql = '''update article_imgs 
                        set article_id = %s, is_main = %s
                        where id = %s'''
                a_args = (d_articleId, DbTable.convBoolean(o_img['is_main']), o_img['id'])
                DbManager.instance().Execute(s_sql, a_args)

    def save_img(d_articleId, s_imgUrl, s_type = 'pic'):
        s_sql = ''' insert into article_imgs(article_id, img_url, type, created_at) values(%s,%s, %s, NOW())'''
        d_lastRow = DbManager.instance().Execute(s_sql, (d_articleId, s_imgUrl, s_type), 'town_community', True, [], True)
        return d_lastRow

    @staticmethod
    def get_imgs(d_articleId):
        s_sql = 'select id, img_url, type, is_main from article_imgs where article_id = %s'
        aa_result = DbManager.instance().GetDbResult(s_sql, [d_articleId,], True)
        return aa_result

    def get_path(d_id):
        s_sql = 'select img_url from article_imgs where id = %s'
        aa_result = DbManager.instance().GetDbResult(s_sql, (d_id,))
        if len(aa_result) > 0:
            return aa_result[0][0]
        return None

    def get_by_id(d_id):
        s_sql = 'select * from article_imgs where id = %s'
        am_result = DbManager.instance().GetDbResult(s_sql, (d_id,), True)
        if len(am_result) > 0:
            return am_result[0]
        return None

    def del_img(d_id):
        s_sql = 'delete from article_imgs where id = %s'
        DbManager.instance().Execute(s_sql, (d_id,))

    def del_all_in_article(d_articleId):
        s_sql = 'delete from article_imgs where article_id = %s'
        DbManager.instance().Execute(s_sql, (d_articleId,))



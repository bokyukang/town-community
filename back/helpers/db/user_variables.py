import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)

from managers.db_manager import DbManager
from helpers.db.db_table import *

class UserVariables(DbTable):

    @staticmethod
    def check_val(d_userId, s_name, s_val):
        s_sql = '''
            select count(*) 
            from user_variables uv
            where user_id = %s
                and name = %s
                and val = %s
        '''
        d_count = DbManager.instance().GetCount(s_sql, (d_userId, s_name, s_val))
        return d_count > 0

    @staticmethod
    def set_val(d_userId, s_name, s_val):
        s_sql = '''
            insert into user_variables(user_id, name, val)
            values(%s, %s, %s)
            on duplicate key update
                val = %s
        '''
        DbManager.instance().Execute(s_sql, (d_userId, s_name, s_val, s_val))

    @staticmethod
    def get_val(d_userId, s_name):
        s_sql = '''
            select val
            from user_variables
            where user_id = %s
                and name = %s
        '''
        a_dbResult = DbManager.instance().GetDbResult(s_sql, (d_userId, s_name))
        if len(a_dbResult) > 0:
            return a_dbResult[0][0]
        return None

    # default val 부터 시작해서 0 으로 되면 false를 반환
    @staticmethod
    def decr_val(d_userId, s_name, d_default):
        s_val = UserVariables.get_val(d_userId, s_name)
        if s_val is None:
            s_val = d_default
        if int(s_val) <=0:
            return True
        UserVariables.set_val(d_userId, s_name, int(s_val)-1)
        return False


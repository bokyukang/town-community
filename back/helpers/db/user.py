import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)

import random

from helpers.db.db_table import *
from helpers.db.scores import Scores

class User(DbTable):

    @staticmethod
    def get_user(s_email):
        s_sql = '''
                    select 
                        users.id as id, 
                        users.email as email, 
                        users.name as name, 
                        users.validated as validated,
                        user_pics.name as profile_path,
                        towns.id as town_id,
                        towns.SIG_CD as town_sig_id,
                        towns.id as town_id
                    from users 
                        left join user_pics
                            on user_pics.user_id = users.id
                                and user_pics.is_main = 1
                        left join towns
                            on users.town_id = towns.id
                    where email = %s
                '''
        a_result = DbManager.instance().GetDbResult(s_sql, [s_email], True)
        if len(a_result) > 0:
            return a_result[0]
        return None

    @staticmethod
    def get_email_by_token(s_token):
        s_sql = ''' select email from users where validation_token = %s'''
        a_result = DbManager.instance().GetDbResult(s_sql, [s_token], False)
        if len(a_result) > 0:
            return a_result[0][0]
        return None

    @staticmethod
    def update_pwd(s_email, s_pwd):
        s_sql = ''' update users set pwd = md5(%s) where email = %s '''
        a_affectedRows = [0]
        DbManager.instance().Execute(s_sql, [s_pwd, s_email], 'town_community', True, [], False, a_affectedRows)
        return a_affectedRows[0] == 1

    @staticmethod
    def get_reputation(d_id):
        return Scores.get_score('user-vote', d_id, 100 )          
        #return DbManager.instance().GetSingleValue(
        #        'users', 'reputation', ['id',], [d_id,])

    @staticmethod
    def gen_token(s_email):
        s_token = "".join(str(random.randint(1,9)) for _ in range(100))
        s_sql = '''update users set validation_token = %s where email = %s'''
        a_affectedRows = [0]
        b_res = DbManager.instance().Execute(s_sql, [ s_token, s_email ], 'town_community', True, [], False, a_affectedRows)
        if a_affectedRows[0] == 1:
            return s_token
        return None


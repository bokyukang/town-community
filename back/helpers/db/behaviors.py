import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
import mysql

class Behaviors(DbTable):

    @staticmethod
    def get_val(s_type, d_targetId, d_userId):
        s_sql = '''
            select val from behaviors 
                where type = %s
                    and target_id = %s
                    and user_id = %s
                    '''
        aa_res = DbManager.instance().GetDbResult(s_sql, (s_type, d_targetId, d_userId))
        if len(aa_res) == 0:
            return 0
        return aa_res[0][0]

    @staticmethod
    def get_vals(s_type, s_sel = 'target_id', d_targetId  = None, d_userId = None):
        a_args = [s_type,]
        s_sql = '''
            select ''' + s_sel + ''' from behaviors
            where type = %s
            '''
        if d_targetId:
            a_args.append(d_targetId)
            s_sql += ''' and target_id = %s '''
        if d_userId:
            a_args.append(d_userId)
            s_sql += ''' and user_id = %s '''

        am_res = DbManager.instance().GetDbResult(s_sql, a_args)
        return [item[0] for item in am_res]

    @staticmethod
    def remove_from_n(s_type, d_userId, d_n):
        s_sql = ''' 
            select at from behaviors
            where user_id = %s
                and type = %s
            order by at desc
            limit ''' + str(d_n) + ''',1'''
        aa_res = DbManager.instance().GetDbResult(s_sql, [d_userId, s_type, ])
        if len(aa_res) > 0:
            o_removeFrom = aa_res[0][0]
            s_sql = '''
                delete from behaviors
                where user_id = %s
                    and type = %s
                    and at <= %s
                    '''
            DbManager.instance().Execute(s_sql, [d_userId, s_type, o_removeFrom])

    # returns change
    @staticmethod
    def add_behavior(d_val, s_type,  d_targetId, d_userId):
        d_before = Behaviors.get_val(s_type, d_targetId, d_userId)
        s_sql = '''
            insert into behaviors(val, type, target_id, user_id, at)
            values(%s, %s, %s, %s, NOW())
            on duplicate key update val = %s
            '''
        b_res = DbManager.instance().Execute(s_sql, 
                (d_val, s_type,  d_targetId, d_userId, d_val))
        return d_val - d_before

    @staticmethod
    def remove_behavior(s_type,  d_targetId, d_userId):
        s_sql = '''
            delete from behaviors 
            where type = %s
                and target_id = %s
                and user_id = %s
        '''
        DbManager.instance().Execute(s_sql, (s_type,  d_targetId, d_userId))

    @staticmethod
    def get_count(s_type, d_targetId, d_userId):
        s_sql = '''
            select count(*) from behaviors
            where type = %s
            '''
        a_args = [s_type]
        if d_targetId:
            s_sql += '''
                and target_id = %s
            '''
            a_args.append(d_targetId)
        if d_userId:
            s_sql += '''
                and user_id = %s
                '''
            a_args.append(d_userId)
        aa_res = DbManager.instance().GetDbResult(s_sql, a_args, False)
        return aa_res[0][0]

    @staticmethod
    def get_sum(s_type,  d_targetId, d_days = None):
        s_sql = '''
            select sum(val) from behaviors
            where type = %s
                and target_id = %s
        '''
        a_args = [s_type,  d_targetId, ]
        if d_days:
            s_sql += '''
                and at > DATE_SUB(NOW(), INTERVAL %s DAY)
                '''
            a_args.append(d_days)
        a_res = DbManager.instance().GetDbResult(s_sql, a_args)
        d_sum = a_res[0][0]
        if d_sum is None:
            d_sum = 0
        return d_sum

if __name__ == '__main__':
    Behaviors.remove_from_n('comment', 29, 3);


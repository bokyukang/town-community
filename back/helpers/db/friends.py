import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
from managers.db_manager import DbManager

class Friends(DbTable):
    @staticmethod
    def request_friend(d_userId, d_friendId):
        s_sql = '''insert into friends(user_id, friend_id, status, created_at)
                    values(%s, %s, 'pending', NOW())'''
        try:
            DbManager.instance().Execute(s_sql, (d_userId, d_friendId))
            return True
        except mysql.IntegrityError:
            return False

    @staticmethod
    def get_status(d_userId, d_friendId):
        s_sql = '''select status from friends 
                    where user_id = %s and friend_id = %s'''
        a_result = DbManager.instance().GetDbResult(s_sql, (d_userId, d_friendId))
        if len(a_result) == 0:
            return 'not_friend'
        return a_result[0][0]

    @staticmethod
    def accept_request(d_userId, d_friendId):
        s_sql = '''update friends 
                    set status = 'accepted'
                    where user_id = %s 
                        and friend_id = %s
                        and status = 'pending'
                        '''

        a_affectedRows = [0]
        DbManager.instance().Execute(s_sql, ( d_friendId, d_userId), 'town_community', True, [], False, a_affectedRows)


        if a_affectedRows[0] == 1:
            s_sql = '''insert into friends(user_id, friend_id, status, created_at)
                        values(%s,%s,'accepted',NOW())'''
            DbManager.instance().Execute(s_sql, (d_userId, d_friendId))

        print(d_userId, d_friendId, a_affectedRows[0])

        return a_affectedRows[0] == 1

    @staticmethod
    def unfriend(d_userId, d_friendId):
        s_sql = '''delete from friends 
                    where 
                        ( user_id = %s and friend_id = %s )
                        or ( user_id = %s and friend_id = %s )
                        '''
        a_affectedRows = [0]
        DbManager.instance().Execute(s_sql, (d_userId, d_friendId, d_friendId, d_userId), 'town_community', True, [], False, a_affectedRows)

        return a_affectedRows[0] >= 1

    @staticmethod
    def cancel_request(d_userId, d_friendId):
        s_sql = '''delete from friends 
                    where 
                        user_id = %s 
                        and friend_id = %s 
                        and status='pending'
                        '''
        a_affectedRows = [0]
        DbManager.instance().Execute(s_sql, (d_userId, d_friendId), 'town_community', True, [], False, a_affectedRows)

        return a_affectedRows[0] == 1

    @staticmethod
    def deny_request(d_userId, d_friendId):
        s_sql = '''delete from friends
                    where
                        user_id = %s
                        and friend_id = %s
                        and status='pending'
                        '''
        a_affectedRows = [0]
        DbManager.instance().Execute(s_sql, (d_friendId, d_userId), 'town_community', True, [], False, a_affectedRows)

        return a_affectedRows[0] == 1

    @staticmethod
    def get_requests(d_userId):
        s_sql = '''select f.user_id,
                        u.name as user_name,
                        t.name as town_name,
                        up.name as user_pic
                    from friends f
                    left join users u
                        on f.user_id = u.id
                    left join towns t
                        on u.town_id = t.id
                    left join user_pics up
                        on u.id = up.user_id
                            and up.is_main = 1
                    where friend_id = %s
                        and status='pending'
                        '''
        a_result = DbManager.instance().GetDbResult(s_sql, (d_userId,), True)
        return a_result

    @staticmethod
    def get_friends(d_userId):
        s_sql = '''select f.user_id,
                        u.name as user_name,
                        t.name as town_name,
                        up.name as user_pic
                    from friends f
                    left join users u
                        on f.user_id = u.id
                    left join towns t
                        on u.town_id = t.id
                    left join user_pics up
                        on u.id = up.user_id
                            and up.is_main = 1
                    where friend_id = %s
                        and status='accepted'
                        '''
        a_result = DbManager.instance().GetDbResult(s_sql, (d_userId,), True)
        return a_result


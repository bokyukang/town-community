import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
from helpers.db.forum_list import *
from managers.db_manager import DbManager
import mysql

class Towns(DbTable):

    # create table town_forums(
    #      id int primary key auto_increment, 
    #      town_id int, forum_list_id int);
    @staticmethod
    def create_forum(d_town_id, s_title, s_type):
        s_sql = 'insert into forum_list(title) values(%s)'
        o_cursor = DbManager.instance().GetCursor()
        o_cursor.execute(s_sql, (s_title,))
        d_forum_list_id = o_cursor.lastrowid
        s_sql = 'insert into town_forums(town_id, forum_list_id, type) values(%s, %s, %s)'
        o_cursor.execute(s_sql, (d_town_id, d_forum_list_id, s_type))
        DbManager.instance().Commit()
        o_cursor.close()
        return d_forum_list_id

    @staticmethod
    def get_default_forum_id(d_town_id):
        d_forum_list_id = DbManager.instance().GetSingleValue('forum_list', 'forum_list_id' ['town_id','type'], [d_town_id,'default'])
        return d_forum_list_id

    @staticmethod
    def get_town_name(d_town_id):
        s_sql = '''select name from towns where id = %s'''
        a_result = DbManager.instance().GetDbResult(s_sql, [d_town_id,], False)
        if len(a_result) > 0:
            return a_result[0][0]

    @staticmethod
    def create_if_not(a_fields, o_properties):
        print('properties', o_properties)
        print('fields', a_fields)
        a_fields = [s_item for s_item in a_fields if s_item in o_properties]
        a_values = [o_properties[s_item] for s_item in a_fields]
        s_sql = '''insert into towns(''' + ','.join(a_fields) + ''')
                    values(''' + ','.join(['%s' for s_item in a_values]) + ')'
        print('sql' , s_sql)
        try:
            d_lastId = DbManager.instance().Execute(s_sql, a_values, 'town_community', True, [], True)
        except mysql.connector.IntegrityError as err:
            pass
        except mysql.connector.errors.IntegrityError as err:
            pass

        s_sql = '''select id from towns where id_0 = %s and id_2 = %s'''

        return DbManager.instance().GetSingleValue('towns', 'id', ['SIG_CD',], [o_properties['SIG_CD'],])


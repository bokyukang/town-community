import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
from helpers.db.user import User
from helpers.db.scores import Scores

class Comments(DbTable):
    @staticmethod
    def add_comment(s_type, d_userId, d_refId, s_content):
        s_sql = ''' insert into comments(type, user_id, ref_id, content, created_at)
                    values(%s,%s,%s,%s,NOW())'''
        d_id = DbManager.instance().Execute(s_sql, (s_type, d_userId, d_refId, s_content), 'town_community', True, [], True)
        return d_id

    @staticmethod
    def del_comment(d_id, d_userId):
        s_sql = ''' delete from comments where id = %s and user_id = %s'''
        DbManager.instance().Execute(s_sql, (d_id, d_userId))

    @staticmethod
    def get_comments(s_type, d_refId, s_sort, d_userId):
        s_orderBy = 'c.created_at desc'
        if s_sort == 'popular':
            s_orderBy = 'reputation desc'
        elif s_sort == 'auto':
            s_orderBy = 'reputation desc'
        s_sql = ''' 
            select c.id, c.type, c.ref_id, c.user_id, c.content, 
                    s.score as reputation, 
                    UNIX_TIMESTAMP(c.created_at) as created_at ,
                    u.name as user_name, up.name as user_pic_url,
                    b.val as vote
                from comments c
                    left join users u
                        on c.user_id = u.id
                    left join user_pics up
                        on u.id = up.user_id
                            and up.is_main = 1
                    left join behaviors b
                        on b.user_id = %s
                            and b.target_id = c.id
                            and b.type='comment'
                    left join scores s
                        on s.target_id = c.id
                            and s.type = 'comment'
                where c.type = %s
                    and c.ref_id = %s
                order by ''' + s_orderBy
        am_result = DbManager.instance().GetDbResult(s_sql, (d_userId, s_type, d_refId), True)
        return am_result

    @staticmethod
    def update_reputation(d_userId, d_refId, d_voteChange):
        d_baseUserReput = 100
        d_voteUnit = 10
        d_userReput = User.get_reputation(d_userId)
        f_modifier = max(d_userReput, 0) / d_baseUserReput
        d_baseReput = 100

        d_score = Scores.get_score('comment', d_refId, d_baseReput)
        #d_recentRep = Votes.get_recent_votes_count('article', d_id, 30)
        d_reputChange = int(d_voteChange * d_voteUnit * f_modifier)
        Scores.set_score('comment', d_refId, d_score + int(d_reputChange))

        return d_reputChange


import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
from managers.db_manager import DbManager
from helpers.db.associations import Associations

class ChatList(DbTable):

    @staticmethod
    def create_new(s_title, s_type, d_typeId, s_tag = None, d_user = None, **kwargs):
        a_fields = ['title', 'user_id', 'type', 'type_id', 'tag']
        a_values = [s_title, d_user, s_type, d_typeId, s_tag]
        if 'description' in kwargs:
            a_fields.append('description')
            a_values.append(kwargs['description'])
        if 'img_url' in kwargs:
            a_fields.append('img_url')
            a_values.append(kwargs['img_url'])

        s_sql = '''insert into chat_list('''  + ','.join(a_fields) + ''')
                    values(''' + ','.join(['%s' for item in a_values]) + ')'
        d_lastInsertId = DbManager.instance().Execute(s_sql, a_values, 'town_community', False, [], True)

        if d_lastInsertId:
            Associations.add_association(s_type, d_typeId, 'chat', d_lastInsertId)
            return d_lastInsertId


        return None

    @staticmethod
    def get_chatrooms(s_ownerType, d_ownerId, s_tag, b_createIfNot = True):
        s_sql = '''select cl.*
                    from chat_list cl
                        left join associations a
                            on a.target_id = cl.id
                    where 
                        tag = %s
                        and a.owner_id = %s
                        and a.owner_type = %s
                        and a.target_type = 'chat'
                        '''
        print((s_tag, d_ownerId,s_ownerType))
        aa_res = DbManager.instance().GetDbResult(s_sql, \
                (s_tag, d_ownerId,s_ownerType), True)
        if len(aa_res) == 0:
            if b_createIfNot:
                s_sqlDelete = '''delete from chat_list 
                                where type = %s
                                    and type_id = %s
                                    and tag = %s '''
                DbManager.instance().Execute(s_sqlDelete, (s_ownerType, d_ownerId, s_tag))
                s_sqlCreate = '''insert into chat_list(type, type_id, tag)
                                    values(%s, %s, %s)'''
                d_lastInsertId = DbManager.instance().Execute(s_sqlCreate, (s_ownerType, d_ownerId, s_tag), 'town_community', False, [], True)
                if d_lastInsertId:
                    Associations.add_association(s_ownerType, d_ownerId, 'chat', d_lastInsertId)
                    aa_res = DbManager.instance().GetDbResult(s_sql, \
                            (s_tag, d_ownerId,s_ownerType), True)
            else:
                return None
        return aa_res

    @staticmethod
    def get_town_chats(d_town_id):
            
        s_sql = '''select 
                        cl.id, cl.tag as type, cl.title, cl.user_id , cl.description, cl.img_url
                    from towns t
                        left join chat_list cl
                            on cl.type_id = t.id
                                and cl.type = 'town'
                    where t.id = %s'''
        return DbManager.instance().GetDbResult(s_sql, [d_town_id,], True)


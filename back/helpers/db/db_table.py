
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)

from managers.db_manager import DbManager

class DbTable:

    def __init__(self):
        pass

    @staticmethod
    def convBoolean( b_val):
        if b_val is None:
            return None
        if b_val == True:
            return 1
        elif b_val == False:
            return 0
        return None

    @staticmethod
    def convBooleanFromDb( d_val ):
        if d_val is None:
            return None
        return d_val == 1

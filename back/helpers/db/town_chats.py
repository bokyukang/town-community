import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
from managers.db_manager import DbManager
from helpers.db.towns import Towns
from helpers.db.chat_list import ChatList

class TownChats(DbTable):
    @staticmethod
    def get_default_town_chat(d_town_id, b_create_if_not = True):
        s_sql = '''select
                        id
                    from chat_list cl
                    where type_id = %s
                        and tag = 'default'
                        and type = 'town'
                        '''
        a_result = DbManager.instance().GetDbResult(s_sql, [d_town_id,],False )
        if len(a_result) == 0 and b_create_if_not:
            # doesn't exist, create
            d_chat_list_id = ChatList.create_new(Towns.get_town_name(d_town_id), 'town', d_town_id, 'default')
            #TownChats.create_new(d_town_id, d_chat_list_id, 'default')
            
        s_sql = '''select 
                        t.id as town_id, cl.id as chat_list_id, cl.tag as type, cl.title, cl.user_id 
                    from towns t
                        left join chat_list cl
                            on t.id = cl.type_id
                            and cl.type = 'town'
                            and cl.tag = 'default'
                    where t.id = %s'''
        return DbManager.instance().GetDbResult(s_sql, [d_town_id,], True)


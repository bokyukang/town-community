import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
from managers.db_manager import DbManager

class NoteMessage(DbTable):
    @staticmethod
    def send_msg(d_senderId, d_receiverId, s_content):
        s_sql = '''insert into note_message(
                    sender_id, receiver_id, content, created_at, opened)
                   values(%s, %s, %s, now(), 0)'''
        DbManager.instance().Execute(s_sql, (d_senderId, d_receiverId, s_content))

    @staticmethod
    def get_msgs(**kwargs):
        d_rows = kwargs.pop('rows', 10)
        d_page = kwargs.pop('page', 0)
        d_start = (d_page) * d_rows
        d_senderId = kwargs.pop('sender_id', None)
        d_receiverId = kwargs.pop('receiver_id', None)
        b_shorten = kwargs.pop('shorten', True)
        b_opened = kwargs.pop('opened', None)
        d_id = kwargs.pop('id', None)
        b_isCount = kwargs.pop('count', False)
        b_markOpened = kwargs.pop('mark_opened', False)

        m_wheres = {
                'sender_id' : d_senderId,
                'receiver_id' : d_receiverId,
                'opened' : b_opened,
                'nm.id' : d_id,
                }
        m_wheres = dict(filter(lambda elem: elem[1] is not None, m_wheres.items()))

        if b_shorten:
            s_content = 'SUBSTRING(nm.content, 1, 200) as content'
        else:
            s_content = 'nm.content as content'

        if b_isCount:
            s_select = ' count(*)'
            s_orderBy = ' '
            s_limit = ' '
        else:
            s_select = ''' nm.id, ''' + s_content + ''', 
                        DATE_FORMAT(nm.created_at, '%Y-%m-%d %H:%M:%S') as created_at,
                            nm.opened ,
                            u.id as user_id, u.name as user_name, 
                            up.name as user_pic '''
            s_orderBy = ' order by nm.created_at desc'
            s_limit = ' limit %s, %s '

        s_table = '''note_message nm
                        left join users u 
                            on nm.sender_id = u.id
                        left join user_pics up
                            on up.user_id = u.id
                                and up.is_main = 1'''
        
        s_where = ' and '.join([s_item + '=%s' for s_item in m_wheres.keys()])



        if b_markOpened:
            s_sql = 'update ' + s_table + '''
                    set nm.opened=1 where
                    ''' + s_where 
            a_args = list(m_wheres.values())
            print('sql', s_sql, a_args)
            DbManager.instance().Execute(s_sql, a_args, 'town_community', False)

        s_sql = 'select ' + s_select + ''' 
                    from ''' + s_table + ''' 
                    where ''' + s_where + s_orderBy + s_limit

        if b_isCount:
            return DbManager.instance().GetCount(s_sql, list(m_wheres.values()))

        else:
            return DbManager.instance().GetDbResult(s_sql, 
                    ( list(m_wheres.values()) + [ d_start, d_rows ] ), True )
        


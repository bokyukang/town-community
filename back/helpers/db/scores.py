import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
import mysql
from helpers.db.behaviors import Behaviors

class Scores(DbTable):

    @staticmethod
    def get_base(s_type):
        if s_type == 'user-vote':
            return 100
        if s_type == 'article':
            return 100
        if s_type == 'comment':
            return 100
        return 100

    @staticmethod
    def calc_from_behaviors(s_type, d_targetId, d_days = None, d_base=100):
        d_sum = Behaviors.get_sum(s_type, d_targetId, d_days)
        Scores.set_score(s_type, d_targetId, d_sum + d_base)

    @staticmethod
    def set_score(s_type, d_targetId, d_score):
        s_sql = '''
            insert into scores(type, target_id, score)
                values(%s, %s, %s)
            on duplicate key update
                target_id = %s,
                score = %s
        '''
        DbManager.instance().Execute(s_sql, (s_type, d_targetId, d_score, d_targetId, d_score))

    @staticmethod
    def get_score(s_type, d_targetId, d_baseReput = 0):
        s_sql = '''
            select score from scores
                where type = %s
                    and target_id = %s
                    '''
        aa_res = DbManager.instance().GetDbResult(s_sql, (s_type, d_targetId))
        if len(aa_res) == 0:
            return d_baseReput
        return aa_res[0][0]

if __name__ == '__main__':
    Behaviors.add_behavior(2, 'test', 1, 1);
    Behaviors.add_behavior(3, 'test', 1, 2);
    Scores.calc_from_behaviors('test', 1)

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
from managers.db_manager import DbManager

class ChatEntries(DbTable):

    @staticmethod
    def create_new(d_chatId, d_userId, s_type, s_content, a_imgs):
        s_sql = '''
            insert into chat_entries(chat_id, user_id, type, content, written_at)
            values(%s, %s, %s, %s, NOW()) '''

        d_lastRow = DbManager.instance().Execute(s_sql, (d_chatId, d_userId, s_type, s_content), 'town_community', True, [], True)
        s_sql = '''
            insert into chat_entry_imgs(url, chat_entry_id) 
            values(%s, %s)'''
        for s_img in a_imgs:
            DbManager.instance().Execute(s_sql, (s_img, d_lastRow))

    # 이 방의 채팅 내역에 존재하는 모든 이용자들 정보 return
    @staticmethod
    def get_users(d_chatId, d_entries = 100):
        s_sql = '''
            select distinct u.id as id, u.name as name, 
                u.town_id as town_id, up.name as profile_path
            from
                chat_entries ce
                left join users u 
                    on ce.user_id = u.id
                left join user_pics up 
                    on u.id = up.user_id 
                        and up.is_main = 1
            where ce.chat_id = %s
            order by ce.written_at desc
            limit %s
            '''
        a_result = DbManager.instance().GetDbResult(s_sql, (d_chatId, d_entries), True)
        return a_result

    @staticmethod
    def get_entries(d_chatId, d_entries = 100):
        s_sql = '''
            select id, user_id, 
                type,
                content, 
                DATE_FORMAT(written_at, '%Y-%m-%d %T') as written_at
            from chat_entries
            where chat_id = %s
            order by  written_at desc
            limit %s
            '''

        a_entries = DbManager.instance().GetDbResult(s_sql, (d_chatId, d_entries), True)
        m_entries = { item['id'] : item for item in a_entries }
        a_ids = [ str(item['id']) for item in a_entries ]

        if len(a_ids) > 0:
            s_sql = '''
                select cei.url, cei.chat_entry_id, ce.id as entry_id
                from chat_entry_imgs cei
                    left join chat_entries ce
                        on cei.chat_entry_id = ce.id
                where ce.id in ''' + '(' + ','.join(a_ids) + ')'
            a_imgs = DbManager.instance().GetDbResult(s_sql, ( ), True)
            for o_img in a_imgs:
                o_entry = m_entries[ o_img['entry_id'] ] 
                if not 'imgs' in o_entry:
                    o_entry['imgs'] = []
                o_entry['imgs'].append(o_img['url'])
        return a_entries


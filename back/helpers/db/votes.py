import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *

class Votes(DbTable):

    def get_reput(d_isUp):
        if d_isUp is None:
            return 0
        if d_isUp == 0:
            return -1;
        if d_isUp == 1:
            return 1
        return 0

    def vote(d_userId, s_type, d_refId, b_isUp):
        d_before = Votes.get_reput(Votes.get_vote(d_userId, s_type, d_refId))
        d_next = Votes.get_reput(DbTable.convBoolean(b_isUp))
        print('vote ', d_before, d_next)
        s_sql = '''
            insert into votes(user_id, type, is_up, ref_id, created_at)
                values(%s, %s, %s, %s, now())
            on duplicate key update
                is_up = %s
                '''
        DbManager.instance().Execute(
                s_sql, (d_userId, s_type, DbTable.convBoolean(b_isUp), d_refId, DbTable.convBoolean(b_isUp)))
        return d_next - d_before

    def get_vote(d_userId, s_type, d_refId):
        s_sql = '''
            select is_up from votes
                where user_id = %s
                    and type = %s
                    and ref_id = %s
                    '''
        aa_res = DbManager.instance().GetDbResult(s_sql, (d_userId, s_type, d_refId), False)
        if len(aa_res) == 0 :
            return None

        return DbTable.convBooleanFromDb(aa_res[0][0])

    def get_vote_sum(s_type, d_refId, d_days, d_isUp):
        s_sql = '''select count(*) from votes 
                where type = %s 
                    and ref_id = %s 
                    and is_up = ''' + str(d_isUp) + '''
                    and created_at >= DATE_SUB(NOW(), INTERVAL ''' + str(d_days) + ''' DAY)'''
        return DbManager.instance().GetCount(s_sql, (s_type, d_refId))

    def get_recent_votes_count(s_type, d_refId, d_days):
        d_down = Votes.get_vote_sum(s_type, d_refId, d_days, 0)
        d_up = Votes.get_vote_sum(s_type, d_refId, d_days, 1)
        return d_up - d_down

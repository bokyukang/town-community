
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)

from helpers.db.db_table import *
from managers.db_manager import DbManager


class Participants(DbTable):

    @staticmethod
    def get_participants(s_type, d_targetId):
        s_sql = ''' select user_id, at from participants 
                    where type = %s 
                        and target_id = %s '''
        return DbManager.instance().GetDbResult(s_sql, (s_type, d_targetId), True)

    @staticmethod
    def add_participant(d_userId, s_type, d_targetId):
        s_sql = ''' insert into participants(user_id, type, target_id, at)
                    values(%s, %s, %s, NOW()) '''
        return DbManager.instance().Execute(s_sql, (d_userId, s_type, d_targetId))

    @staticmethod
    def remove_participant(d_userId, s_type, d_targetId):
        s_sql = ''' delete from participants
                    where user_id = %s 
                        and type = %s
                        and target_id = %s '''
        return DbManager.instance().Execute(s_sql, (d_userId, s_type, d_targetId))


if __name__ == '__main__':
    Participants.add_participant(1, 'test', 2)
    Participants.add_participant(2, 'test', 2)
    print(Participants.get_participants('test',2))
    Participants.remove_participant(2, 'test', 2)
    print(Participants.get_participants('test',2))

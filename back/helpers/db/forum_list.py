import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
from managers.db_manager import DbManager
from helpers.db.associations import Associations

class ForumList(DbTable):

    @staticmethod
    def get_default_forum(s_type, d_type_id, b_create_if_not = True, s_name=''):

        s_sql = '''select
                        *
                    from forum_list
                    where type_id = %s
                        and type = %s
                        and tag = 'default'
                        '''
        a_result = DbManager.instance().GetDbResult(s_sql, [d_type_id, s_type],False )
        if len(a_result) == 0 and b_create_if_not:
            # doesn't exist, create
            d_forum_list_id = ForumList.create_new(s_name, s_type, d_type_id, 'default')
            #TownForums.create_new(d_town_id, d_forum_list_id, 'default')
            
        return DbManager.instance().GetDbResult(s_sql, [d_type_id,s_type], True)[0]

    @staticmethod
    def get_forums(s_ownerType, d_ownerId, s_tag):
        s_sql = '''select fl.*
                from forum_list fl
                    left join associations a
                        on a.target_id = fl.id
                where 
                    tag = %s
                    and a.owner_id = %s
                    and a.owner_type = %s
                    and a.target_type = 'forum'
                    '''
        return DbManager.instance().GetDbResult(s_sql, ( s_tag, d_ownerId, s_ownerType )
                , True)

    @staticmethod
    def create_new(s_title, s_type, d_typeId, s_tag = None, d_user = None, **kwargs):
        a_fields = ['title', 'user_id', 'type', 'type_id', 'tag']
        a_values = [s_title, d_user, s_type, d_typeId, s_tag]
        if 'description' in kwargs:
            a_fields.append('description')
            a_values.append(kwargs['description'])
        if 'img_url' in kwargs:
            a_fields.append('img_url')
            a_values.append(kwargs['img_url'])

        s_sql = '''insert into forum_list('''  + ','.join(a_fields) + ''')
                    values(''' + ','.join(['%s' for item in a_values]) + ')'
        d_lastInsertId = DbManager.instance().Execute(s_sql, a_values, 'town_community', False, [], True)


        if d_lastInsertId:
            Associations.add_association(s_type, d_typeId, 'forum', d_lastInsertId)
            return d_lastInsertId

        return None

    @staticmethod
    def get_town_forums(d_town_id):
            
        s_sql = '''select 
                        t.id as town_id, fl.id, fl.tag as type, fl.title, fl.user_id, fl.img_url, fl.description
                    from towns t
                        left join forum_list fl
                            on t.id = fl.type_id
                            and type='town'
                    where t.id = %s'''
        return DbManager.instance().GetDbResult(s_sql, [d_town_id,], True)

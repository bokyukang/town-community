import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *
from helpers.db.article_imgs import *
from helpers.db.user import User
from managers.file_manager import FileManager
from helpers.db.votes import Votes
from helpers.db.scores import Scores

class ForumArticles(DbTable):

#    @staticmethod
#    def add_new_article( d_forum_list_id, d_user_id, s_title, s_contents):
#        s_sql = '''
#            insert into forum_articles(forum_list_id, user_id, title, content, created_at)
#            values(%s, %s, %s, %s, NOW())
#            '''
#        DbManager.instance().Execute(s_sql, (d_forum_list_id, d_user_id, s_title, s_contents))

    def add_new_article( d_forum_list_id, d_user_id, s_title, s_contents, s_summary, s_user_name, a_imgs):
        s_sql = '''
            insert into forum_articles(forum_list_id, user_id, title, content, summary, user_name, created_at)
            values(%s, %s, %s, %s, %s, %s, NOW())
            '''
        a_args = (d_forum_list_id, d_user_id, s_title, s_contents, s_summary, s_user_name)
        o_cursor = DbManager.instance().GetCursor(False)
        print(a_args)
        o_cursor.execute(s_sql, a_args)
        d_last_insert_id = o_cursor.lastrowid
        o_cursor.close()
        DbManager.instance().GetDb().commit()

        # img urls
        ArticleImgs.update_imgs( d_last_insert_id, a_imgs )
        return d_last_insert_id

    def save_article( d_id, s_title, s_contents, s_summary, d_user_id, s_user_name, a_imgs):
        s_sql = '''
            update forum_articles
                set title = %s, 
                    content = %s, 
                    summary = %s, 
                    user_id = %s, 
                    modified_at = NOW(), 
                    user_name = %s
            where id = %s
            '''
        DbManager.instance().Execute(s_sql, ( s_title, s_contents, s_summary, d_user_id,  s_user_name, d_id))
        # img urls
        ArticleImgs.update_imgs( d_id, a_imgs )

    def delete_article(d_id, d_userId):
        s_sql = '''
            delete from forum_articles
                where id = %s
                '''
        a_args = [d_id,]

        if d_userId:
            s_sql += ' and user_id = %s'
            a_args.append(d_userId)

        a_affectedRows = [0]
        DbManager.instance().Execute(s_sql, a_args, 'town_community', True, [], False, a_affectedRows)
        if a_affectedRows[0] > 0:
            am_articleImgs = ArticleImgs.get_imgs(d_id)
            for m_articleImg in am_articleImgs:
                print('deleting ' + m_articleImg['img_url'])
                FileManager.delete_file(m_articleImg['img_url'])
            ArticleImgs.del_all_in_article(d_id)
            return True
        return False


    @staticmethod
    def get_article( d_article_id ):
        s_sql = '''
            select fa.id, 
                fa.forum_list_id as forum_list_id,
                fa.title as title,
                UNIX_TIMESTAMP(fa.created_at) as created_at,
                fa.user_id as user_id,
                fa.content as content,
                u.name as user_name,
                up.name as user_pic_url,
                s.score as reputation
            from forum_articles fa
                left join users u
                    on fa.user_id = u.id
                left join user_pics up
                    on fa.user_id = up.user_id
                        and up.is_main = 1
                left join scores s
                    on fa.id = s.target_id
                        and s.type = 'article'
            where fa.id = %s 
            '''
        a_result = DbManager.instance().GetDbResult(s_sql, (d_article_id,), True)
        if len(a_result) > 0:
            o_result = a_result[0]
            a_imgs = ArticleImgs.get_imgs(d_article_id)
            o_result['img_urls'] = a_imgs
            return o_result
        return None

    @staticmethod
    def get_forum_articles( d_forum_list_id, d_start_idx, d_count, s_orderBy ):
        s_sql = '''
            select f.id, 
                f.title, 
                f.summary as summary,
                UNIX_TIMESTAMP(f.created_at) as created_at,
                u.id as user_id,
                u.name as user_name,
                up.name as user_pic_url,
                ai.img_url as img_url,
                ai.type as img_type
            from forum_articles f
                left join users u 
                    on f.user_id = u.id
                left join user_pics up 
                    on u.id = up.user_id
                    and up.is_main = 1 
                left join article_imgs ai
                    on f.id = ai.article_id
                        and ai.is_main = 1
            where f.forum_list_id = %s
            order by ''' + s_orderBy + '''
            limit %s, %s'''

        a_dbResult = DbManager.instance().GetDbResult(s_sql, (d_forum_list_id, d_start_idx, d_count), True)
        print('order by ', s_orderBy)
        print(a_dbResult)
        return a_dbResult

    @staticmethod
    def get_user_articles( d_user_id, d_start_idx, d_count, s_order_by = 'created_at desc'):
        s_sqlMiddle = '''
            from forum_articles fa
                left join forum_list fl on fa.forum_list_id = fl.id
                left join town_forums tf on fl.id = tf.forum_list_id
                left join towns t on tf.town_id = t.id
                left join users u on fa.user_id = u.id
            where u.id = %s 
            ''' 
        s_sql = '''
            select fa.id,
                fa.title,
                UNIX_TIMESTAMP(fa.created_at) as created_at,
                fl.title as forum_title,
                t.SIG_KOR_NM as town_name
                '''  \
                + s_sqlMiddle + ' order by ' + s_order_by \
                + ''' limit %s, %s '''
        a_dbResult = DbManager.instance().GetDbResult(s_sql, (d_user_id, d_start_idx, d_count), True)
        s_sqlCount = 'select count(*) ' + s_sqlMiddle
        d_count = DbManager.instance().GetCount(s_sqlCount, (d_user_id,))
        return a_dbResult, d_count
            
    @staticmethod
    def get_forum_articles_count( d_forum_list_id ):
        s_sql = '''
            select count(*) as count
            from forum_articles f
            where f.forum_list_id = %s
            '''

        d_count = DbManager.instance().GetCount(s_sql, (d_forum_list_id,))
        return d_count

    @staticmethod
    def update_reputation(d_userId, d_id, d_voteChange):
        d_baseUserReput = 100
        d_voteUnit = 10
        d_userReput = User.get_reputation(d_userId)
        f_modifier = max(d_userReput, 0) / d_baseUserReput
        d_baseReput = 100

        d_score = Scores.get_score('article', d_id, d_baseReput)
        #d_recentRep = Votes.get_recent_votes_count('article', d_id, 30)
        d_reputChange = int(d_voteChange * d_voteUnit * f_modifier)
        Scores.set_score('article', d_id, d_score + int(d_reputChange))
        return d_reputChange



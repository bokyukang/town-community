import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)

from helpers.db.db_table import *
from managers.file_manager import FileManager

class UserPics(DbTable):

    @staticmethod
    def get_pic_names( d_user_id ):

        a_wheres = []
        a_whereVals = []

        a_wheres.append('b.id = %s')
        a_whereVals.append(d_user_id)

        s_sql = '''select a.id as id, a.name as name, a.is_main as is_main
                        from user_pics a 
                            left join users b
                                on a.user_id = b.id
                        where
                        ''' + ' and '.join(a_wheres)

        a_dbResult = DbManager.instance().GetDbResult(s_sql, a_whereVals, True)

        return a_dbResult

    @staticmethod
    def get_file_name(d_id):
        s_sql = ''' select name from user_pics where id = %s '''
        a_dbResult =  DbManager.instance().GetDbResult(s_sql, [d_id])
        if len(a_dbResult) > 0 :
            return a_dbResult[0][0]
        return None

    @staticmethod
    def save_pic(d_user_id, s_pic_path):

        s_sql = '''insert into user_pics(user_id, name) 
                    values(%s, %s)'''
        
        return DbManager.instance().Execute(s_sql, [ d_user_id, s_pic_path ])

    @staticmethod
    def set_main(d_user_id, d_id):
        s_sql = '''update user_pics set is_main = 0 
                    where user_id = %s'''
        DbManager.instance().Execute(s_sql, [d_user_id])
        s_sql = '''update user_pics set is_main = 1 
                    where id = %s'''
        DbManager.instance().Execute(s_sql, [d_id])

    @staticmethod
    def delete_pic(d_id):
        s_name = UserPics.get_file_name(d_id)
        try:
            FileManager.delete_file( s_name )
        except Exception:
            pass
        s_sql = '''delete from user_pics where id = %s'''
        DbManager.instance().Execute(s_sql, [d_id])


if __name__ == '__main__':
    o = UserPics()
    o.get_pic_names(29)

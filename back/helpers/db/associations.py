import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
from helpers.db.db_table import *

class Associations(DbTable):

    @staticmethod
    def add_association(s_ownerType, d_ownerId, s_targetType, d_targetId):
        s_sql = '''insert into associations(owner_type, owner_id, target_type, target_id)
            values(%s,%s,%s,%s)'''
        print(s_sql)
        DbManager.instance().Execute(s_sql, (s_ownerType, d_ownerId, s_targetType, d_targetId))
        

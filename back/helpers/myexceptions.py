class NotLoggedInException(Exception):
    pass

class NoDataFound(Exception):
    pass

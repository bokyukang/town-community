import requests
from osmtogeojson import osmtogeojson

URL="http://localhost:1001/api/interpreter"

class BoundaryHelper:

    _instance = None

    @staticmethod
    def instance():
        if not BoundaryHelper._instance:
            BoundaryHelper._instance = BoundaryHelper()
        return BoundaryHelper._instance

    def getBoundaries(self,o_bbox, d_zoomLevel):
        s_bbox = ','.join([
                str(o_bbox['_southWest']['lat']),
                str(o_bbox['_southWest']['lng']),
                str(o_bbox['_northEast']['lat']),
                str(o_bbox['_northEast']['lng'])
                ])
        s_query = '''
                [out:json];
                relation(%s)[boundary=administrative][admin_level=6];
                (._;>;);
                out geom center;
                ''' % (s_bbox,)

        o_res = requests.get(URL, params={'data': s_query})

        o_geojson = osmtogeojson.process_osm_json(o_res.json())

        return o_geojson

import cv2

class ImageHelper:

    @staticmethod
    def resize(d_maxSize, s_path):
        o_img = cv2.imread(s_path)
        d_s = max(o_img.shape[0], o_img.shape[1])
        if d_s > d_maxSize:
            f_scale = d_maxSize /d_s
            o_newImg = cv2.resize(o_img, (int(o_img.shape[1] * f_scale), int(o_img.shape[0] * f_scale)), interpolation = cv2.INTER_AREA)
            cv2.imwrite(s_path, o_newImg)
            return True
        return False


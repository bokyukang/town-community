import random
from datetime import datetime

ALLCHAR = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

class GeneralHelper:


    @staticmethod
    def generate_random_str(d_len):

        d_charLen = len(ALLCHAR)

        s_val = ''
        for i in range(0, d_len):
            c = ALLCHAR[int(random.random() * d_charLen) % d_charLen]
            s_val += c

        return s_val

    @staticmethod
    def datetimeToStr(o_datetime):
        if o_datetime:
            return o_datetime.strftime('%Y-%m-%d %H:%M')
        return None

    @staticmethod
    def getTodayStr():
        return datetime.today().strftime('%Y-%m-%d')

if __name__ == '__main__':
    print(GeneralHelper.generate_random_str(10))


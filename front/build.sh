mkdir ./dist_temp
mkdir ./dist
ng build --configuration production --aot --output-path ./dist_temp 
cp ./dist_temp/* ./dist/ --recursive
cp ./dist/* ~/production/town-community/front/dist/ --recursive

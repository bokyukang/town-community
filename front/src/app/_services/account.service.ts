import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import {  tap, catchError, retry } from 'rxjs/operators';
import { User, UserProfile } from '@app/_models';
import { AccountDataService } from '@app/_services/account-data.service';
import { FriendsService } from '@app/_services/friends.service';
import { NoteMessageService } from '@app/_services/note-message.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

    public static ENDPOINT = '/api/account'; 

  constructor(private httpClient:HttpClient, 
              private accountDataService:AccountDataService,
              private friendsService:FriendsService,
              private noteMessageService:NoteMessageService
             )
  { 
      this.reloadUser().subscribe() ;
  }

  signUp(s_email, s_pwd, s_userName, s_birthDate):Observable<any[]> {
      return this.httpClient.post<any[]>(AccountService.ENDPOINT + '/signup',
          {
                  email :s_email,
                  pwd: s_pwd,
                  userName:s_userName,
                  birthDate:s_birthDate
          });  
  }

  signIn(s_email, s_pwd):Observable<any[]> {
      return this.httpClient.post<any[]>(
          AccountService.ENDPOINT + '/signin',
          {
              email :s_email,
              pwd: s_pwd,
          },
          {
              headers: new HttpHeaders(
                  { 'Content-Type': 'application/json' })
          }
      )
          .pipe(
              tap(
                  data => 
                  {
                      console.log('login data = ' + JSON.stringify(data));
                      this.accountDataService.userSubject.next( data['user']);
                      localStorage.setItem('token', data['token']);
                      this.friendsService.loadAll();
                      this.noteMessageService.getNewMsgs();
                      return data;
                  },
                  error =>
                  {
                      console.log('error' + JSON.stringify(error));
                      return error;
                  }
              )
          );

  }

  pwdReset(s_email):Observable<string> {
      return this.httpClient.post<string>(
          AccountService.ENDPOINT + '/pwdreset',
          {
              email: s_email,
          }
      );
  }

  pwdResetConfirm(s_token, s_pwd):Observable<string>{
      return this.httpClient.post<string>(
          AccountService.ENDPOINT + '/pwdreset_confirm',
          {
              token: s_token,
              pwd: s_pwd
          }
      );
  }

  signOut():Observable<any[]> {
      return this.httpClient.post<any[]>(
          AccountService.ENDPOINT + '/signout',
          {}
      )
      .pipe(
          tap(
              data =>
              {
                  console.log('logged out');
                  this.accountDataService.userSubject.next(null);
                  localStorage.removeItem('token');
                  return data;
              }
          )
      );
  }

  checkSignedIn():Observable<any[]> {
      console.log('checking signed in');
      return this.httpClient.post<any[]>(
          AccountService.ENDPOINT + '/check_signed_in',
          {
          });

  }

  reloadUser():Observable<any[]> {
      return this.httpClient.post<any[]>(
          AccountService.ENDPOINT + '/reload_user',
          {
          })
      .pipe(
          tap(

              data =>
              {
                  this.accountDataService.userSubject.next( data['user'] );
                  return data;
              }
          )
      );

  }

  getProfileImgs():Observable<any[]> {
      return this.httpClient.post<any[]>(
          AccountService.ENDPOINT + '/get_profile_imgs',
          {
          });

  }

  getMyProfile():Observable<UserProfile> {
      return this.httpClient.post<UserProfile>(
          AccountService.ENDPOINT + '/get_my_profile',
          {
          });
  }

  /* get another user's profile other then myself */
  getUserProfile(d_user_id):Observable<UserProfile> {
      return this.httpClient.post<UserProfile>(
          AccountService.ENDPOINT + '/get_user_profile', {
              'user_id' : d_user_id
          });
  }
  getUser(d_user_id):Observable<User> {
      return this.httpClient.post<User>(
          AccountService.ENDPOINT + '/get_user', {
              'user_id' : d_user_id
          });
  }

  uploadImg(o_pic:File) {
      const o_formData = new FormData();
      o_formData.append('pic', o_pic)
      return this.httpClient.post<any[]>(
          AccountService.ENDPOINT + '/upload_img', o_formData);
  }

  updateField(s_name:string, s_value:string) {
      return this.httpClient.post<any[]>(
          AccountService.ENDPOINT + '/update_field', {
              name: s_name,
              value: s_value
          });
  }

  delImg(id) {
      return this.httpClient.post<any[]>(
          AccountService.ENDPOINT + '/del_img', 
          {
              img_id : id
          }
      );
  }

  setMainImg(id) {
      return this.httpClient.post<any[]>(
          AccountService.ENDPOINT + '/set_main_img', 
          {
              img_id : id
          }
      )
      .pipe(
          tap(
              data =>
              {
                  // todo : reload user (update profile pic on account snippet)
                  this.reloadUser().subscribe();
              }
          )
      );
  }

  test():Observable<any[]> {
      return this.httpClient.post<any[]>(AccountService.ENDPOINT + '/test', {'test':'a1','test2':'a2'});
  }

  vote(d_id, b_up) {
      return this.httpClient.post<any[]>(
          AccountService.ENDPOINT + '/vote',
          {
              user_id: d_id,
              is_up: b_up,
          }
      );
  }

}

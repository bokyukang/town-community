import { Injectable } from '@angular/core';
import { SigninComponent } from '@app/signin/signin.component';
import { Subject, Observable, throwError } from 'rxjs';
import { AccountDataService } from '@app/_services/account-data.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(
      private httpClient:HttpClient,
      private accountDataService:AccountDataService,
  ) { }

  formatDate(o_date:Date) {
      return o_date.getFullYear() + '-' + ( o_date.getMonth() + 1 ) + '-' +
        o_date.getDate() + ' ' + o_date.getHours() + ':' + o_date.getMinutes() + ':' + o_date.getSeconds();
  }

    uploadImg(o_file, s_type):Observable<any[]> {
        const formData = new FormData();
        formData.append('file', o_file, o_file.name);
        formData.append('type', s_type);
        return this.httpClient.post<any[]>(
            '/api/common/upload_img',
                formData
        );
    }
    delImg(s_file):Observable<any[]> {
        return this.httpClient.post<any>(
            'api/common/del_img',
            {
                'file':s_file,
            }
        );
    }

    loadJson(s_url):Observable<any[]> {
        return this.httpClient.get<any[]>(s_url);
    }

    checkUserVariable(s_name, s_val):Observable<any> {
        return this.httpClient.post<any>(
            'api/common/check_user_variable', 
            {
                'name': s_name,
                'val': s_val
            }
        );
    }

    setUserVariable(s_name, s_val):Observable<any> {
        return this.httpClient.post<any>(
            'api/common/set_user_variable', 
            {
                'name': s_name,
                'val': s_val
            }
        );
    }

    decrUserVariable(s_name, d_initialVal):Observable<any> {
        return this.httpClient.post<any>(
            'api/common/decr_user_variable', 
            {
                'name': s_name,
                'initial_val': d_initialVal
            }
        );
    }
    getTimeDiffStr(s_dbTimestamp) {
        const d_timeDiff = new Date().getTime() - s_dbTimestamp;
        if ( d_timeDiff <= 1000 * 60 ) {
            return Math.round(d_timeDiff / 1000).toString() + '초';
        }
        if ( d_timeDiff <= 1000 * 60 * 60 ) {
            return Math.round(d_timeDiff / (1000*60)).toString() + '분';
        }
        if ( d_timeDiff <= 1000 * 60 * 60 * 24) {
            return Math.round(d_timeDiff / (1000*60*60)).toString() + '시간';
        }
        return Math.round(d_timeDiff / (1000*60*60*24)).toString() + '일';
    }
    checkLogin(dialogService) {
        if ( this.accountDataService.userValue == null  ) {
            dialogService.replace(SigninComponent, {
                header: '',
                width: '70%',
                data:{
                },
            });
            return false;
        }
        return true;
    }
    getParticipants(s_type, d_targetId):Observable<Number[]> {
        return this.httpClient.post<Number[]>(
            'api/common/get_participants',
            {
                type: s_type,
                target_id: d_targetId
            }
        );
    }
    participate(s_type, d_targetId):Observable<boolean> {
        return this.httpClient.post<boolean>(
            'api/common/participate',
            {
                type: s_type,
                target_id: d_targetId
            }
        );
    }
    unparticipate(s_type, d_targetId):Observable<boolean> {
        return this.httpClient.post<boolean>(
            'api/common/unparticipate',
            {
                type: s_type,
                target_id: d_targetId
            }
        );
    }

    getScore(s_type, d_targetId):Observable<number> {
        return this.httpClient.post<number>(
            'api/common/get_score',
            {
                type: s_type,
                target_id: d_targetId,
            }
        );
    }

}


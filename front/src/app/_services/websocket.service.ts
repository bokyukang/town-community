import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from '@app/../environments/environment';

export interface SocketMessage {
    op: string;
}

export interface SocketListener {
    onMessage(o_msg);
}


@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

    URL:string = environment.socket_url;// + ':' + environment.ip + '/socket';
    m_listeners = {};
    pendingMsgs = [];

    constructor() { }

    private ws:WebSocket = null;

    public subscribe(s_type:string, o_listener:SocketListener) {
        if ( !(s_type in this.m_listeners) ) {
            this.m_listeners[s_type] = [];
        }
        let a_listeners = this.m_listeners[s_type];
        a_listeners.push(o_listener);
    }

    public onMessage(data) {
        let s_op = data.op;
        if ( s_op in this.m_listeners ) {
            let a_listeners = this.m_listeners[s_op];
            for ( let i=0; i < a_listeners.length; ++i ) {
                a_listeners[i].onMessage(data);
            }
        }
    }

    public onError(err) {
    }

    public onClose() {
    }

    public init():void {
    }

    public connect():void {
        if ( !this.ws ) {
            this.ws = new WebSocket(this.URL);
            this.ws.addEventListener('message', event => {
                this.onMessage(JSON.parse(event.data));
            });
            this.ws.addEventListener('error', err => {
            });
            this.ws.addEventListener('close', () => {
                this.ws.close();
                this.ws = null;
            });
            this.ws.addEventListener('open', () => {
                for ( let i=0;i < this.pendingMsgs.length; ++i ) {
                    this.ws.send(JSON.stringify(this.pendingMsgs[i]));
                }
                this.pendingMsgs = [];
            });
        }
    }

    public send(data):void {
        const s_token = localStorage.getItem('token');
        if ( s_token ) {
            data['token'] = s_token;
        }
        if (this.ws == null || this.ws.readyState !== WebSocket.OPEN) {
            this.connect();
            this.pendingMsgs.push(data);
        }
        else {
            this.ws.send(JSON.stringify(data));
        }
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { LatLngBounds } from 'leaflet';

export enum ModeEnum {
    village='village',
    event='event'
};

export enum MenuEvent {
    add_meetup,
    add_debate,
    add_group,
};

@Injectable({
  providedIn: 'root'
})
export class MapService {

    public static ENDPOINT = '/api/map'; 
    private static ZOOM_INIT = 14;

    public modeSubject:BehaviorSubject<ModeEnum>;
    public modeObservable:Observable<ModeEnum>;

    public boundsSubject:BehaviorSubject<LatLngBounds>;
    public boundsObservable:Observable<LatLngBounds>;

    public b_geojsonBordersSubject:BehaviorSubject<boolean> 
        = new BehaviorSubject<boolean>(true);

    public e_threeIntersects:BehaviorSubject<any[]> = new BehaviorSubject<any[]>(null);
    public e_menuEvent:BehaviorSubject<MenuEvent> = new BehaviorSubject<MenuEvent>(null);

    public sub_zoomLevel:BehaviorSubject<number> = new BehaviorSubject<number>(MapService.ZOOM_INIT);
    public sub_searchDateFrom = new BehaviorSubject<string>('');
    public sub_searchDateTo = new BehaviorSubject<string>('');

    public get modeValue():ModeEnum {
        return this.modeSubject.value;
    }
    public set modeValue(s_val) {
        this.modeSubject.next(s_val);
    }

    public get boundsValue():LatLngBounds {
        return this.boundsSubject.value;
    }
    public set boundsValue(o_bound:LatLngBounds) {
        this.boundsSubject.next(o_bound);
    }
    public get boundsValueJson():Object {
        const sw = this.boundsValue.getSouthWest()
        const ne = this.boundsValue.getNorthEast();
        return {
            'left': sw.lng,
            'bottom': sw.lat,
            'right': ne.lng,
            'top': ne.lat
        };
    }

    public set b_geojsonBorders(b_set) {
        this.b_geojsonBordersSubject.next(b_set);
    }

    public get b_geojsonBorders() {
        return this.b_geojsonBordersSubject.value;
    }

    constructor(private httpClient:HttpClient) { 
         this.modeSubject = new BehaviorSubject<ModeEnum>(ModeEnum.village);
         this.modeObservable = this.modeSubject.asObservable();

         this.boundsSubject = new BehaviorSubject<LatLngBounds>(null);
         this.boundsObservable = this.boundsSubject.asObservable();
    }

    requestBoundaries(o_latLngBounds, d_zoomLevel):Observable<any[]> {
        return this.httpClient.post<any[]>(MapService.ENDPOINT + '/boundaries', {
                bounds:o_latLngBounds,
                zoom_level:d_zoomLevel
        });
    }

}

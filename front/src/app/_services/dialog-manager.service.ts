import { Injectable, Type } from '@angular/core';
import { DialogService, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DialogManagerService {

    a_dialogs:any[] = [];
    a_subjects:any[] = [];
    public o_dialogEvent:Subject<string> = new Subject();

    constructor(private dialogService:DialogService) { }

    open(componentType: Type<any>, config: DynamicDialogConfig, b_isReplace = false) {
        if ( window.innerWidth < 800 ) {
            config.width='100%';
        }
        let o_dialogRef = this.dialogService.open(componentType, config);
        o_dialogRef.onClose.subscribe( () => {
            this.pop();
            this.popSub();
            console.log('popping ' + this.a_dialogs.length);
        });


        this.a_dialogs.push(o_dialogRef);
        this.o_dialogEvent.next('push');

        let o_subject = null;
        if (!b_isReplace) {
            o_subject = new Subject();
            this.a_subjects.push(o_subject);
        }


        console.log('pushing ' + this.a_dialogs.length);

        return o_subject;
    }

    replace(componentType:Type<any>, config: DynamicDialogConfig) {
        this.pop();
        this.open(componentType, config, true);
        this.o_dialogEvent.next('replace');
    }

    close() {
        if ( this.a_dialogs.length > 0 ) {
            this.a_dialogs[ this.a_dialogs.length-1 ].close();
        }
    }

    private pop() {
        if ( this.a_dialogs.length > 0 ) {
            let o_dialogRef = this.a_dialogs.pop();
            o_dialogRef.destroy();
            console.log('popping and destroying ' + this.a_dialogs.length);
            this.o_dialogEvent.next('pop');
        }

    }
    private popSub() {
        if ( this.a_subjects.length > 0 ) {
            let o_subject = this.a_subjects.pop();
            o_subject.next('close');
        }
    }


}

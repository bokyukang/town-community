import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AccountService } from '@app/_services/account.service';
import {  tap, catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TownService {
    public static ENDPOINT = '/api/town';

  constructor(private httpClient:HttpClient,
      private accountService:AccountService
  ) {
  }

  onLoad(o_properties) {
      return  this.httpClient.post<any[]>(TownService.ENDPOINT + '/on_load', {
          properties: o_properties
      });
  }

  onILiveHere(d_townId) {
      return this.httpClient.post<any[]>(
          TownService.ENDPOINT + '/on_ilivehere', {
          town_id: d_townId
      }).pipe(
          tap(
              data=>
              {
                 this.accountService.reloadUser().subscribe();
                 return data;
              }
          )
      );
  }


}

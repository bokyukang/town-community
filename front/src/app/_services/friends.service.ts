import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { tap, catchError, retry } from 'rxjs/operators';
import { Friend, FriendRequest } from '@app/_models/friend';

const ENDPOINT = 'api/friends';

export enum FriendStatus {
    NOT_FRIEND = 'not_friend',
    PENDING = 'pending',
    ACCEPTED = 'accepted',
    REQUESTED = 'requested',
};

@Injectable({
  providedIn: 'root'
}) 
export class FriendsService {

    public a_requestsSubject:BehaviorSubject<FriendRequest[]>;
    public a_requests:Observable<FriendRequest[]>;

    public a_friendsSubject:BehaviorSubject<Friend[]>;
    public a_friends:Observable<Friend[]>;
    public a_votersSubject:BehaviorSubject<number[]>;
    public a_myvotersSubject:BehaviorSubject<number[]>;

  constructor(private httpClient:HttpClient) { 
      this.a_requestsSubject = new BehaviorSubject<FriendRequest[]>(null);
      this.a_requests = this.a_requestsSubject.asObservable();
      this.a_friendsSubject = new BehaviorSubject<Friend[]>(null);
      this.a_friends = this.a_friendsSubject.asObservable();
      this.a_votersSubject = new BehaviorSubject<number[]>(null);
      this.a_myvotersSubject = new BehaviorSubject<number[]>(null);
  }

  requestFriend(d_friendId):Observable<any[]>{
      return this.httpClient.post<any[]>(
          ENDPOINT + '/request_friend',
          {
              'friend_id': d_friendId
          });
  }
  cancelRequest(d_friendId):Observable<any[]>{
      return this.httpClient.post<any[]>(
          ENDPOINT + '/cancel_request',
          {
              'friend_id': d_friendId
          });
  }
  loadRequests():void {
      console.log('loadRequests');
      this.httpClient.post<FriendRequest[]>(
          ENDPOINT + '/get_requests', {}
      ).subscribe(
          a_requests => {
              console.log('friends_request' + JSON.stringify(a_requests));
              this.a_requestsSubject.next(a_requests);
          }
      );
  }
  loadFriends():void {
      this.httpClient.post<Friend[]>(
          ENDPOINT + '/get_friends', {}
      ).subscribe(
          a_friends => {
              console.log('get_friends' + JSON.stringify(a_friends));
              this.a_friendsSubject.next(a_friends);
          }
      );
  }
  loadMyVoters():void {
      this.httpClient.post<number[]>(
          ENDPOINT + '/get_myvoters', {}
      ).subscribe(
          a_voters => {
              this.a_myvotersSubject.next(a_voters);
          }
      );
  }
  loadVoters():void {
      this.httpClient.post<number[]>(
          ENDPOINT + '/get_voters', {}
      ).subscribe(
          a_voters => {
              console.log('voters', a_voters);
              this.a_votersSubject.next(a_voters);
          }
      );
  }
  loadAll():void {
      this.loadRequests();
      this.loadFriends();
      this.loadVoters();
      this.loadMyVoters();
  }
  getFriendStatus(d_friendId):Observable<FriendStatus> {
      return this.httpClient.post<FriendStatus>(
          ENDPOINT  + '/get_status',
          {
              'friend_id': d_friendId
          });
  }
  unfriend(d_friendId):Observable<any[]> {
      return this.httpClient.post<any[]>(
          ENDPOINT  + '/unfriend',
          {
              'friend_id': d_friendId
          }).pipe(
              tap(
                  ()=> {
                      this.loadAll();
                  }
              )
          );
  }
  acceptRequest(d_friendId):Observable<FriendStatus> {
      return this.httpClient.post<FriendStatus>(
          ENDPOINT  + '/accept_request',
          {
              'friend_id': d_friendId
          }).pipe(
              tap(
                  () => {
                      this.loadAll();
                  }
              )
          );
  }
  denyRequest(d_friendId):Observable<any[]> {
      return this.httpClient.post<any[]>(
          ENDPOINT  + '/deny_request',
          {
              'friend_id': d_friendId
          }).pipe(
              tap(
                  () => {
                      this.loadRequests();
                  }
              )
          );
          
  }
}

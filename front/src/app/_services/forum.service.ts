import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, retry } from 'rxjs/operators';
import { ForumListItem } from '@app/_models/forum-list-item';
import { TownForum } from '@app/_models/town-forum';
import { ForumArticle } from '@app/_models/forum-article';
import { Forum } from '@app/_models/forum';
import { Image } from '@app/_models/image';
import { Comment } from '@app/_models/comment';

@Injectable({
  providedIn: 'root'
})
export class ForumService {
    public static ENDPOINT = '/api/forum'; 

  constructor(private httpClient:HttpClient) 
  {
  }

  uploadImg(o_file, s_type = null, s_url = null):Observable<Image> {
      const formData = new FormData();
      if ( o_file ) {
          formData.append('file', o_file, o_file.name);
      }
      if ( s_type ) {
          formData.append('type', s_type);
      }
      if ( s_url ) {
          formData.append('url', s_url);
      }

      return this.httpClient.post<Image>(
          ForumService.ENDPOINT + '/upload_img',
          formData);
  }

  delImg(d_id):Observable<any[]> {
      return this.httpClient.post<any[]>(
          ForumService.ENDPOINT + '/del_img',
          {
              id:d_id,
          });
  }

  // d_id : -1 when saving new
  // d_forum_list_id : d_id 가 없으면 이게 있어야 함.
  saveContents(d_id, d_forum_list_id, s_title, s_html, s_summary, s_user_name, a_imgs):Observable<any[]> {
      return this.httpClient.post<any[]>(
          ForumService.ENDPOINT + '/save_contents',
            {
                'id': d_id,
                'forum_list_id': d_forum_list_id,
                'title': s_title,
                'contents': s_html,
                'summary': s_summary,
                'user_name': s_user_name,
                'imgs': a_imgs,
            }
          );
  }

  getForumArticles(d_forum_list_id, d_start_idx, d_rows, s_sort):Observable<ForumListItem[]> {
      return this.httpClient.post<any[]>(
          ForumService.ENDPOINT + '/get_forum_articles',
            {
                'forum_list_id': d_forum_list_id,
                'start_idx': d_start_idx,
                'rows': d_rows,
                'sort': s_sort
            }
      );
  }

  getUserArticles(d_user_id, d_start_idx, d_rows):Observable<ForumListItem[]> {
      return this.httpClient.post<any[]>(
          ForumService.ENDPOINT + '/get_user_articles',
            {
                'user_id': d_user_id,
                'start_idx': d_start_idx,
                'rows': d_rows
            }
      );
  }

  getArticle(d_id, s_sort='date'):Observable<ForumArticle>
  {
      return this.httpClient.post<ForumArticle>(
          ForumService.ENDPOINT + '/get_article',
          {
              'forum_article_id': d_id,
              'comments_sort': s_sort
          }
      );
  }

  getTownForums(d_town_id):Observable<TownForum[]> {
      return this.httpClient.post<TownForum[]>(
          ForumService.ENDPOINT + '/get_town_forums',
          {
              'town_id': d_town_id
          }
      );
  }
    createForum(o_forum:Forum):Observable<any[]> {
        return this.httpClient.post<any[]>(
            ForumService.ENDPOINT  + '/create_forum',
            o_forum
        );
    }

    delete(d_id):Observable<any[]> {
        return this.httpClient.post<any[]>(
            ForumService.ENDPOINT  + '/delete',
            {
                id: d_id
            }
        );
    }

    vote(d_dir, d_articleId, s_type = 'article'):Observable<any[]> {
        return this.httpClient.post<any[]>(
            ForumService.ENDPOINT + '/vote',
            {
                ref_id:d_articleId,
                type:s_type,
                dir:d_dir
            }
        );
    }

    getComments(d_articleId, s_sort):Observable<Comment[]> {
        return this.httpClient.post<Comment[]>(
            ForumService.ENDPOINT + '/get_comments',
            {
                article_id:d_articleId,
                type:'article',
                sort:s_sort
            }
        );
    }

    addComment(o_comment):Observable<any[]> {
        return this.httpClient.post<any[]>(
            ForumService.ENDPOINT + '/add_comment',
            {
                comment:o_comment
            }
        );
    }

    delComment(d_id):Observable<any[]> {
        return this.httpClient.post<any[]>(
            ForumService.ENDPOINT + '/del_comment',
            {
                id:d_id
            }
        );
    }

    getDefaultForum(s_type, d_typeId):Observable<number> {
        return this.httpClient.post<number> (
            ForumService.ENDPOINT + '/get_default_forum',
            {
                type: s_type,
                type_id: d_typeId,
            }
        );
    }

    getForums(s_type, d_typeId, s_tag):Observable<Forum[]> {
        return this.httpClient.post<Forum[]> (
            ForumService.ENDPOINT + '/get_forums',
            {
                type: s_type,
                type_id: d_typeId,
                tag: s_tag,
            }
        );
    }

}


import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalConfigService {

    public f_imgSizeLimit = 3000000;

  constructor() { }
}

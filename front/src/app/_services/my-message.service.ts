import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class MyMessageService {

  constructor(private messageService:MessageService) { }

  public showSuccess(s_title, s_msg) {
      this.messageService.add({severity: 'success', summary: s_title, detail:s_msg});
  }

  public showInfo(s_title, s_msg) {
      this.messageService.add({severity: 'info', summary: s_title, detail:s_msg});
  }
  public showErr(s_title, s_msg) {
      this.messageService.add({severity: 'error', summary: s_title, detail:s_msg});
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, catchError, retry } from 'rxjs/operators';
import { UtilService } from '@app/_services/util.service';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { NoteMessage } from '@app/_models/note-message';

const ENDPOINT = '/api/note_message';

@Injectable({
  providedIn: 'root'
})
export class NoteMessageService {

    public a_newMsgsSubject:BehaviorSubject<any>;
    public a_newMsgs:Observable<any>;

  constructor(private httpClient:HttpClient, 
      private utilService:UtilService,
  ) {
      this.a_newMsgsSubject = new BehaviorSubject<any>(null);
      this.a_newMsgs = this.a_newMsgsSubject.asObservable();
  }

  sendMsg(s_content, d_receiverId):Observable<any[]> {
      return this.httpClient.post<any[]>(
          ENDPOINT + '/send_msg',
          {
              content: s_content,
              receiver_id: d_receiverId
          }
      );
  }

  uploadImg(o_file):Observable<any[]> {
      return this.utilService.uploadImg(o_file, 'note_message');
  }

  getNewMsgs():void{
      this.httpClient.post<any>(
          ENDPOINT + '/get_msgs',
          { 
              opened:0
          }
      ).subscribe( a_msgs => {
          this.a_newMsgsSubject.next(a_msgs);
      });
  }

  getMsgs(d_rows, d_page):Observable<any[]> {
      return this.httpClient.post<any[]>(
          ENDPOINT + '/get_msgs',
          { 
              rows:d_rows,
              page:d_page
          }
      );
  }

  getMsg(d_id, b_markOpened = false):Observable<NoteMessage> {
      return this.httpClient.post<NoteMessage>(
          ENDPOINT + '/get_msg',
          { 
              id:d_id,
              mark_opened:b_markOpened
          }
      );
  }
}

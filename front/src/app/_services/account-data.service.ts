import { Injectable } from '@angular/core';
import { User } from '@app/_models';
import { Observable, throwError, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountDataService {
    public user:Observable<User>;
    public userSubject:BehaviorSubject<User>;

  constructor() { 
      this.userSubject = new BehaviorSubject<User>(null);
      this.user = this.userSubject.asObservable();
  }
  public get userValue(): User {
      return this.userSubject.value;
  }

}

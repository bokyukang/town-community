import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError , BehaviorSubject, Subject } from 'rxjs';
import { tap, catchError, retry } from 'rxjs/operators';
import { MapService } from '@app/_services/map.service';
import { Event } from '@app/_models/event';
import { EventViewComponent } from '@app/events/event-view/event-view.component';
import { GroupViewComponent } from '@app/events/group-view/group-view.component';
import { DialogManagerService } from '@app/_services/dialog-manager.service';

const ENDPOINT = '/api/event';

export enum EventEnum {
    meetup = 'meetup',
    group = 'group',
};

@Injectable({
  providedIn: 'root'
})
export class EventService {

    public eventsOnMapSubject:BehaviorSubject<Event[]>;
    public eventsOnMapObservable:Observable<Event[]>;
    public eventClicked:Subject<Event>;
    public eventAdded:Subject<Event>;
    public eventOpened:Subject<Event>;
    public selEvents:BehaviorSubject<EventEnum[]>;

    public get eventsOnMapValue():Event[] {
        return this.eventsOnMapSubject.value;
    }

  constructor(
      private httpClient:HttpClient,
      private mapService:MapService,
      private dialogService:DialogManagerService,
  ) { 
      this.eventsOnMapSubject = new BehaviorSubject<Event[]>([]);
      this.eventsOnMapObservable = this.eventsOnMapSubject.asObservable();
      this.selEvents = new BehaviorSubject<EventEnum[]>([]);

      this.eventClicked = new Subject<Event>();
      this.eventAdded = new Subject<Event>();
      this.eventOpened = new Subject<Event>();
  }

  addEvent(o_event:Event):Observable<number> {
      return this.httpClient.post<number>(
          ENDPOINT + '/add_event',
          {
              'event':o_event
          }
      ).pipe(
          tap(
              d_id => {
                  o_event.id = d_id;
                  this.eventAdded.next(o_event);
              }
          )
      );
  }

  getEvent(d_id):Observable<Event> {
      return this.httpClient.post<Event>(
          ENDPOINT + '/get_event',
          {
              id: d_id
          }
      );
  }

  getMyEvents():Observable<Event[]> {
      return this.httpClient.get<Event[]>(
          ENDPOINT + '/get_myevents');
  }

  updateEventsOnMap(a_types, s_from, s_to):void {
      if ( this.mapService.sub_zoomLevel.value >= 13 ) {
          console.log('update events', s_from, s_to);
          this.httpClient.post<any[]>(
              ENDPOINT + '/get_events',
              {
                  'types':a_types,
                  'bounds': this.mapService.boundsValueJson,
                  'dateFrom': s_from,
                  'dateTo': s_to
              }
          ).subscribe(
            (a_events) => {
                this.eventsOnMapSubject.next(a_events);
            }
          );
      }
  }

  getComponent(s_type):any {
    if ( s_type == 'group' ) {
        return GroupViewComponent;
    }
    else {
        return EventViewComponent;
    }
  }

  openEventById(d_eventId) {
      this.getEvent(d_eventId)
        .subscribe(
            o_event => {
                this.openEvent(o_event);
            }
        );
  }
  openEvent(o_event:Event) {
        let c_component = this.getComponent(o_event.type);
        let s_name = '';
        if ( o_event.type == 'group' ) {
            s_name = '그룹';
        }
        else {
            s_name = '이벤트';
        }

        this.eventOpened.next(o_event);
        this.dialogService.open(
            c_component, {
                header:  ' > ' + s_name,
                width: '70%',
                data: {
                    id: o_event.id
                }
            }
        );
  }
}

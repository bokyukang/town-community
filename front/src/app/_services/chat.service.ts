import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, catchError, retry } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { WebsocketService, SocketMessage, SocketListener } from '@app/_services/websocket.service';
import { AccountDataService } from '@app/_services/account-data.service';
import { ChatRoom } from '@app/_models/chat-room';

export interface ChatMessage extends SocketMessage {
    chat_list_id;
    msg?:string;
}

const NAME:string = 'chat';
const ENDPOINT = '/api/chat'; 

@Injectable({
  providedIn: 'root'
})
export class ChatService implements SocketListener{

    m_handlers:{ [index: number]: SocketListener[]} = {};

    constructor(
        private wsService:WebsocketService,
        private accountDataService:AccountDataService,
        private httpClient:HttpClient
    ) {

        this.wsService.subscribe(NAME, this);
    }


    /* handle incoming msgs */
    onMessage(o_data) {
        let d_id = o_data['chat_id'];
        if ( d_id in this.m_handlers ) {
            for ( let i=0; i < this.m_handlers[d_id].length; ++i ) {
                this.m_handlers[d_id][i].onMessage(o_data);
            }
        }
    }

    createChatRoom(o_chatRoom):Observable<any[]> {
        return this.httpClient.post<any[]>(
            ENDPOINT  + '/create_chatroom',
            o_chatRoom
        );
    }

    getChatRooms(s_type, d_typeId, s_tag):Observable<ChatRoom[]> {
        return this.httpClient.post<ChatRoom[]> (
            ENDPOINT + '/get_chatrooms',
            {
                'type': s_type,
                'type_id':d_typeId,
                'tag':s_tag,
            }
        );
    }

    getDefaultChatRoom(s_type, d_typeId ):Observable<number> {
        return this.httpClient.post<number> (
            ENDPOINT + '/get_default_chatroom',
            {
                'type': s_type,
                'type_id':d_typeId,
            }
        );
    }

    uploadImg(o_file):Observable<any[]> {
        const formData = new FormData();
        formData.append('file', o_file, o_file.name);
        return this.httpClient.post<any[]>(
            ENDPOINT + '/upload_img',
            formData);
    }

    subscribe(d_id:number, o_listener:SocketListener) {
        if ( !(d_id in this.m_handlers)) {
            this.m_handlers[d_id] = [];
        }
        this.m_handlers[d_id].push(o_listener);
    }
    unsubscribe(d_id:number, o_listener:SocketListener) {
        let a_handlers = this.m_handlers[d_id];
        for ( let i=0;i < a_handlers.length; ++i ){
            if ( a_handlers[i] === o_listener ) {
                a_handlers.splice(i, 1);
                break;
            }
        }
    }

    send(o_msg) {
        o_msg['op'] = NAME;
        this.wsService.send(o_msg);
    }

    enter(d_chatId):void {

        let o_msg = {
            chat_op : 'enter',
            chat_id : d_chatId,
        };
        this.send(o_msg);
        //this.subject.next(o_msg);

    }

    exit(d_chatId):void {

        let o_msg = {
            chat_op : 'exit',
            chat_id : d_chatId,
        };
        this.send(o_msg);
        //this.subject.next(o_msg);

    }

    content(d_chatId, s_content, a_imgs):void {

        let o_msg = {
            chat_op : 'content',
            chat_id : d_chatId,
            content : s_content,
            imgs : a_imgs
        };
        this.send(o_msg);

    }

}


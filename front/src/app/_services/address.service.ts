import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient, HttpHeaders } from '@angular/common/http';
import { map, of, catchError, tap, Observable, throwError } from 'rxjs';
import proj4 from 'proj4';


const ADDR_URL = '/addrlink/addrLinkApi.do';
const XY_URL = '/addrlink/addrCoordApi.do';
const ADDR_TOKEN = 'U01TX0FVVEgyMDI0MDYxMzA0MjgyNDExNDgzOTI=';
const XY_TOKEN = 'devU01TX0FVVEgyMDIyMDUxMjExMzEzNTExMjU1OTA=';
    

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(
      private httpClient:HttpClient
  ) { 
  }

  getSuggestions(s_address):Observable<any> {
      console.log('getsuggestions', s_address);
      const body = {
              keyword:s_address,
              resultType:'json',
              currentPage:1,
              countPerPage:10,
              confmKey:ADDR_TOKEN,
          };

      var s_url = ADDR_URL + '?';

      for ( var key in body) {
          s_url += key + '=' + body[key] + '&';
      }
      console.log('url', s_url);
      return this.httpClient.get<any>(
          s_url
      ).pipe( catchError(
          this.fixResult
      ));
  }


  fixResult(err: any, caught:Observable<any>):Observable<any>  {
    console.log('[get error]', err);
      var s_res = err.error.text;
      if ( s_res[0] == '(' && s_res[ s_res.length -1 ] == ')' ) {
          return of(JSON.parse(s_res.substring(1, s_res.length-1)));
      }

      return of({});
  }

  getCoords(o_juso):Observable<any> {
      const body  = 
          {
              confmKey:XY_TOKEN,
              admCd:o_juso['admCd'],
              rnMgtSn:o_juso['rnMgtSn'],
              udrtYn:o_juso['udrtYn'],
              buldMnnm:o_juso['buldMnnm'],
              buldSlno:o_juso['buldSlno'],
              resultType:'json',
          };
      var s_url = XY_URL + '?';

      for ( var key in body) {
          s_url += key + '=' + body[key] + '&';
      }
      return this.httpClient.get<any>(
          s_url
      ).pipe( catchError(
          this.fixResult
      )).pipe( map( 
          this.getLatLng.bind(this)
      ));
  }

  getLatLng(o_res) {
      console.log('getlatlng', o_res);
      const a_juso = o_res['results']['juso'];
      if ( a_juso && a_juso.length > 0 ) {
          return this.project( parseFloat(a_juso[0]['entX']), parseFloat(a_juso[0]['entY']));
      }
      return [0,0];
  }

  project(x, y) {
      proj4.defs([ 'EPSG:5179', '+proj=tmerc +lat_0=38 +lon_0=127.5 +k=0.9996 +x_0=1000000 +y_0=2000000 +ellps=GRS80 +units=m +no_defs' ]);

      var grs80 = '+proj=tmerc +lat_0=38 +lon_0=127.5 +k=0.9996 +x_0=1000000 +y_0=2000000 +ellps=GRS80 +units=m +no_defs';
      var wgs84 = proj4.defs("EPSG:4326");
      const p = proj4(grs80, wgs84, [x,y]);
      return p;
  }



}

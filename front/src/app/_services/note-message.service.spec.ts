import { TestBed } from '@angular/core/testing';

import { NoteMessageService } from './note-message.service';

describe('NoteMessageService', () => {
  let service: NoteMessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NoteMessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

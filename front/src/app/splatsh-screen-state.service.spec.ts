import { TestBed } from '@angular/core/testing';

import { SplatshScreenStateService } from './splatsh-screen-state.service';

describe('SplatshScreenStateService', () => {
  let service: SplatshScreenStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SplatshScreenStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

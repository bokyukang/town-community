import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AccountDataService } from '@app/_services/account-data.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private accountDataService: AccountDataService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                // TODO
                // this.accountDataService.logout();
                location.reload();
            }

            const error =  err.statusText;
            return throwError(error);
        }))
    }
}

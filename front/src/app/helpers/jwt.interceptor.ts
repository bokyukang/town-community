import { Injectable } from '@angular/core';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AccountDataService } from '@app/_services/account-data.service';
import { tap } from 'rxjs/operators';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { SigninComponent } from '@app/signin/signin.component';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(
        private accountDataService: AccountDataService ,
              public dialogService:DialogManagerService,
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> 
    {
        let s_token = localStorage.getItem('token');
        console.log('token', s_token);
        if ( s_token ) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${s_token}`,
                }
            });
        }

        let o_response = next.handle(request);
        return o_response.pipe(tap( { error : (d) => {
            if ( d.status == 401 ) {
                this.dialogService.replace(SigninComponent, {
                    header: '',
                    width: '70%',
                    data:{
                    },
                });
            }
        }
        }));

        return o_response;
    }
}

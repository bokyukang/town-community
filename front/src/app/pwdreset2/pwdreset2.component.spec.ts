import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pwdreset2Component } from './pwdreset2.component';

describe('Pwdreset2Component', () => {
  let component: Pwdreset2Component;
  let fixture: ComponentFixture<Pwdreset2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pwdreset2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pwdreset2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

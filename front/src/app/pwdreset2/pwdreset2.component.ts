import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AccountService } from '@app/_services/account.service';
import { ActivatedRoute } from '@angular/router';
import { MyMessageService } from '@app/_services/my-message.service';

@Component({
  selector: 'app-pwdreset2',
  templateUrl: './pwdreset2.component.html',
  styleUrls: ['./pwdreset2.component.css']
})
export class Pwdreset2Component implements OnInit {

    pwdForm:FormControl = new FormControl('');
    pwdConfirmForm:FormControl = new FormControl('');
    formGroup:FormGroup = new FormGroup({
        pwd: this.pwdForm,
        pwdConfirm: this.pwdConfirmForm,
    });
    s_token = '';
    b_modified = false;

    constructor(private accountService:AccountService,
                private messageService:MyMessageService,
                private route: ActivatedRoute
               ) {
                }

  ngOnInit(): void {
      this.s_token = this.route.snapshot.paramMap.get('token');
      console.log('token', this.s_token);
  }

  resetPwd() {
      if ( this.formGroup.status == 'VALID' ) {
            let s_pwd = this.formGroup.controls.pwd.value;
            let s_pwdConfirm = this.formGroup.controls.pwdConfirm.value;
            if ( s_pwd !== s_pwdConfirm ) {
                this.messageService.showErr("실패", "비밀번호가 일치하지 않습니다.");
                return;
            }

            this.accountService.pwdResetConfirm(this.s_token, s_pwd)
                .subscribe({
                    next: (s_res) => {
                        this.b_modified = true;
                        this.messageService.showSuccess('성공', '비밀번호가 변경되었습니다.');
                    },
                    error: (error) => {
                        this.messageService.showSuccess('실패', '비밀번호 변경에 실패하였습니다.');
                    }
                });
      }
  }

}

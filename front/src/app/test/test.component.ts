import { AfterViewInit, ViewChild, ElementRef, Component, OnInit, Renderer2 } from '@angular/core';
import * as L from 'leaflet';
import { FormGroup, FormControl } from '@angular/forms';
import * as Quill  from 'quill';
import ImageUploader from 'quill-image-uploader';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { tap, map, catchError } from 'rxjs/operators';
import { Observable, Subject } from "rxjs";
import { ChatService } from '@app/_services/chat.service';
import { HttpClient } from '@angular/common/http';
import { WebsocketService  } from '@app/_services/websocket.service';
import { MsgListComponent } from '@app/msg/msg-list/msg-list.component';
import { ForumArticleListComponent } from '@app/forum/forum-article-list/forum-article-list.component';
import { ForumContentViewComponent } from '@app/forum/forum-content-view/forum-content-view.component';
import { CreateEventComponent } from '@app/events/create-event/create-event.component';
import { EventViewComponent } from '@app/events/event-view/event-view.component';
import { GroupViewComponent } from '@app/events/group-view/group-view.component';
import { UserProfileComponent } from '@app/profile/user-profile.component';

@Component({
    selector: 'app-test',
    templateUrl: './test.component.html',
    styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

    public messages: Subject<string>;

    constructor(private wsService:WebsocketService,
      private dialogService:DialogManagerService,
    ) { 
    }

    ngOnInit(): void {
      //this.dialogService.open(UserProfileComponent, {
      //    header: ' > ',
      //    width: '70%',
      //    data: {
      //        user_id:31,
      //    }
      //});
    }

    onSubmit():void {
    }

}

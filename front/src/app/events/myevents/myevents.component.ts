import { Component, OnInit } from '@angular/core';
import { EventService } from '@app/_services/event.service';
import { Event } from '@app/_models/event';

@Component({
  selector: 'app-myevents',
  templateUrl: './myevents.component.html',
  styleUrls: ['./myevents.component.css']
})
export class MyeventsComponent implements OnInit {

    a_meetups= null;
    a_groups = null;

  constructor(private eventService:EventService) { }

  ngOnInit(): void {
      this.eventService.getMyEvents().subscribe(
          a_events => {
              console.log('myevents', a_events);
              this.updateEvents(a_events);
          }
      );
  }

  updateEvents(a_events) {
      this.a_groups = a_events.filter( o_event => o_event.type=='group' );
      this.a_meetups = a_events.filter( o_event => o_event.type=='meetup' );
      this.a_meetups.sort((a,b)=> { a.at < b.at } );
  }

  onClick(d_id) {
      this.eventService.openEventById(d_id);
  }

}

import { Component, OnInit } from '@angular/core';
import { Privilege } from '@app/_models/privilege';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { Event, User } from '@app/_models';
import { EventService } from '@app/_services/event.service';
import { UtilService } from '@app/_services/util.service';
import { AccountService } from '@app/_services/account.service';
import { AccountDataService } from '@app/_services/account-data.service';
import { MyMessageService } from '@app/_services/my-message.service';
import { ChatService } from '@app/_services/chat.service';
import { ChatRoom } from '@app/_models/chat-room';
import { ChatRoomComponent } from '@app/chat/chat-room/chat-room.component';
import { ForumService } from '@app/_services/forum.service';
import { ForumArticleListComponent } from '@app/forum/forum-article-list/forum-article-list.component';
import { CreateChatComponent } from '@app/chat/create-chat/create-chat.component';
import { CreateForumComponent } from '@app/forum/create-forum/create-forum.component';
import { Forum } from '@app/_models/forum';

@Component({
  selector: 'app-event-view',
  templateUrl: './event-view.component.html',
  styleUrls: ['./event-view.component.css']
})
export class EventViewComponent implements OnInit {

    d_id:number = null;
    o_event:Event = null;
    o_user:User = null;
    b_participating:boolean;
    a_participants = null;
    a_forums:Forum[] = null;
    a_chatRooms:ChatRoom[] = null;

    constructor(
        public config:DynamicDialogConfig,
        protected dialogService:DialogManagerService,
        protected eventService:EventService,
        protected utilService:UtilService,
        protected accountService:AccountService,
        protected accountDataService:AccountDataService,
        protected myMessageService:MyMessageService,
        protected chatService:ChatService,
        protected forumService:ForumService,
    ) 
    {
    }

    ngOnInit(): void {
        const d_id = this.config.data['id'];
        this.d_id = d_id;
        this.eventService.getEvent(d_id)
            .subscribe(o_event => {
                this.o_event = o_event;
                this.loadOrganizer(o_event.user_id);
            });
        this.updateParticipants();
    }

    getChatRooms():void  {
        this.chatService.getChatRooms('event', this.d_id, 'public')
            .subscribe(
                (a_chatRooms:ChatRoom[]) => {
                    this.a_chatRooms = a_chatRooms;
                }
            );
    }

    getForums():void  {
        this.forumService.getForums('event', this.d_id, 'public')
            .subscribe(
                (a_forums:Forum[]) => {
                    this.a_forums = a_forums;
                }
            );
    }

    protected updateParticipants(): void {
        this.utilService.getParticipants('event', this.d_id)
            .subscribe(
                a_participants => {
                    const d_userId = this.accountDataService.userSubject.subscribe(
                        o_user => {
                            if (o_user) {
                                const d_userId = o_user.id;
                                this.a_participants = a_participants;
                                this.b_participating = a_participants.filter(
                                    o_p => o_p['user_id'] == d_userId
                                ).length > 0;
                            }
                        }
                    );
                }
            );
    }

    loadOrganizer(d_userId) {
        this.accountService.getUser(d_userId).subscribe(
            o_user => {
                this.o_user = o_user;
            }
        );
    }

    participate(event) {
      if ( this.utilService.checkLogin( this.dialogService ) ) {
        this.utilService.participate('event', this.d_id).subscribe(
            b_res => {
                if ( b_res ) {
                    this.myMessageService.showSuccess('성공', '참여되었습니다.')
                    this.updateParticipants();
                }
            }
        );
      }
    }

    unparticipate(event) {
        this.utilService.unparticipate('event', this.d_id).subscribe(
            b_res => {
                if ( b_res ) {
                    this.myMessageService.showSuccess('성공', '참여취소 되었습니다.')
                    this.updateParticipants();
                }
            }
        );
    }

    getPrivilege():Privilege {
        if ( this.b_participating ) {
            return new Privilege();
            
        }
        else {
            return {read:false, write:false};
        }
    }

    openForum(d_id) {
        this.dialogService.open(ForumArticleListComponent, {
            header: this.config.header + ' > 게시판',
            width: '70%',
            data: {
                id:d_id,
                privilege:this.getPrivilege(),
            }
        });
    }

    openChat(d_id) {
      if ( this.utilService.checkLogin( this.dialogService ) ) {
        this.dialogService.open(ChatRoomComponent, {
            header: this.config.header + ' > 대화방',
            width: '70%',
            data: {
                id:d_id,
                privilege:this.getPrivilege(),
            }
        });
      }
    }

    openDefaultChat(event) {
        console.log('open default chat');
        if ( this.utilService.checkLogin( this.dialogService ) ) {
            this.chatService.getDefaultChatRoom('event', this.d_id)
            .subscribe( o_chatRooms => {
                this.dialogService.open(ChatRoomComponent, {
                    header: this.config.header + ' > 대화방',
                    width: '70%',
                    data: {
                        id:o_chatRooms['id'],
                        privilege:this.getPrivilege(),
                    }
                });
            }
          );
        }
    }

    openDefaultForum(event) {
        console.log('open default forum');
        this.forumService.getDefaultForum('event', this.d_id)
            .subscribe( o_forum => {
                console.log('default forum ', JSON.stringify(o_forum));
                this.dialogService.open(ForumArticleListComponent, {
                    header: this.config.header + ' > 게시판',
                    width: '70%',
                    data: {
                        id:o_forum['id'],
                        privilege:this.getPrivilege(),
                    }
                });
            }
        );
    }

    createChat(event) {
        this.dialogService.open(CreateChatComponent, {
            header: this.config.header + ' > 대화방 만들기',
            width: '70%',
            data: {
                type: 'event',
                type_id: this.d_id
            }
        })
        .subscribe(
            () => {
                this.getChatRooms();
            })
        ;
    }

    createForum(event) {
        this.dialogService.open(CreateForumComponent, {
            header: this.config.header + ' > 게시판 만들기',
            width: '70%',
            data: {
                type: 'event',
                type_id: this.d_id
            }
        })
        .subscribe(
            () => {
                this.getForums();
            });
        ;
    }

}

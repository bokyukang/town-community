import { Component, OnInit } from '@angular/core';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { Event, User } from '@app/_models';
import { EventService } from '@app/_services/event.service';
import { UtilService } from '@app/_services/util.service';
import { AccountService } from '@app/_services/account.service';
import { AccountDataService } from '@app/_services/account-data.service';
import { MyMessageService } from '@app/_services/my-message.service';
import { ChatService } from '@app/_services/chat.service';
import { ChatRoom } from '@app/_models/chat-room';
import { ChatRoomComponent } from '@app/chat/chat-room/chat-room.component';
import { EventViewComponent } from '@app/events/event-view/event-view.component';

@Component({
  selector: 'app-group-view',
  templateUrl: './group-view.component.html',
  styleUrls: ['./group-view.component.css']
})
export class GroupViewComponent extends EventViewComponent {


  ngOnInit(): void {
        const d_id = this.config.data['id'];
        this.d_id = d_id;
        this.eventService.getEvent(d_id)
            .subscribe(o_event => {
                this.o_event = o_event;
                this.loadOrganizer(o_event.user_id);
            });
        this.updateParticipants();
        this.getChatRooms();
        this.getForums();
  }

}

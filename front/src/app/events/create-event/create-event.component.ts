import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Event } from '@app/_models/event';
import LatLng from 'leaflet';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { EventService } from '@app/_services/event.service';
import { MyMessageService } from '@app/_services/my-message.service';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css'],
  providers: [MessageService]
})
export class CreateEventComponent implements OnInit {

    s_title:string = '';
    s_address:string = '';
    s_description:string = '';
    s_date:string = null
    s_inputClass='';

    o_latlng:LatLng = null;
    f_lat:number = 0;
    f_lng:number = 0;
    s_type:string = '';
    b_online:boolean = false;

  constructor(
      public config:DynamicDialogConfig,
      private dialogManagerService:DialogManagerService,
      private eventService:EventService,
      private myMessageService:MyMessageService,
  ) { 
      this.o_latlng = this.config.data['latlng'];
      this.f_lat = this.o_latlng.lat;
      this.f_lng = this.o_latlng.lng;
      this.s_type = this.config.data['type'];
  }

  ngOnInit(): void {
  }

  onSubmit(event):void {
      console.log('submitting event', this.s_address);
      this.s_inputClass = 'ng-dirty';
      const o_event:Event = {
          id:-1,
          title: this.s_title,
          type: this.s_type,
          lat: this.f_lat,
          lng: this.f_lng,
          address: this.s_address,
          description: this.s_description,
          at: this.s_date,
          user_id:null,
          created_at:null,
          is_online:this.b_online,
      };
      this.eventService.addEvent(o_event)
          .subscribe( {
              next: (d_rowId) => {
                  this.myMessageService.showSuccess( '성공', '모임을 개설하였습니다.');
                  setTimeout( () => {
                      this.dialogManagerService.close();
                      this.eventService.openEventById(d_rowId);
                  }, 1000);
              } ,
              error: err =>  {
                  if ( err.status == 400 ) {
                      this.myMessageService.showErr( '실패', '필수 항목을 입력해 주세요.');
                  }
                  console.log(JSON.stringify(err));
              }
          });
  }
  onCancel(event):void {
        this.dialogManagerService.close();
  }

}

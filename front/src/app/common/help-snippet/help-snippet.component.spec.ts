import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpSnippetComponent } from './help-snippet.component';

describe('HelpSnippetComponent', () => {
  let component: HelpSnippetComponent;
  let fixture: ComponentFixture<HelpSnippetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HelpSnippetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpSnippetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

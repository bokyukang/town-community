import { Input, Component, OnInit } from '@angular/core';
import { UtilService } from '@app/_services/util.service';

@Component({
  selector: 'app-help-snippet',
  templateUrl: './help-snippet.component.html',
  styleUrls: ['./help-snippet.component.css']
})
export class HelpSnippetComponent implements OnInit {

    b_showHelp = true;

    @Input()
    s_name:string;

    @Input()
    s_key:string;

    @Input()
    s_description:string;

  constructor(private utilService:UtilService) { 
  }

  ngOnInit(): void {
      this.utilService.checkUserVariable(this.s_key, '0')
        .subscribe(
            b_result => {
                this.b_showHelp = !b_result;
            });

  }

  confirm(event):void {
      this.utilService.setUserVariable(this.s_key, '0')
        .subscribe(
            () => {
                this.b_showHelp = false;
            }
        );
  }

}

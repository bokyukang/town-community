import { EventEmitter, Output, Input, Component, OnInit } from '@angular/core';
import { Comment } from '@app/_models/comment';
import { ForumService } from '@app/_services/forum.service';
import { UtilService } from '@app/_services/util.service';
import { AccountDataService } from '@app/_services/account-data.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

    @Input()
    a_comments:Comment[] = null;

    @Output()
    e_onUpVote = new EventEmitter<number>();
    @Output()
    e_onDownVote = new EventEmitter<number>();


    o_userValue = null;

  constructor(private utilService:UtilService,
      private forumService:ForumService,
      private accountDataService:AccountDataService,
  ) { 
      this.accountDataService.user.subscribe(
          o_user => {
              this.o_userValue = o_user;
          });
  }

  ngOnInit(): void {
  }

  onUpVote(o_comment) {
      this.e_onUpVote.emit(o_comment);
  }
  onDownVote(o_comment) {
      this.e_onDownVote.emit(o_comment);
  }

  delComment(d_id) {
      this.forumService.delComment(d_id).subscribe(
          () => {
              this.a_comments = this.a_comments.filter( o_comment => o_comment.id != d_id  );
          }
      );
  }

}


import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { Subject, Observable, throwError } from 'rxjs';
import { UtilService } from '@app/_services/util.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css'],
  providers: [ConfirmationService,],
})
export class ConfirmComponent implements OnInit {

  constructor(
      private confirmationService:ConfirmationService,
      private utilService:UtilService
  ) { }

  ngOnInit(): void {
  }
    decrUserVariable(s_name, d_initialVal, s_msg):Subject<boolean> {
        const subject = new Subject<boolean>();
        this.utilService.decrUserVariable(s_name, d_initialVal).subscribe(
            (b_result) => {
                if ( b_result ) {
                    subject.next(true);
                }
                else {
                    this.confirmationService.confirm({
                        message: s_msg,
                        accept: () => {
                            subject.next(true);
                        }
                    });
                }
            });

        return subject;
    }

}


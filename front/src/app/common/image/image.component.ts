import { Component, OnInit, Input } from '@angular/core';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {

    @Input() s_src:string = null;

  constructor(
          public config: DynamicDialogConfig ,
  ) { 
    if ( this.config.data && this.config.data['src'] ) {
        this.s_src = this.config.data['src'];
    }
  }

  ngOnInit(): void {
  }

}

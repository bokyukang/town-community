import { EventEmitter, Component, OnInit, Input, Output } from '@angular/core';
import { AddressService } from '@app/_services/address.service';


@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

    @Input()
    f_lat:number;
    @Output()
    f_latChange = new EventEmitter<number>();

    @Input()
    f_lng:number;
    @Output()
    f_lngChange = new EventEmitter<number>();

    @Input()
    s_address:string  = '';
    @Output()
    s_addressChange = new EventEmitter<string>();

    s_addressFront:string = '';
    s_addressDetail:string = '';
    a_suggestions:string[] = [];
    ao_suggestions:any[] = [];

  constructor(
      private addressService:AddressService
    ) { }

  ngOnInit(): void {
  }

  getJusoStr(o_juso) {
    return o_juso['roadAddrPart1'] + o_juso['roadAddrPart2'] + o_juso['detBdNmList'];
  }

  scheduled = null;
  addressChange(event) {

      this.addressService.getSuggestions(this.s_addressFront)
          .subscribe(
              res => {
                  const o_res = res;
                  console.log('suggestions', this.s_addressFront, o_res);

                  const a_juso = o_res['results']['juso'];
                  this.a_suggestions = [];
                  if ( a_juso ) {
                    a_juso.forEach(
                        o_juso => {
                        const s_str = this.getJusoStr(o_juso);
                        this.a_suggestions.push(s_str);
                    }
                    );
                    this.ao_suggestions = a_juso;
                  }
              }
          );

  }

  onSelect(event) {
      // find entry
      const o_found = this.ao_suggestions.find(
          o_juso => {
              const s_str = this.getJusoStr(o_juso);
              if (s_str == this.s_addressFront) {
                  return true;
              }
              return false;
          }
      );

      if ( o_found ) {
          // 좌표 반환
          this.addressService.getCoords(o_found)
            .subscribe(a_res => {
                console.log('coords', a_res);
                this.f_lat = a_res[1];
                this.f_lng = a_res[0];
                this.f_latChange.next(a_res[1]);
                this.f_lngChange.next(a_res[0]);
            }
            );
      }
  }

  onFrontChange(event) {
      console.log('onfrontchange', this.s_addressFront);
      this.updateAddr();
  }

  onDetailChange(event) {
      this.updateAddr();
  }

  updateAddr() {
      this.s_address = this.s_addressFront + ' ' + this.s_addressDetail;
      this.s_addressChange.next(this.s_address);
  }

}

import { EventEmitter, Output, Input, Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Image } from '@app/_models/image';
@Component({
  selector: 'app-image-thumb',
  templateUrl: './image-thumb.component.html',
  styleUrls: ['./image-thumb.component.scss']
})
export class ImageThumbComponent implements OnInit {
    @Input() img:Image = null;
    @Input() hasClose:boolean = true;
    @Input() hasCheck:boolean = true;
    @Output() delClick = new EventEmitter<number>();
    @Output() checkClick = new EventEmitter<number>();

  constructor(
      private sanitizer:DomSanitizer,
  ) { }

  ngOnInit(): void {
      console.log('image thumb', this.img.img_url);
  }

  onDelImg(d_id):void {
      this.delClick.emit(d_id);
  }
  onCheckImg(d_id):void {
      this.img.is_main = true;
      this.checkClick.emit(d_id);
  }
    cleanURL(s_url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(s_url);
    }

}

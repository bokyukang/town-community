import { ViewChild, ElementRef, Input, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reputation',
  templateUrl: './reputation.component.html',
  styleUrls: ['./reputation.component.css']
})
export class ReputationComponent implements OnInit {

    @ViewChild('container') o_container:ElementRef = null;

    _d_reputation = 0;
    @Input()
    set d_reputation(val) {
        this._d_reputation = val;
        if ( this.o_container) {
            this.o_container.nativeElement.style.backgroundColor = this.getColor(val);
        }
    }
    get d_reputation() {
        return this._d_reputation;
    }

    getColor(d_rep) {
        if ( d_rep < 50 ) {
            return '#a22';
        }
        if ( d_rep < 120 ) {
            return '#152';
        }
        if ( d_rep < 200 ) {
            return '#181';
        }
        return '#ca0';
    }


  constructor() { }

  ngOnInit(): void {
  }

}

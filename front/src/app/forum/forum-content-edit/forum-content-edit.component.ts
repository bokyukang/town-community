import { AfterViewInit, ViewChild, ElementRef, Component, OnInit, AfterContentInit } from '@angular/core';
import { GlobalConfigService } from '@app/_services/global-config.service';
import { SigninComponent } from '@app/signin/signin.component';
import { FormGroup, FormControl } from '@angular/forms';
import * as Quill  from 'quill';
import ImageUploader from 'quill-image-uploader';
import { ForumService } from '@app/_services/forum.service';
import { tap, map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { ForumArticle } from '@app/_models/forum-article';
import { ForumContentViewComponent } from '@app/forum/forum-content-view/forum-content-view.component';
import { AccountDataService } from '@app/_services/account-data.service';
import { Image } from '@app/_models/image';
import { NoSanitizePipe } from '@app/helpers/nosanitize-pipe';
import { MyMessageService } from '@app/_services/my-message.service';
import BlotFormatter  from 'quill-blot-formatter/dist/BlotFormatter';
import { UtilService } from '@app/_services/util.service';

@Component({
  selector: 'app-forum-content-edit',
  templateUrl: './forum-content-edit.component.html',
  styleUrls: ['./forum-content-edit.component.scss'],
  providers: [ ],
})
export class ForumContentEditComponent implements OnInit, AfterViewInit {


    articleTitle:string = '';

    userName:string = '';

    @ViewChild('title')
    title:ElementRef;

    @ViewChild('editor')
    editor:ElementRef;

    b_submitDisabled:boolean = false;

    private quill:any;
    article:ForumArticle;

    d_id;
    d_forum_list_id;

    a_imgs:Image[] = [];
    o_qlEditor = null;
    o_imgUploader = null;

    s_videoUrl = '';
    b_videoDialog = false;
    d_cursorIndex = 0;
	b_linkDialog = false;
	s_linkUrl = '';
    o_user = null;

    constructor(
        private forumService:ForumService,
      private dialogManagerService:DialogManagerService,
      public config: DynamicDialogConfig,
      private accountDataService:AccountDataService,
      private globalConfig:GlobalConfigService,
      private myMessageService:MyMessageService,
      private utilService:UtilService,
    ) { 
        this.d_id = this.config.data['id'];
        this.d_forum_list_id = this.config.data['forum_list_id'];
        this.o_user = this.accountDataService.userValue;
    }

    ngAfterViewInit() :void {
        this.o_qlEditor = this.editor.nativeElement.getElementsByClassName("ql-editor")[0];
    }
    uploadImg(o_file) {
        const o_domFiles = this.o_imgUploader.fileHolder.files;
        if ( o_domFiles.length > 0 ) {
            const f_fileSize = o_domFiles[0].size;
            console.log(f_fileSize);
            if (f_fileSize > this.globalConfig.f_imgSizeLimit ) 
            {
                this.myMessageService.showErr('파일크기제한', '파일크기가 너무 큽니다. 이미지 사이즈를 줄여서 업로드 해 주세요.');
                return new Promise((resolve, reject) => {
                    reject('파일사이즈');
                });


            }
        }

        return new Promise((resolve, reject) => {
            this.forumService.uploadImg(o_file)
                .pipe(
                    catchError(
                        error => {
                            reject(error);
                            return throwError('erro');
                        })
                ).subscribe(
                    (o_result:Image) => {
                        const d_id = o_result['id'];
                        const s_imgUrl = o_result['img_url'];
                        resolve( s_imgUrl);
                        this.pushImg(o_result);
                        return o_result;
                    }
                );
        });
    }

    ngOnInit(): void {

        //let Font = Quill.import('attributors/style/font');
        //Font.whitelist = ['Nanum Gothic', 'Noto Sans KR','Nanum Brush Script' ];
        //Quill.register(Font, true);

        if ( this.utilService.checkLogin( this.dialogManagerService ) == false ) {
            return;
        }

        Quill.register('modules/imageUploader', ImageUploader);
        Quill.register('modules/blotFormatter', BlotFormatter);

        var bindings = {
        };

        this.quill = new Quill('#editor', 
            {
                modules: { 
                    'toolbar': '#toolbar-container',
                    imageUploader: {
                        upload: this.uploadImg.bind(this)
                    },
                    keyboard: {
                        bindings: bindings
                    },
                    blotFormatter : {
                    },
                },

                theme: 'snow'
            },
        );
        this.o_imgUploader = this.quill.getModule('imageUploader');

        this.quill.on('text-change', this.contentChange.bind(this));
        this.quill.on('selection-change', this.selectionChange.bind(this));
        const o_toolbar = this.quill.getModule('toolbar');
        o_toolbar.addHandler('video', this.showVideo.bind(this));
        o_toolbar.addHandler('link', this.showLink.bind(this));

        this.load();

    }

    showVideo(event) {
        const d_cursor = this.o_qlEditor.selectionStart;
        this.b_videoDialog = true;
    }
    insertVideo() {
        this.b_videoDialog = false;
        const o_selection = this.quill.getSelection();
        let d_index = this.d_cursorIndex;
        if ( o_selection != null ) 
        {
            d_index = o_selection.index;
        }
        const s_url = this.s_videoUrl.replace(/watch\?v=/, '/embed/');
        this.quill.insertEmbed(d_index, 'video', s_url);
        this.forumService.uploadImg(null, 'vid', s_url)
            .subscribe(
                (o_result) => {
                    const d_id = o_result['id'];
                    const s_imgUrl = o_result['img_url'];
                    this.pushImg(o_result);
                    return o_result;
                }
            );
    }

    pushImg(o_img) {
        o_img['is_main'] = this.a_imgs.length == 0;
        this.a_imgs.push(o_img);
    }

    showLink(event) {
        this.b_linkDialog = true;
    }

    insertLink() {
        this.b_linkDialog = false;
        this.quill.format('link', this.s_linkUrl, Quill.sources.USER);
    }

    selectionChange(range, oldRange, source) {
        if (range) {
            this.d_cursorIndex = range.index;
        }
    }

    load():void {
        if(this.d_id && this.d_id != -1 ){
            this.forumService.getArticle(this.d_id)
                .subscribe( (o_article:ForumArticle) => {
                    this.article = o_article;
                    console.log('article', JSON.stringify(o_article));
                    this.a_imgs = o_article.img_urls;
                    if (o_article.title) {
                        this.articleTitle = o_article.title;
                    }
                    else {
                        this.articleTitle = "";
                    }
                    if ( o_article.user_name) {
                        this.userName = o_article.user_name;
                    }
                    const o_delta = this.quill.clipboard.convert(this.article.content);
                    this.quill.setContents(o_delta, 'silent');
                });
        } else {
            this.userName = this.accountDataService.userValue.name;
        }
    }

    filterSummary(s_summary) { 
        const d_textLimit = 300;
        const d_lineLimit = 7;

        let d_lineCount = 0;
        for ( let i=0; i < d_textLimit; ++i ) {
            if ( s_summary[i] == '\n' ) {
                d_lineCount++;
                if ( d_lineCount == d_lineLimit ) {
                    return s_summary.substring(0, i) + '...';
                }
            }
        }
        if ( s_summary.length > d_textLimit ) {
            return s_summary.substring(0, d_textLimit) + '...';
        }
        return s_summary;
    }

    onSubmit():void {
        if ( this.b_submitDisabled ) return;
        let s_content = this.o_qlEditor.innerHTML;
        let s_summary = this.filterSummary(this.quill.getText());
        let s_title = this.title.nativeElement.value;
        let that = this;
        if ( s_content ) {
            this.forumService.saveContents(this.d_id, this.d_forum_list_id, s_title, s_content, s_summary, this.userName, this.a_imgs).subscribe(
                (o_res) => {
                    let d_id = o_res['id'];
                    this.dialogManagerService.replace( ForumContentViewComponent, {
                        data: {
                            id: d_id,
                            forum_list_id:this.d_forum_list_id
                        },
                        header:that.config.header,
                        width:'70%'
                    });
                }
            )
            ;
        }
    }
    getImgIdx(d_id) {
        let idx = -1;
        for ( var i=0; i < this.a_imgs.length; ++i ) {
            if (this.a_imgs[i].id == d_id) {
                idx = i;
            }
        }
        return idx;
    }
    delImg(d_id) {
        this.forumService.delImg(d_id).subscribe(
            b_res => {
                
                const idx = this.getImgIdx(d_id);
                const o_img = this.a_imgs[idx];
                if ( idx >=0 ) {
                    this.a_imgs.splice(idx, 1);
                }
                let s_regex = '';
                if (o_img.type == 'pic' ) {
                    s_regex = '<img[^>]*src="' + o_img.img_url + '[^>]*>';
                }
                else if ( o_img.type == 'vid' ) {
                    s_regex = '<iframe[^>]*src="' + o_img.img_url + '[^>]*>';
                }
                const s_new = this.o_qlEditor.innerHTML.replaceAll(
                    new RegExp(s_regex, 'g'), '');
                this.o_qlEditor.innerHTML = s_new;
            });
    }
    checkImg(d_id) {
        const d_idx = this.getImgIdx(d_id);

        for ( var i=0;i < this.a_imgs.length; ++i ) {
            const o_img= this.a_imgs[i];
            o_img.is_main = d_idx == i;
        }

    }

    contentChange(e) {
        this.checkValid();
    }

    titleChange() {
        this.checkValid();
    }

    checkValid() {
        let b_valid = true;
        let s_title = this.articleTitle;
        if (s_title.trim() == '' ) {
            b_valid = false;
        }
        let s_content = this.o_qlEditor.innerText || this.o_qlEditor.textContent;
        if ( s_content.trim() == '' && this.a_imgs.length == 0 ) {
            b_valid = false;
        }
        this.b_submitDisabled = !b_valid;
    }


}

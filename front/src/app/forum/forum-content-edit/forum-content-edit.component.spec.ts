import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumContentEditComponent } from './forum-content-edit.component';

describe('ForumContentEditComponent', () => {
  let component: ForumContentEditComponent;
  let fixture: ComponentFixture<ForumContentEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForumContentEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumContentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

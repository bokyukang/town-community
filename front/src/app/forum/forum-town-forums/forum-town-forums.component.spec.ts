import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumTownForumsComponent } from './forum-town-forums.component';

describe('ForumTownForumsComponent', () => {
  let component: ForumTownForumsComponent;
  let fixture: ComponentFixture<ForumTownForumsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForumTownForumsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumTownForumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

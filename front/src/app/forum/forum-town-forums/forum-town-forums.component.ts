import { Component, OnInit } from '@angular/core';
import { ForumService } from '@app/_services/forum.service';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { ForumContentViewComponent } from '@app/forum/forum-content-view/forum-content-view.component';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { TownForum } from '@app/_models/town-forum';

@Component({
  selector: 'app-forum-town-forums',
  templateUrl: './forum-town-forums.component.html',
  styleUrls: ['./forum-town-forums.component.css']
})
export class ForumTownForumsComponent implements OnInit {

    a_town_forums: TownForum[];
    d_town_id;

  constructor(private forumService:ForumService,
              private dialogService:DialogManagerService,
              public config: DynamicDialogConfig 
  ) { 
        this.d_town_id = this.config.data['town_id'];
  }

  load():void {
      this.forumService.getTownForums(this.d_town_id).subscribe(
          {
              next: (data) => {
                  console.log('town forums data ' + data);
                  this.a_town_forums = data;
              },
              error: error => {
                  console.log('town forums err ' + error);
              }
          }
      );
  }

  ngOnInit(): void {
  }

}

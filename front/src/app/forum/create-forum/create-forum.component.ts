import { Component, OnInit, ElementRef } from '@angular/core';
import { ForumService } from '@app/_services/forum.service';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { Forum } from '@app/_models/forum';

@Component({
  selector: 'app-create-forum',
  templateUrl: './create-forum.component.html',
  styleUrls: ['./create-forum.component.css'],
  providers: [MessageService]
})
export class CreateForumComponent implements OnInit {

    s_title:string = '';
    s_backImg:string = null;
    s_description:string = '';
    d_typeId:number;
    s_type:string;

  constructor(
      public config:DynamicDialogConfig,
      private forumService:ForumService,
      private messageService:MessageService,
      private dialogService:DialogManagerService
  )
  {
      this.d_typeId = this.config.data['type_id'];
      this.s_type = this.config.data['type'];
  }

  ngOnInit(): void {
  }

    uploadImg():void {
        let input = document.createElement('input');
        input.type = 'file';
        input.accept = 'image/*';
        input.onchange = () => {
            let files = Array.from(input.files);
            this.forumService.uploadImg(files[0]).subscribe(
                (a_files) => {
                    console.log('returned ' + JSON.stringify(a_files));
                    this.s_backImg = a_files[0];
                }
            );
        };
        input.click();
    }

    descriptionType(event) {
    }

    submit(event) {
        if ( this.s_title.trim() == '' ) {
            this.messageService.add({severity: 'fail', summary: '필수항목 누락', detail:'제목을 입력해 주세요.'});
            return;
        }
        let o_forum:Forum = {
            id: -1,
            title: this.s_title,
            type:this.s_type,
            type_id:this.d_typeId,
            description: this.s_description,
            img_url:this.s_backImg,
            user_id:null,
            tag:'public'
        };
        this.forumService.createForum(o_forum).subscribe(
            (o_result) => {
                console.log('returned');
                this.messageService.add({severity: 'success', summary: '개설 완료', detail:'게시판이 개설되었습니다.'});
                setTimeout(() => {
                    this.dialogService.close();
                }, 2000);
            }
        );
    }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumContentViewComponent } from './forum-content-view.component';

describe('ForumContentViewComponent', () => {
  let component: ForumContentViewComponent;
  let fixture: ComponentFixture<ForumContentViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForumContentViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumContentViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

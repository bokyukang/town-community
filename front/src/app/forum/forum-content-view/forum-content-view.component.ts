import { Component, OnInit } from '@angular/core';
import { ForumContentEditComponent } from '@app/forum/forum-content-edit/forum-content-edit.component';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { ForumArticle } from '@app/_models/forum-article';
import { ForumService } from '@app/_services/forum.service';
import { UserProfileComponent } from '@app/profile/user-profile.component';
import { AccountDataService } from '@app/_services/account-data.service';
import { ConfirmationService } from 'primeng/api';
import { MyMessageService } from '@app/_services/my-message.service';
import { Comment } from '@app/_models/comment';

@Component({
    selector: 'app-forum-content-view',
    templateUrl: './forum-content-view.component.html',
    styleUrls: ['./forum-content-view.component.css'],
    providers: [ConfirmationService]
})
export class ForumContentViewComponent implements OnInit {

    article:ForumArticle = null;
    d_loggedInUserId :Number = -1;
    b_upvoted = false;
    b_downvoted = false;
    d_id = -1;
    s_comment = '';
    a_comments:Comment[] = null;
    s_commentSort = 'auto';

    commentSortOptions = [
        {name: '자동', value:'auto'},
        {name: '인기', value:'popular'},
        {name: '날짜', value:'date'}
    ];

    constructor(
        private forumService:ForumService,
        private dialogManagerService:DialogManagerService,
        public ref: DynamicDialogRef,
        public config: DynamicDialogConfig,
        private accountDataService: AccountDataService,
        private confirmationService:ConfirmationService,
        private messageService:MyMessageService,
    ) {
    }

    ngOnInit(): void {
        this.d_id = this.config.data['id'];
        this.accountDataService.user.subscribe(
            (o_user) => {
                if (o_user!= null)
                    this.d_loggedInUserId = o_user['id'];
            }
        );
        this.forumService.getArticle(this.d_id)
            .subscribe( (o_article:ForumArticle) => {
                this.article = o_article;
                console.log(JSON.stringify(o_article));
                this.loadComments();
                if (o_article['vote'] != null && o_article['vote'] != 0 ) {
                    this.setRecommend(o_article['vote'], 0);
                }
            });
    }

    onEdit(): void {
        this.dialogManagerService.replace( ForumContentEditComponent, {
            data: {
                id: this.d_id,
                forum_list_id: this.config.data['forum_list_id']
            },
            header: this.config.header,
            width: '100%',
        });
    }

    userClick(event): void {
        this.dialogManagerService.open(UserProfileComponent, {
            header: this.config.header + ' > 사용자 정보',
            width: '70%',
            data: {
                user_id: this.article.user_id
            }
        });
    }

    onDelete(event):void {
        this.confirmationService.confirm({
            message: '삭제하시겠습니까?',
            accept: ()=> {
                this.doDelete();
            },
            target: event.target,
            icon: 'pi pi-exclamation-triangle'
        }
        );
    }

    doDelete():void {
        this.forumService.delete(this.d_id)
            .subscribe( (b_result) => {
                if ( b_result ) {
                    this.messageService.showSuccess('삭제', '삭제되었습니다.');
                    this.dialogManagerService.close();
                }
                else {
                    this.messageService.showErr('실패', '실패하였습니다.');
                }
            });
    }

    setRecommend(d_dir, d_reputChange) {
        this.b_upvoted = this.b_downvoted = false;

        if ( d_dir != null ) {
            if ( d_dir == 1 ) {
                this.b_upvoted = true;
            }
            else if (d_dir == -1)  {
                this.b_downvoted = true;
            }
        }
        this.article.reputation += d_reputChange;
            
    }

    onUpVote() {
        let d_dir = 1;
        if ( this.b_upvoted ) {
            d_dir = 0;
        }
        this.forumService.vote(d_dir, this.d_id)
            .subscribe(
                d_reputChange => {
                    if ( d_dir == 0) {
                        this.messageService.showSuccess('추천', '추천 취소 되었습니다.');
                    }
                    else {
                        this.messageService.showSuccess('추천', '추천되었습니다.');
                    }
                    this.setRecommend(d_dir, d_reputChange);
                }
            );
    }

    onDownVote() {
        let d_dir = -1;
        if ( this.b_downvoted ) {
            d_dir = 0;
        }
        this.forumService.vote(d_dir, this.d_id)
            .subscribe(
                d_reputChange => {
                    if ( d_dir == 0 ) {
                        this.messageService.showSuccess('비추천', '비추천 취소 되었습니다.');
                    }
                    else {
                        this.messageService.showSuccess('비추천', '비추천되었습니다.');
                    }
                    this.setRecommend(d_dir, d_reputChange);
                }
            );
    }

    addComment(event) {
        this.forumService.addComment(
            {
                type:'article',
                ref_id:this.d_id,
                content:this.s_comment
            }
        ).subscribe(d_id => {
            console.log('comment success', d_id);
            this.loadComments();
            this.s_comment = '';
        });
    }

    onCommentUpVote(o_comment) {
        this.voteComment(o_comment, 1, '추천');
    }

    voteComment(o_comment, d_dir, s_msg) {
        console.log(JSON.stringify(o_comment));
        if ( o_comment.vote == d_dir ) {
            d_dir = 0;
        }
        this.forumService.vote(d_dir, o_comment.id, 'comment')
            .subscribe(
                d_reputChange => {
                    if ( d_dir != 0 ) {
                        this.messageService.showSuccess(s_msg, s_msg + '되었습니다.');
                    }
                    else if (d_dir == 0 ) {
                        this.messageService.showSuccess(s_msg + ' 취소', s_msg + ' 취소 되었습니다.');
                    }
                    this.setCommentRecommend(o_comment, d_dir, d_reputChange);
                });
    }

    onCommentDownVote(o_comment) {
        this.voteComment(o_comment, -1, '비추천');
    }

    setCommentRecommend(o_comment, d_dir, d_reputChange) {
        o_comment.vote = d_dir;
        o_comment.reputation += d_reputChange;
    }

    onCommentSortChange(event) {
        console.log(event);
        this.loadComments();
    }

    loadComments() {
            this.forumService.getComments(this.d_id, this.s_commentSort)
                .subscribe(
                    a_comments => {
                        console.log('comments', a_comments);
                        this.a_comments = a_comments;
                    }
                );
    }
}

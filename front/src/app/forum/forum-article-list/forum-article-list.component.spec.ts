import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumArticleListComponent } from './forum-article-list.component';

describe('ForumArticleListComponent', () => {
  let component: ForumArticleListComponent;
  let fixture: ComponentFixture<ForumArticleListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForumArticleListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumArticleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

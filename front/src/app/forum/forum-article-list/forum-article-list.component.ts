import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { ForumService } from '@app/_services/forum.service';
import { ForumListItem } from '@app/_models/forum-list-item';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { ForumContentViewComponent } from '@app/forum/forum-content-view/forum-content-view.component';
import { ForumContentEditComponent } from '@app/forum/forum-content-edit/forum-content-edit.component';
import { UtilService } from '@app/_services/util.service';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';

@Component({
    selector: 'app-forum-article-list',
    templateUrl: './forum-article-list.component.html',
    styleUrls: ['./forum-article-list.component.css'],
    providers: []
})
export class ForumArticleListComponent implements OnInit{

    a_listItems: ForumListItem[];
    d_totalRecords = 0;
    d_rows = 4;
    d_page = 0;
    d_id = -1;
    s_title = '';

    s_sort = 'auto';
    sortOptions = [
        {name: '자동', value:'auto'},
        {name: '인기', value:'popular'},
        {name: '날짜', value:'date'}
    ];

    constructor(private forumService:ForumService,
        private utilService:UtilService,
              private dialogService:DialogManagerService,
              private sanitizer:DomSanitizer,
              public config: DynamicDialogConfig
    ) { 
        this.s_title = this.config.header;
    }

    load(): void {
        console.log('id = ' + JSON.stringify(this.config.data['id']));
        this.d_id = this.config.data['id'] ? this.config.data['id'] : 0;
        this.forumService.getForumArticles(this.d_id, this.d_page * this.d_rows, this.d_rows, this.s_sort).subscribe(
            {
                next: (data) => {
                    console.log('forum data ' + JSON.stringify(data));
                    this.a_listItems = data['articles'];
                    this.d_totalRecords = data['totalRecords'];

                    const temp = document.createElement('div');
                    const now = new Date().getTime();

                    const d_timeDiffDays = 50;

                    this.a_listItems.map( o_item => {
                        temp.innerHTML = o_item.summary;
                        o_item.summary = temp.innerText || temp.textContent;
                        const d_timeDiff = now - o_item.created_at * 1000 ;
                        if ( d_timeDiff < d_timeDiffDays * 24 * 60 * 60 * 1000 ) {
                            o_item['time_diff'] = this.getTimeDiffStr(o_item.created_at * 1000);
                        }
                        return o_item;
                    });
                },
                error: error => {
                    console.log('forum err ' + error);
                }
            }
        );
    }

    getTimeDiffStr(d_timeDiff) {
        return this.utilService.getTimeDiffStr(d_timeDiff);
    }

    paginate(event) : void {
        this.d_rows = event.rows;
        this.d_page  = event.page;
        this.load();
        /*
         event.first
         event.rows
         event.page
         event.pageCount
         */
        console.log('paginate '  + event.page);
    }

    rowClick(event, o_listItem):void {
        console.log('rowclick ' + o_listItem['id']);
        let o_subject = this.dialogService.open(ForumContentViewComponent , {
            data: {
                id:o_listItem['id'],
                forum_list_id: this.d_id
            },
            header: this.s_title,
            width: '70%',
        });

        o_subject.subscribe((s_event) => {
            console.log('event ' + s_event);
            if ( s_event == 'close' ) {
                this.load();
            }
        });
    }

    ngOnInit(): void {
        this.load();
        console.log('oninit');
    }

    writeNew(event):void {
        console.log('writenew');
        let o_subject = this.dialogService.open(ForumContentEditComponent , {
            data: {
                id:-1,
                forum_list_id:this.d_id
            },
            header: this.s_title,
            width: '100%',
        });

        o_subject.subscribe((s_event) => {
            console.log('event ' + s_event);
            if ( s_event == 'close' ) {
                this.load();
            }
        });
    }

    rowSelectable(event) {
        return false;
    }

    cleanURL(s_url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(s_url);
    }

    onSortChange(event) {
        console.log('sort ' + JSON.stringify(event) + this.s_sort);
        this.load();
    }
}

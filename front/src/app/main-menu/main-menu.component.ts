import { Component, OnInit } from '@angular/core';
import { EventService } from '@app/_services/event.service';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { TestComponent } from '@app/test/test.component';
import { SignupComponent } from '../signup/signup.component';
import { SigninComponent } from '../signin/signin.component';
import { AccountDataService } from '../_services/account-data.service';
import { AccountService } from '../_services/account.service';
import { AlertService } from '../_services/alert.service';
import { MessageService } from 'primeng/api';
import { User } from '@app/_models';
import { MenuItem } from 'primeng/api';
import { MyProfileComponent } from '@app/profile/myprofile.component';
import { FriendsService } from '@app/_services/friends.service';
import { ModeEnum, MapService } from '@app/_services/map.service';
import { MsgListComponent } from '@app/msg/msg-list/msg-list.component';
import { NoteMessageService } from '@app/_services/note-message.service';
import { UtilService } from '@app/_services/util.service';
import { EventEnum } from '@app/_services/event.service';
import { DateUtil } from '@app/common/date-util';
import { isDevMode } from '@angular/core';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {

    user:User = null;
    accountMenuItems:MenuItem[];
    accountMenuItemsLoggedIn:MenuItem[];
    s_mode:ModeEnum=ModeEnum.village;
    a_modeOptions = [
        {name: '동네', value:ModeEnum.village},
        {name: '모임', value:ModeEnum.event}
    ];
    d_newMsgCount = 0;
    d_score = 0;
    a_typeOptions=[];
    a_selTypes=[];
    b_isDev = false;
    _s_dateFrom = '';
    get s_dateFrom() { return this._s_dateFrom; }
    set s_dateFrom(val) { 
        this._s_dateFrom = val; 
        this.mapService.sub_searchDateFrom.next(this._s_dateFrom);
    } 
    _s_dateTo = '';
    get s_dateTo() { return this._s_dateTo; }
    set s_dateTo(val) { 
        this._s_dateTo = val; 
        this.mapService.sub_searchDateTo.next(this._s_dateTo);
    } 


  constructor(
              public dialogService:DialogManagerService,
              private accountService:AccountService,
              private accountDataService:AccountDataService,
              private alertService:AlertService,
              private messageService: MessageService,
              private friendsService:FriendsService,
              private mapService:MapService,
              private noteMessageService:NoteMessageService,
              private utilService:UtilService,
              private eventService:EventService,
  ) 
  {
  }

  ngOnInit(): void {
      //this.testClicked();

      const that = this;
      this.b_isDev = isDevMode();
      this.s_dateFrom = DateUtil.getToday();
      this.mapService.sub_searchDateFrom.next(this.s_dateFrom);

      this.a_typeOptions = [
          {
              value:EventEnum.meetup,
              name:"이벤트"
          },
          {
              value:EventEnum.group,
              name:"그룹"
          }
      ];
      Object.keys(EventEnum).forEach((key, idx) => {
          const s_val = Object.values(EventEnum)[idx];
          this.a_selTypes.push(s_val);
          this.eventService.selEvents.next(this.a_selTypes);
      });

      this.friendsService.a_requests.subscribe(
          a_requests => {
              this.a_friendRequests = a_requests;
          }
      );
      this.accountDataService.user.subscribe(
          x => { 
              that.user = x;
              console.log('user ' + JSON.stringify(that.user));
              if ( that.user ) {
              }

          }
      );
      this.noteMessageService.a_newMsgs.subscribe(
          a_msgs => {
              if ( a_msgs != null ) {
                  //console.log('new msg len ' + JSON.stringify(a_msgs));
                  this.d_newMsgCount = a_msgs.totalRecords;
              }
          }
      );
      // init account menu
      this.accountMenuItems = [{
          label: '계정',
          items: [{
              label: '로그인',
              icon: 'fa fa-user fa-sm',
              command: () => {
                  this.signIn();
              }
          },
          {
              label: '회원가입',
              command: () => {
                  this.signUp();
              }
          }]
      }];
      this.accountMenuItemsLoggedIn = [{
          label: '계정',
          items: [{
              label: '로그아웃',
              icon: '',
              command: () => {
                  this.signOut();
              }
          },
          {
              label: '프로필',
              icon: 'fa fa-user fa-sm',
              command: () => {
                  this.showProfile();
              }
          }
          ]
      }];
      this.accountDataService.userSubject.subscribe(
          user => {
              if ( user ) {
                  this.utilService.getScore('user-vote', user.id)
                      .subscribe( d_score => {
                          this.d_score = d_score;
                      });
              }
          });
      this.mapService.modeSubject.subscribe(
          s_val => {
              this.s_mode = s_val;
          }
      );
  }
    a_friendRequests = [];
    testClicked() {
        this.dialogService.open(TestComponent , {
            header: 'test',
            width: '70%',
            data: {
            }
        });
    }

    signUp() {
        this.dialogService.open(SignupComponent, {
            header: '회원 가입',
            width: '70%',
        });
    }
    signIn() {
        this.dialogService.open(SigninComponent, {
            header: '로그인',
            width: '70%',
        });
    }
    signOut() {
        this.accountService.signOut().subscribe(data => {
             this.messageService.add({severity: 'success', summary: '성공', detail:'로그아웃 되었습니다.'}); 
        });
    }
    showProfile() {
        this.dialogService.open(MyProfileComponent, {
            header: '프로파일',
            width: '70%',
        });
    }
    modeChange(event) {
        this.mapService.modeValue = this.s_mode;
    }
    showNoteMsgs(event) {
        this.dialogService.open(MsgListComponent, {
            header: '받은 쪽지함',
            width: '70%',
        }).subscribe(
             (s_cmd) => {
                 if ( s_cmd == 'close' ) {
                    console.log('close');
                    this.noteMessageService.getNewMsgs();
                 }
            
        });
    }

    typeChange(event) {
        console.log('sel types',this.a_selTypes);
          this.eventService.selEvents.next(this.a_selTypes);
          this.eventService.updateEventsOnMap(this.a_selTypes, this.s_dateFrom, this.s_dateTo);
    }

    dateToChange(e) {
        console.log('dateto', this.s_dateTo);
        this.mapService.sub_searchDateTo.next(this.s_dateTo);
      this.eventService.updateEventsOnMap(this.eventService.selEvents.value, this.mapService.sub_searchDateFrom.value, this.mapService.sub_searchDateTo.value);
    }

    dateFromChange(e) {
        console.log('datefrom', this.s_dateFrom);
        this.mapService.sub_searchDateFrom.next(this.s_dateFrom);
      this.eventService.updateEventsOnMap(this.eventService.selEvents.value, this.mapService.sub_searchDateFrom.value, this.mapService.sub_searchDateTo.value);
    }

}

import { Component, OnInit } from '@angular/core';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { FormGroup, FormControl } from '@angular/forms';
import { AccountService } from '../_services/account.service';
import { MessageService } from 'primeng/api';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { SignupComponent } from '@app/signup/signup.component';
import { PwdresetComponent } from '@app/pwdreset/pwdreset.component';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
  providers: [MessageService]
})
export class SigninComponent implements OnInit {

    emailForm:FormControl = new FormControl('');
    pwdForm:FormControl = new FormControl('');
    signinForm:FormGroup = new FormGroup({
        email: this.emailForm,
        pwd: this.pwdForm
    });

  constructor(private accountService:AccountService,
              private messageService: MessageService,
             public ref: DynamicDialogRef,
             private dialogService:DialogManagerService,
  ) { }

  ngOnInit(): void {
  }

  onSubmit():void {
        if ( this.signinForm.status == 'VALID' ) {
            let s_email = this.signinForm.controls.email.value;
            let s_pwd = this.signinForm.controls.pwd.value;
            this.accountService.signIn(s_email, s_pwd).subscribe(
                {
                    next: (data) => {
                        console.log('signin data ' + data);
                         this.messageService.add({severity: 'success', summary: '성공', detail:'로그인 되었습니다.'}); 
                         setTimeout(()=> {
                             this.close();
                         }, 2000);
                    },
                    error: error => {
                        if ( error['error'] == 'NOT_EXIST' ) {
                             this.messageService.add({severity: 'error', summary: '실패', detail:'계정이 존재하지 않습니다.'}); 
                        }
                        else if ( error['error'] == 'PWD_WRONG' ) {
                             this.messageService.add({severity: 'error', summary: '실패', detail:'비밀번호가 틀렸습니다.'}); 
                        }
                        else if ( error['error'] == 'NOT_VALIDATED' ) {
                             this.messageService.add({severity: 'error', summary: '실패', detail:'이메일이 인증되지 않았습니다. 이메일 인증을 진행해 주세요.'}); 
                        }
                    }

                });
        }
  }

  onSignup():void {
            this.dialogService.replace(SignupComponent, {
                header: '',
                width: '70%',
                data:{
                },
            });
  }

  close():void {
      this.ref.close();
  }

  onPwdReset():void {
            this.dialogService.replace(PwdresetComponent, {
                header: '비밀번호 재설정',
                width: '70%',
                data:{
                },
            });
  }

}

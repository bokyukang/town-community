import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { AccountService } from '@app/_services/account.service';
import { AccountDataService } from '@app/_services/account-data.service';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { UserProfile } from '@app/_models';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { ImageComponent } from '@app/common/image/image.component';
import { MenuItem } from 'primeng/api';
import { FriendsService, FriendStatus } from '@app/_services/friends.service';
import { SendMsgComponent } from '@app/msg/send-msg/send-msg.component';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  providers: [MessageService]
})
export class UserProfileComponent implements OnInit {

    d_userId = -1;
    userProfile:UserProfile = null;
    imgs:string[] = null;
    s_friendStatus:BehaviorSubject<FriendStatus> = null;
    s_voteStatus:BehaviorSubject<number> = null;

    _activeIndex: number = 1;


    constructor(
        private accountService:AccountService,
        private accountDataService:AccountDataService,
        public config:DynamicDialogConfig,
        private dialogManagerService:DialogManagerService,
        private friendsService:FriendsService,
        private messageService:MessageService,
    ) { 
        this.s_friendStatus = new BehaviorSubject<FriendStatus>(null);
        this.s_voteStatus = new BehaviorSubject<number>(0);
    }

    get activeIndex(): number {
        return this._activeIndex;
    }

    set activeIndex(newValue) {
        if (this.imgs && 0 <= newValue && newValue <= (this.imgs.length - 1)) {
            this._activeIndex = newValue;
        }
    }

	responsiveOptions:any[] = [
        {
            breakpoint: '1024px',
            numVisible: 5
        },
        {
            breakpoint: '768px',
            numVisible: 3
        },
        {
            breakpoint: '560px',
            numVisible: 1
        }
    ];

    menuItems:MenuItem[];

    loadMenu():void {

        let friendMenu = null;
        let voteMenu = null;
        let bMy = this.accountDataService?.userValue?.id == this.d_userId;
        friendMenu = {
            icon: 'assets/imgs/random.jpg',
            label: '친구 맺기',
            command:() => {
                this.requestFriend();
            },
            disabled: bMy
        };
        let o_status = this.s_friendStatus.value;
        if ( o_status == FriendStatus.PENDING ) {
            friendMenu = {
                icon: 'assets/imgs/random.jpg',
                label: '친구 요청중',
                items: [{
                    label: '요청 취소',
                    command:() => {
                        this.cancelFriendRequest();
                    }
                }],
                disabled: bMy
            }
        }
        else if ( o_status == FriendStatus.ACCEPTED ) {
            friendMenu = {
                icon: 'assets/imgs/random.jpg',
                label: '친구 관계임',
                items: [
                    {
                        icon: 'assets/imgs/random.jpg',
                        label: '친구 해지',
                        command:() => {
                            this.unfriend();
                        }
                    }
                ],
                disabled: bMy

            }
        }

        let d_voteStatus = this.s_voteStatus.value;

        voteMenu = 
            {
                label: '지지하기',
                command:() => {
                    this.vote(true);
                },
                disabled: bMy
            };
        if ( d_voteStatus == 1 ) {
            voteMenu = {
                label: '지지 철회하기',
                command:() => {
                    this.vote(false);
                },
                disabled: bMy
            };
        }
        this.menuItems = 
            [
                friendMenu,
                {
                    icon: 'assets/imgs/random.jpg',
                    label: '쪽지 보내기',
                    command:() => {
                        this.sendNoteMsg();
                    },
                    disabled: bMy
                },
                voteMenu,
            ];

    }

    vote(b_up) {
        this.accountService.vote(this.d_userId, b_up).subscribe(
            {
                next: () => {
                    if ( b_up ) {
                        this.s_voteStatus.next(1);
                        let a_myvoters = this.friendsService.a_myvotersSubject.value;
                        a_myvoters.push(this.d_userId);
                        this.friendsService.a_myvotersSubject.next(a_myvoters);
                    }
                    else {
                        this.s_voteStatus.next(0);
                        let a_myvoters = this.friendsService.a_myvotersSubject.value;
                        let a_new = a_myvoters.filter( d_userId => d_userId != this.d_userId );
                        this.friendsService.a_myvotersSubject.next(a_new);
                    }
                },
                error: (err) => {
                    console.log('err',err);
                    if ( err.error == 'max-count-exceed' ) {
                        this.messageService.add({severity: 'error', summary: '실패', detail:'총 5명까지 지지할 수 있습니다.'});
                    }
                }
            }
        );
    }

    ngOnInit(): void {
        let that = this;
        let d_user_id = this.config.data['user_id'];
        this.d_userId = d_user_id;
        this.accountService.getUserProfile(d_user_id).subscribe(
            result => {
                that.userProfile = result;
                let imgs = [];
                for( let i=0; i < result['imgs'].length; ++i ) {
                    imgs.push(result['imgs'][i]['name']);
                }
                that.imgs = result['imgs'];
                that.s_voteStatus.next(result['vote']);
                console.log(JSON.stringify(result));
            });

        this.s_friendStatus.subscribe(
            () => {
                this.loadMenu();
            }
        );
        this.s_voteStatus.subscribe(
            () => {
                this.loadMenu();
            }
        );
        this.loadFriendStatus();
    }
    loadFriendStatus() {
        this.friendsService.getFriendStatus(this.d_userId).subscribe(
            e_status => {
                this.s_friendStatus.next(e_status);
            }
        );
    }

    next() {
        this.activeIndex++;
    }

    prev() {
        this.activeIndex--;
    }

    imageClick(s_img) {
        this.dialogManagerService.open(ImageComponent, {
            header: this.config.header + ' > 이미지',
            width: '70%',
            data: {
                src: s_img
            }
        });
    }

    requestFriend() {
        this.friendsService.requestFriend(this.d_userId).subscribe(
            () => {
                this.messageService.add({severity: 'success', summary: '성공', detail:'친구 요청 되었습니다.'});
                this.loadFriendStatus();
            }
        );
    }
    unfriend() {
        this.friendsService.unfriend(this.d_userId).subscribe(
            () => {
                this.messageService.add({severity: 'success', summary: '성공', detail:'친구목록에서 삭제되었습니다.'});

                this.loadFriendStatus();
            }
        );
    }

    cancelFriendRequest() {
        this.friendsService.cancelRequest(this.d_userId).subscribe(
            () => {
                this.messageService.add({severity: 'success', summary: '성공', detail:'친구 요청을 취소하였습니다.'});
                this.loadFriendStatus();
            }
        );
    }

    sendNoteMsg() {
        this.dialogManagerService.open(SendMsgComponent, {
            header: this.config.header + ' > 쪽지 보내기',
            width: '70%',
            data: {
                receiver_id: this.d_userId
            }
        });
    }
}

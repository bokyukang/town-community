import { Component, OnInit , Input} from '@angular/core';
import { ForumService } from '@app/_services/forum.service';
import { ForumListItem } from '@app/_models/forum-list-item';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { ForumContentViewComponent } from '@app/forum/forum-content-view/forum-content-view.component';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-user-articles',
  templateUrl: './user-articles.component.html',
  styleUrls: ['./user-articles.component.scss']
})
export class UserArticlesComponent implements OnInit {

    a_listItems: ForumListItem[];
    d_totalRecords = 0;
    d_rows = 4;
    d_page = 0;
    s_title = '';

    @Input() d_userId = null;

  constructor(
      private forumService:ForumService,
      private dialogService:DialogManagerService,
      public config: DynamicDialogConfig
  ) { 
        this.s_title = this.config.header + ' > 게시글들';
  }

  ngOnInit(): void {
      this.load();
  }

  load(): void {
        if (this.config.data && this.config.data['user_id']) 
        {
            this.d_userId = this.config.data['user_id'];
        }

        this.forumService.getUserArticles(this.d_userId, this.d_page * this.d_rows, this.d_rows).subscribe(
            {
                next: (data) => {
                    console.log('user forum data ' + JSON.stringify(data));
                    this.a_listItems = data['articles'];
                    this.d_totalRecords = data['totalRecords'];
                },
                error: error => {
                    console.log('user forum articles err ' + error);
                }
            });
  }

    paginate(event) : void {
        this.d_rows = event.rows;
        this.d_page  = event.page;
        this.load();
    }

    rowClick(event, o_listItem):void {
        console.log('rowclick ' + o_listItem['id']);
        let o_subject = this.dialogService.open(ForumContentViewComponent , {
            data: {
                id:o_listItem['id']
            },
            header: this.s_title,
            width: '70%',
        });

        o_subject.subscribe((s_event) => {
            console.log('event ' + s_event);
            if ( s_event == 'close' ) {
                this.load();
            }
        });
    }
    rowSelectable(event) {
        return false;
    }

}

import { Component, OnInit } from '@angular/core';
import { AccountService } from '@app/_services/account.service';
import { MessageService } from 'primeng/api';
import { UserProfile } from '@app/_models';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import {EditFieldComponent} from '@app/profile/edit-field/edit-field.component';

@Component({
  selector: 'app-my-profile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css'],
  providers: [MessageService]
})
export class MyProfileComponent implements OnInit {

    myProfile:UserProfile = null;
    imgs:string[] = null;

  constructor(private accountService:AccountService,
      private messageService:MessageService,
      private dialogService:DialogManagerService,
      private config:DynamicDialogConfig
  ) { }

  ngOnInit(): void {
      this.loadMyProfile();
  }

  onFileSelected(event):void {
      const o_file:File = event.target.files[0];
      let that = this;
      if ( o_file ) {
          this.accountService.uploadImg(o_file).subscribe(
              {
                  next: (data) => {
                      console.log(data);
                      that.loadMyProfile();
                  },
                  error: error => {
                      console.log(error);
                  }
              });
      }
  }

  onLoadProfileImgs():void {
      this.accountService.getProfileImgs().subscribe(
          {
              next: (data) => {
                  this.imgs = data;
              },
              error: error=> {
                  console.log(error);
              }
          });
  }

  loadMyProfile():void {
      let that = this;
      this.accountService.getMyProfile().subscribe(
          {
              next: (data) => {
                  that.myProfile = data;
              },
              error: (error) => {
                  console.log(error);
              }
          });
  }

  onDelImg(d_id):void {
      let that = this;
      this.accountService.delImg(d_id).subscribe(
          data =>  {
                this.messageService.add({severity: 'success', summary: '성공', detail:'이미지가 삭제되었습니다.'});
                that.loadMyProfile();
          });
  }

  onSetMainImg(d_id):void {
      this.accountService.setMainImg(d_id).subscribe(
          data =>  {
                this.messageService.add({severity: 'success', summary: '성공', detail:'이미지가 메인으로 등록되었습니다.'});
                this.loadMyProfile();
          });
  }
  
  modifyIntro(event):void {
      let that = this;
      this.dialogService.open(EditFieldComponent, {
          header: this.config.header + ' > 소개글 수정',
          width: '70%',
          data: {
              name: 'intro',
              value: this.myProfile['intro']
          }
      }).subscribe((s_event) => {
          if (s_event == 'close' ) {
              that.loadMyProfile();
          }
      });
  }

  modifyName(event):void {
      this.dialogService.open(EditFieldComponent, {
          header: this.config.header + ' > 이름 수정',
          width: '70%',
          data: {
              name:'name',
              value: this.myProfile['name']
          }
      }).subscribe((s_event) => {
          if (s_event == 'close' ) {
              this.loadMyProfile();
          }
      });
  }

}

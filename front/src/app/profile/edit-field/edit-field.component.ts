import { Component, OnInit } from '@angular/core';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { AccountService } from '@app/_services/account.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-edit-field',
  templateUrl: './edit-field.component.html',
  styleUrls: ['./edit-field.component.css'],
  providers: [MessageService]
})
export class EditFieldComponent implements OnInit {

    name:string = '';
    value:string = '';

  constructor(
      public config: DynamicDialogConfig,
      private accountService: AccountService,
      private messageService:MessageService
  ) { }

  ngOnInit(): void {
      this.name = this.config.data['name'];
      this.value = this.config.data['value'];
  }

    updateField(event) :void {
        this.accountService.updateField(this.name, this.value).subscribe(
            {
                next: () => {
                    this.messageService.add({severity: 'success', summary: '성공', detail:'업데이트 되었습니다.'});
                },
                error: (error) => {
                    console.log(error);
                }
            }
        );
    }
}


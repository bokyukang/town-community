import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditIntroComponent } from './edit-intro.component';

describe('EditIntroComponent', () => {
  let component: EditIntroComponent;
  let fixture: ComponentFixture<EditIntroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditIntroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditIntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, AfterContentInit, HostBinding } from '@angular/core';
import {
    trigger,
    state,
    style,
    animate,
    transition,
} from '@angular/animations';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.css'],
  animations: [
      trigger('largeSmall', [
          state('large', style({
              fontSize:'100px',
              opacity:1,
              color:'#252',
              backgroundColor:'#9cc9',
              borderStyle:'solid',
              borderColor:'#000',
              borderWidth:'2px',
              fontWeight:'bold',
              borderRadius:'3px',
          })),
          state('small', style({
              fontSize:'15px',
              opacity:1.0,
              color:'#252',
              borderStyle:'solid',
              borderWidth:'1px',
              marginTop:'0px',
              fontWeight:'bold',
          })),
          transition('large => small', [
              animate('2s 2s')
          ]),
          transition('small => large', [
              animate('2s')
          ]),
      ]),
  ]
})
export class LogoComponent implements OnInit, AfterContentInit {

    isSmall = false;
  constructor() { }

  ngOnInit(): void {
      setTimeout(() => this.isSmall = true)
  }
  ngAfterContentInit(): void {
  }
  test() :void {
      this.isSmall = !this.isSmall;
  }

}

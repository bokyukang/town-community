export interface FriendRequest {
    user_id:number,
    user_name:string,
    town_name:string,
    user_pic:string
}

export interface Friend {
    user_id:number,
    user_name:string,
    town_name:string,
    user_pic:string
}

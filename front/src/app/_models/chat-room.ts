export interface ChatRoom {
    id: number;
    title: string;
    type: string;
    type_id: number;
    description: string;
    img_url:string;
    user_id:number;
    tag:string;
}


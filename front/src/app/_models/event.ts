export class Event {
    id:number;
    title:string;
    lat:number;
    lng:number;
    address:string;
    description:string;
    at:string;
    user_id:number;
    type:string;
    created_at:string;
    is_online?:boolean = false;
}

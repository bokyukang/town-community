export class Comment {
    id;
    type;
    user_id;
    user_name;
    user_pic_url;
    ref_id;
    content;
    reputation;
    created_at;
    vote; // 1 : up , -1 : down, null: none
}

import { Image }  from '@app/_models/image';

export interface ForumArticle {
    id; 
    forum_list_id;
    user_id;
    title; 
    content;
    created_at;
    user_name;
    user_pic_url;
    img_urls:Image[];
    reputation;
}

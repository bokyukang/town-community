export interface ForumListItem {
    id; 
    title; 
    created_at;
    user_id;
    user_name;
    user_pic_url;
    forum_title;
    town_name;
    summary;
    img_url;
    img_type;
}

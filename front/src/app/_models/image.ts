export interface Image {
    id:number;
    img_url:string;
    type:string;
    is_main:boolean;
}

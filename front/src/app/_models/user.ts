﻿export class User {
    id:Number;
    email:string;
    name:string;
    profile_path:string; 
    town_sig_id:Number;
    town_id:number;
    town_sig_kor_nm:string;
}

export class UserProfile {
    name:string;
    email:string;
    town_sig_id:string;
    town_sig_kor_nm:string;
    town_id:number;
    intro:string;
    imgs:string[];
}

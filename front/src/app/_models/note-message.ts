export interface NoteMessage {
    id:number,
    user_name:string,
    content:string,
    user_pic:string,
    user_id:number,
    created_at:string,
    opened:number,
}

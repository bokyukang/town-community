import { LOCALE_ID, APP_INITIALIZER } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeKr from '@angular/common/locales/ko';
registerLocaleData(localeKr);
import { NoSanitizePipe } from '@app/helpers/nosanitize-pipe';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http';
import { VillagePaneComponent } from './village-pane/village-pane.component';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TooltipModule } from 'primeng/tooltip';
import { AlertComponent } from './alert/alert.component';
import { EmailValidatedComponent } from './email-validated/email-validated.component';
import { ChipModule } from 'primeng/chip';
import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { MenuModule } from 'primeng/menu';
import { MessageService } from 'primeng/api';
import { EditorModule } from 'primeng/editor';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TestComponent } from './test/test.component';
import { CheckboxModule } from 'primeng/checkbox';
import { MyProfileComponent } from './profile/myprofile.component';
import { MatIconModule } from '@angular/material/icon';
import { JwtInterceptor, ErrorInterceptor } from './helpers';
import { ContextMenuModule } from 'primeng/contextmenu';
import { ForumArticleListComponent } from './forum/forum-article-list/forum-article-list.component';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { ForumContentViewComponent } from './forum/forum-content-view/forum-content-view.component';
import { DialogService } from 'primeng/dynamicdialog';
import { FriendsService } from '@app/_services/friends.service';
import { ForumContentEditComponent } from './forum/forum-content-edit/forum-content-edit.component';
import { RouterModule } from '@angular/router';
import { ForumTownForumsComponent } from './forum/forum-town-forums/forum-town-forums.component';
import { InputTextModule } from 'primeng/inputtext';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { AvatarModule } from 'primeng/avatar';
import { AvatarGroupModule } from 'primeng/avatargroup';
import { UserProfileComponent } from './profile/user-profile.component';
import { GalleriaModule } from 'primeng/galleria';
import { CardModule } from 'primeng/card';
import { FieldsetModule } from 'primeng/fieldset';
import { DialogModule } from 'primeng/dialog';
import { EditFieldComponent } from './profile/edit-field/edit-field.component';
import { UserArticlesComponent } from './profile/user-articles/user-articles.component';
import { ChatRoomComponent } from './chat/chat-room/chat-room.component';
import { WebsocketService } from '@app/_services/websocket.service';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { ImageComponent } from './common/image/image.component';
import { DividerModule } from 'primeng/divider';
import { CalendarModule } from 'primeng/calendar';
import { TabViewModule } from 'primeng/tabview';
import { SidebarModule } from 'primeng/sidebar';
import { DockModule } from 'primeng/dock';
import { TagModule } from 'primeng/tag';
import { DataViewModule } from 'primeng/dataview';
import { CreateChatComponent } from './chat/create-chat/create-chat.component';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { PanelModule } from 'primeng/panel';
import { CreateForumComponent } from './forum/create-forum/create-forum.component';
import { MenubarModule } from 'primeng/menubar';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { FriendsComponent } from './friends/friends.component';
import { MapThreeLayerComponent } from './map/map-three-layer/map-three-layer.component';
import { SelectButtonModule } from 'primeng/selectbutton';
import { CreateEventComponent } from './events/create-event/create-event.component';
import { SendMsgComponent } from './msg/send-msg/send-msg.component';
import { MsgListComponent } from './msg/msg-list/msg-list.component';
import { NoteMessageService } from '@app/_services/note-message.service';
import { MsgViewComponent } from './msg/msg-view/msg-view.component';
import { SplitterModule } from 'primeng/splitter';
import { UserChipComponent } from './components/user-chip/user-chip.component';
import { ConfirmComponent } from './common/confirm/confirm.component';
import { HelpSnippetComponent } from './common/help-snippet/help-snippet.component';
import { ImageThumbComponent } from './common/image-thumb/image-thumb.component';
import { ReputationComponent } from './common/reputation/reputation.component';
import { CommentsComponent } from './common/comments/comments.component';
import { EventViewComponent } from './events/event-view/event-view.component';
import { LocationChipComponent } from './components/location-chip/location-chip.component';
import { DateComponent } from './components/date/date.component';
import { MybuttonComponent } from './components/mybutton/mybutton.component';
import { ParticipantComponent } from './components/participant/participant.component';
import { GroupViewComponent } from './events/group-view/group-view.component';
import { RoomItemComponent } from './components/room-item/room-item.component';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { AddressComponent } from './common/address/address.component';
import { MyeventsComponent } from './events/myevents/myevents.component';
import { PwdresetComponent } from './pwdreset/pwdreset.component';
import { Pwdreset2Component } from './pwdreset2/pwdreset2.component';
import { LogoComponent } from './logo/logo.component';

const config: SocketIoConfig = { url: 'http://localhost:4444', options: {}};

@NgModule({
  declarations: [
	NoSanitizePipe,
    AppComponent,
    MapComponent,
    VillagePaneComponent,
    SigninComponent,
    SignupComponent,
    AlertComponent,
    EmailValidatedComponent,
    TestComponent,
    MyProfileComponent,
    ForumArticleListComponent,
    ForumContentViewComponent,
    ForumContentEditComponent,
    ForumTownForumsComponent,
    UserProfileComponent,
    EditFieldComponent,
    UserArticlesComponent,
    ChatRoomComponent,
    ImageComponent,
    CreateChatComponent,
    CreateForumComponent,
    MainMenuComponent,
    FriendsComponent,
    MapThreeLayerComponent,
    CreateEventComponent,
    SendMsgComponent,
    MsgListComponent,
    MsgViewComponent,
    UserChipComponent,
    ConfirmComponent,
    HelpSnippetComponent,
    ImageThumbComponent,
    ReputationComponent,
    CommentsComponent,
    EventViewComponent,
    LocationChipComponent,
    DateComponent,
    MybuttonComponent,
    ParticipantComponent,
    GroupViewComponent,
    RoomItemComponent,
    AddressComponent,
    MyeventsComponent,
    PwdresetComponent,
    Pwdreset2Component,
    LogoComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    EditorModule,
    DynamicDialogModule,
    ChipModule,
    ToastModule,
    HttpClientModule,
    AutoCompleteModule,
    FormsModule, 
    TooltipModule,
    ReactiveFormsModule,
    ButtonModule,
    MenuModule,
    MatIconModule,
    TableModule,
    InputTextModule,
    RouterModule,
    PaginatorModule,
    SocketIoModule.forRoot(config),
    AvatarModule,
    AvatarGroupModule,
    GalleriaModule ,
    CardModule,
    FieldsetModule,
    DialogModule,
    CheckboxModule,
    ToggleButtonModule,
    ScrollPanelModule,
    DividerModule,
    CalendarModule,
    TabViewModule,
    SidebarModule,
    DockModule,
    TagModule,
    DataViewModule,
    InputTextareaModule ,
    PanelModule,
    MenubarModule,
    OverlayPanelModule,
    SelectButtonModule,
    ContextMenuModule,
    SplitterModule,
    ConfirmDialogModule,
    ConfirmPopupModule,
  ],
  providers: [
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
      /*{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }*/
      { provide: APP_INITIALIZER, useFactory: (ws: WebsocketService) => () => { return ws.init()}, deps: [WebsocketService], multi: true },
      { provide: APP_INITIALIZER, useFactory: (fs: FriendsService) => () => { return fs.loadAll() }, deps: [FriendsService], multi: true },
      { provide: APP_INITIALIZER, useFactory: (ms: NoteMessageService) => () => { return ms.getNewMsgs() }, deps: [NoteMessageService], multi: true },
      { provide: LOCALE_ID, useValue: 'ko-KR' },
      MessageService,
      DialogService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

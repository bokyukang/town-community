import { OnInit, OnDestroy, Component } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { SplashScreenStateService } from '@app/_services/splash-screen-state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'dongchingu';

  constructor(private config:PrimeNGConfig,
             private splashScreenStateService:SplashScreenStateService) {}

  ngOnInit() {
      this.config.setTranslation({
          today: '오늘',
          clear: '삭제',
      });
      setTimeout(() => {
          this.splashScreenStateService.stop();
      }, 5000);
  }

  ngOnDestroy() {
  }

}

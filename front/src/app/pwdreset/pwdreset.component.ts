import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AccountService } from '@app/_services/account.service';
import { MyMessageService } from '@app/_services/my-message.service';

@Component({
  selector: 'app-pwdreset',
  templateUrl: './pwdreset.component.html',
  styleUrls: ['./pwdreset.component.css']
})
export class PwdresetComponent implements OnInit {

    emailForm:FormControl = new FormControl('');
    formGroup:FormGroup = new FormGroup({
        email: this.emailForm,
    });

    constructor(private accountService:AccountService,
                private messageService:MyMessageService) {

    }

    ngOnInit(): void {
    }

    sendResetPwdEmail() {
        if ( this.formGroup.status == 'VALID' ) {
            let s_email = this.formGroup.controls.email.value;
            this.accountService.pwdReset(s_email).subscribe(
                {
                    next:
                        s_res => {
                        if ( s_res == 'OK' ) {
                            this.messageService.showSuccess('비밀번호 재설정 이메일이 발송되었습니다.', '이메일을 확인해 주세요.');
                        }
                        else {
                        }
                    },
                    error:
                        (error) => {
                            this.messageService.showSuccess('실패.', '이메일 발송에 실패하였습니다.');
                    }
                }
            );
        }
    }

}

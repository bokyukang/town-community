import { Mode } from '@app/map/modes/mode';
import { EventService } from '@app/_services/event.service';
import { MapService } from '@app/_services/map.service';
import { MapThreeLayerComponent } from '@app/map/map-three-layer/map-three-layer.component';
import { Event } from '@app/_models/event';
import * as L from 'leaflet';

const NUM_INDEX = 10;

export class EventMode implements Mode {

    mm_events = {};
    eventService:EventService;
    mapService:MapService;
    _a_eventTooltips = [];
    map = null;
    o_mapThreeLayer = null;

    constructor(
        eventService:EventService,
        mapService:MapService,
        map:any,
        mapThreeLayer:MapThreeLayerComponent
    )
    {
        this.eventService = eventService;
        this.mapService = mapService;
        this.map = map;
        this.mapService.e_threeIntersects.subscribe(
            a_intersects => {
                console.log(JSON.stringify(a_intersects));
            });
            this.o_mapThreeLayer = mapThreeLayer;
    }


    onMoveEnd(event) {
        this.eventService.updateEventsOnMap(this.eventService.selEvents.value, this.mapService.sub_searchDateFrom.value, this.mapService.sub_searchDateTo.value);
    }

    onMove(event){
    }

    onEnter(){
        this.eventService.updateEventsOnMap(this.eventService.selEvents.value, this.mapService.sub_searchDateFrom.value, this.mapService.sub_searchDateTo.value);

        this.eventsOnMapSubscription  = this.eventService.eventsOnMapObservable.subscribe(
            (a_events:Event[]) => {

                this.removeTooltips();
                const tooltipOffset = 1 * Math.pow(2,this.mapService.sub_zoomLevel.value - 10);

                for ( let i=0;i < a_events.length; ++i ) {
                    let o_event = a_events[i];
                    const o_tooltip = L.tooltip({
                        permanent:true,
                        direction:'right',
                        offset:L.point(tooltipOffset,0),
                    });
                    this._a_eventTooltips.push(o_tooltip);
                    o_tooltip.setContent(o_event.title);
                    o_tooltip.setLatLng(L.latLng(o_event.lat, o_event.lng));

                    o_tooltip.addTo(this.map);
                }
                //this.positionEventTooltips();
            }
        );
        this.mapService.b_geojsonBorders = false;
    }

    eventsOnMapSubscription = null;

    onExit(){
        this.removeTooltips();
        if ( this.eventsOnMapSubscription ) {
            this.eventsOnMapSubscription.unsubscribe();
        }
    }

    onClick(event){
        console.log('event.latLng', event.latLng);
        this.o_mapThreeLayer.onClick(event);
    }

    onZoomStart(d_zoom) {
        this.removeTooltips();
    }

    onZoomEnd(d_zoom) {
    }

    removeTooltips() {
        for ( let i=0;i < this._a_eventTooltips.length; ++i ) {
            this._a_eventTooltips[i].removeFrom(this.map);
        }
        this._a_eventTooltips = [];
    }
}

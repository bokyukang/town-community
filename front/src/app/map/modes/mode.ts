export interface Mode {
    onMoveEnd(event);
    onMove(event);
    onEnter();
    onExit();
    onClick(event);
    onZoomStart(d_zoom);
    onZoomEnd(d_zoom);
}

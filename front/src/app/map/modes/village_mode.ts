import { Mode } from '@app/map/modes/mode';
import { MapService } from '@app/_services/map.service';

export class VillageMode implements Mode {
    mapService:MapService;
    constructor(mapService:MapService) {
        this.mapService = mapService;
    }
    onMoveEnd(event) {
    }
    onMove(event){
    }
    onEnter(){
        this.mapService.b_geojsonBorders = true;
    }
    onExit(){
        this.mapService.b_geojsonBorders = false;
    }
    onClick(event){
    }
    onZoomStart(d_zoom) {
    }

    onZoomEnd(d_zoom) {
    }
}

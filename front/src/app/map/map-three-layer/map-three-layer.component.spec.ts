import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapThreeLayerComponent } from './map-three-layer.component';

describe('MapThreeLayerComponent', () => {
  let component: MapThreeLayerComponent;
  let fixture: ComponentFixture<MapThreeLayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapThreeLayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapThreeLayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

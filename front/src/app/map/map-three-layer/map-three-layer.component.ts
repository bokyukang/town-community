import { HostListener, ViewChild, ElementRef, Component, OnInit, AfterViewInit } from '@angular/core';
import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { ModeEnum, MapService } from '@app/_services/map.service';
import {  EventService } from '@app/_services/event.service';
import { Event } from '@app/_models'
import { LatLng,  LatLngBounds } from 'leaflet';


@Component({
  selector: 'app-map-three-layer',
  templateUrl: './map-three-layer.component.html',
  styleUrls: ['./map-three-layer.component.css']
})
export class MapThreeLayerComponent implements OnInit, AfterViewInit {

    o_scene = null;
    o_camera = null;
    o_renderer = null;
    a_events:Event[] = [];
    s_mode:ModeEnum = ModeEnum.event;
    o_domContainer = null;
    a_markers = [];
    o_domRect = null;
    o_loader = null;
    m_models = {};
    f_camHeight = 10;

    @ViewChild('container') dom_container: ElementRef;
    constructor(
        private mapService:MapService,
        private eventService:EventService
    ) { }

    ngOnInit(): void {
    }

    ngAfterViewInit():void {
        const that = this;
        this.o_loader = new GLTFLoader();
        this.o_scene = new THREE.Scene();
        this.o_renderer = new THREE.WebGLRenderer({
            alpha: true
        });
        this.o_domContainer = this.dom_container.nativeElement
        //this.o_domContainer.appendChild(this.o_renderer.domElement);

        const f_camAngle = Math.atan(1/2) * 180 / Math.PI * 2;
        let o_domRect = this.o_domContainer.getBoundingClientRect();
        this.o_domRect = o_domRect;
        this.o_camera = new THREE.PerspectiveCamera( f_camAngle, o_domRect.width/o_domRect.height, 0.1, 1000 );
        this.o_camera.lookAt(0,0,0);
        //this.o_camera.position.z = this.f_camHeight;
        this.o_renderer.setSize(o_domRect.width, o_domRect.height);
        let light = new THREE.AmbientLight(0xffffff, 4);
        this.o_scene.add(light);
        light = new THREE.DirectionalLight(0xffffff, 1);
        light.position.set(50,0,50);
        this.o_scene.add(light);

        window.addEventListener('mouseout', this.onMouseOut);
        window.addEventListener('mouseleave', this.onMouseLeave);
        window.addEventListener('mouseclick', this.onMouseClick);
        //window.addEventListener('drag', this.onMouseDrag, false);

        this.loadResources();
    }

    async loadResources() {


        const loadModel = s_name => {
            return new Promise((resolve, reject) => {
                this.o_loader.load(
                    'assets/models/'+s_name+'.glb',
                    (gltf) => {
                        this.m_models[s_name] = gltf;
                        console.log('gltf loaded', s_name);
                        resolve(gltf);
                    }
                );
            })
        };


        await loadModel('tower');
        await loadModel('group');

        console.log('loading done');

        this.loaded();
    }

    loaded() {
        this.mapService.modeSubject.subscribe(
            s_mode => {
                this.updateMode(s_mode);
            });
        this.eventService.eventsOnMapObservable.subscribe(
            (a_events:Event[]) => {
                this.updateEventsMarkers(a_events);
            }
        );
        this.eventService.eventAdded.subscribe(
            (o_event:Event) => {
                this.a_events.push(o_event);
                const o_marker = this.createEvent(o_event);
                this.a_markers.push(o_marker);
                this.o_scene.add(o_marker);
                this.positionMarkers();
            }
        );

        //if (this.s_mode) {
        //    this.updateMode(this.s_mode);
        //}

        this.animate();
    }


    createCube(x,y) {
        const geometry = new THREE.BoxGeometry(.2,.2,.2);
        const material = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
        const cube = new THREE.Mesh(geometry, material);
        cube.position.x = x;
        cube.position.y = y;
        this.o_scene.add(cube);
    }

    getIntersects(x, y) {
        this.o_pointer.x = (x/ this.o_domRect.width) * 2 -1;
        this.o_pointer.y = - (y / this.o_domRect.height) * 2 + 1;

         this.o_raycaster.setFromCamera(this.o_pointer, this.o_camera);
        return this.o_raycaster.intersectObjects(this.o_scene.children);
    }

    getIntersect(x,y) {
        const a_intersects = this.getIntersects(x,y);
        if ( a_intersects.length > 0 ) {
            let o_object = a_intersects[0].object;
            while ( o_object.parent && o_object.userData != 'model') {
                o_object = o_object.parent;
            }

            return o_object;
        }
        return null;

    }

    _curHoverObject = null;
    MOUSEOVER_SCALE = 1.1;

    setCurHoverObject(o_object) {
        if ( o_object == this._curHoverObject ) 
            return;
        if (this._curHoverObject) {
            this._curHoverObject.scale.multiplyScalar(1/this.MOUSEOVER_SCALE);
        }
        this._curHoverObject = o_object;
        if (o_object)
            o_object.scale.multiplyScalar(this.MOUSEOVER_SCALE);
    }

    onMouseMove(event) {
        const o_object = this.getIntersect(event.originalEvent.layerX, event.originalEvent.layerY);
        this.setCurHoverObject(o_object);
    }

    onClick(event) {
        const o_object = this.getIntersect(event.originalEvent.layerX, event.originalEvent.layerY);
        if ( o_object ) {
            this.mapService.e_threeIntersects.next(o_object);
            this.eventService.eventClicked.next(o_object.event);
        }
    }

    onMouseOut(event) {
    }

    onMouseLeave(event) {
    }
    onMouseClick(event) {
    }

    onDrag(event) {
        this.positionMarkers();
        this.onMouseMove(event);
    }

    onZoomStart(event) {
        for ( let i=0;i < this.a_markers.length; ++i ) {
            this.o_scene.remove(this.a_markers[i]);
        }
    }
    animate():void {
        requestAnimationFrame( this.animate.bind(this));
        this.o_renderer.render(this.o_scene, this.o_camera);
    }

    updateMode(s_mode) {
        console.log('updating mode', s_mode);
        const cont = this.o_domContainer;
        if ( cont ) {
            const el = this.o_renderer.domElement;
            console.log('UPDATEMODE', s_mode, cont, el);
            this.s_mode = s_mode;
            if ( this.s_mode == ModeEnum.event && !cont.contains(el)) {
                cont.appendChild(el);
            }
            else if ( cont.contains(el) ) {
                cont.removeChild(el);
            }
            return true;
        }
        return false;
    }


    updateEventsMarkers(a_events:Event[]) {
        for ( let i=0;i < this.a_markers.length; ++i ) {
            this.o_scene.remove(this.a_markers[i]);
        }
        this.a_markers = [];

        this.a_events = a_events;
        for ( let i=0;i < a_events.length; ++i ) {
            const o_marker = this.createEvent(a_events[i]);
            this.a_markers.push(o_marker);
        }
        this.a_markers.forEach( o_marker => this.o_scene.add(o_marker) );
        this.positionMarkers();
    }

    createEvent(o_event:Event) {
        let o_model = null;
        if ( o_event.type === 'meetup' ) {
            o_model =  this.m_models['tower'].scene.clone();
        }
        else if ( o_event.type === 'group' ) {
            o_model =  this.m_models['group'].scene.clone();
        }
        o_model.userData = 'model';
        o_model.event = o_event;
        o_model.scale.set(0.1,0.1,0.1);
        return o_model;
    }

    positionMarkers( o_bounds:LatLngBounds = null ) {

        if ( o_bounds == null ) {
            o_bounds = this.mapService.boundsValue;
        }
        if ( o_bounds == null ) return;

        const f_mapHeight = o_bounds.getNorth() - o_bounds.getSouth();
        const f_mapWidth =( o_bounds.getEast() - o_bounds.getWest() );
        this.f_camHeight = f_mapHeight * 500;
        this.o_camera.position.z = this.f_camHeight;

        for ( let i=0;i < this.a_events.length; ++i ) {
            const o_event = this.a_events[i];
            const o_marker = this.a_markers[i];
            const f_xRate = (o_event.lng - o_bounds.getWest()) / f_mapWidth-.5;
            const f_yRate = (o_event.lat - o_bounds.getSouth()) / f_mapHeight-.5;

            //console.log('f_xRate ' + f_xRate + ', f_yRate '  + f_yRate);

            o_marker.position.set(
                this.f_camHeight * f_xRate * this.o_camera.aspect,
                this.f_camHeight * f_yRate,
                .1
            );
            //o_marker.updateMatrix();
            //o_marker.updateMatrixWorld(true);
            //o_marker.updateWorldMatrix(true, true);
        }
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        //console.log('onResize ' + event.target.innerWidth + ', ' + event.target.innerHeight);
        let o_domRect = this.o_domContainer.getBoundingClientRect();
        this.o_domRect = o_domRect;
        this.o_camera.aspect =  o_domRect.width/o_domRect.height;
        this.o_renderer.setSize(o_domRect.width, o_domRect.height);
        this.o_camera.updateProjectionMatrix();
    }

    onMapMove(o_bounds) {
        this.positionMarkers(o_bounds);
    }

    o_raycaster = new THREE.Raycaster();
    o_pointer = new THREE.Vector2();


}


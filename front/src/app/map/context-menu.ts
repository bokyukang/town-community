import { MenuEvent, ModeEnum, MapService } from '../_services/map.service';

export class ContextMenu {

    o_mapService:MapService = null;

    a_commonItems = [
    ];

    m_modeItems : { [key in ModeEnum]? : any } = {
        [ModeEnum.village] :
        [ 
            {
            },
        ],
        [ModeEnum.event] :
        [
            {
                label: '이곳에추가하기',
                items: [ 
                    {
                        label: '이벤트',
                        icon: 'pi pi-fw pi-pencil',
                        command: () => {
                            this.fire(MenuEvent.add_meetup);
                        }
                    },
                    {
                        label: '그룹',
                        icon: 'pi pi-fw pi-pencil',
                        command: () => {
                            this.fire(MenuEvent.add_group);
                        }
                    },
                ]
            },
        ],
    };

    constructor(mapService:MapService) {
        this.o_mapService = mapService;
    }

    fire(e_event: MenuEvent ):void {
        console.log('fire', e_event);
        this.o_mapService.e_menuEvent.next(e_event);
    }

    getModeItems(e_mode:ModeEnum) {
        return this.a_commonItems.concat(this.m_modeItems[e_mode]);
    }
}

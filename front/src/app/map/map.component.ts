import { ViewChild, Component, OnInit, AfterViewInit } from '@angular/core';
import { GroupViewComponent } from '@app/events/group-view/group-view.component';
import * as L from 'leaflet';
import { MenuEvent, ModeEnum, MapService } from '../_services/map.service';
import { icon, Marker } from 'leaflet';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { VillagePaneComponent } from '../village-pane/village-pane.component';
import { SignupComponent } from '../signup/signup.component';
import { SigninComponent } from '../signin/signin.component';
import { AccountDataService } from '../_services/account-data.service';
import { AccountService } from '../_services/account.service';
import { AlertService } from '../_services/alert.service';
import { MessageService } from 'primeng/api';
import { User, Event } from '@app/_models';
import { MapThreeLayerComponent } from '@app/map/map-three-layer/map-three-layer.component';
import { EventEnum } from '@app/_services/event.service';
//import boundariesData from '../../../../data/gadm/gadm40_KOR_2.geo.json';
import { ForumArticleListComponent } from '@app/forum/forum-article-list/forum-article-list.component';
import { MyProfileComponent } from '@app/profile/myprofile.component';
import { EventService } from '@app/_services/event.service';
import { CreateEventComponent } from '@app/events/create-event/create-event.component';
import { EventMode , VillageMode, Mode } from '@app/map/modes';
import { UtilService } from '@app/_services/util.service';
import { ContextMenu } from '@app/map/context-menu';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs';
import { EventViewComponent } from '@app/events/event-view/event-view.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  providers: [MessageService],
})
export class MapComponent implements OnInit, AfterViewInit{

    map = null;
    popup = null;
    geoJsonBorders = null;
    //labelsPane = null;
    user:User = null;
    a_tooltipDivs = [];
    s_mode:ModeEnum = ModeEnum.event;

	_a_menuItems = [];
	a_menuItemsDom = this.a_menuItems;


    private set a_menuItems(a_items) {
        this._a_menuItems = a_items;
        if ( this.dialogService.a_dialogs.length == 0 ) {
            this.a_menuItemsDom = this._a_menuItems;
        }
        else {
            this.a_menuItemsDom = null;
        }

    }

    o_clickMarker = null;
    o_mode:Mode = null;
    m_modes = {};
    s_boundaries = null;
    o_contextMenu:ContextMenu = null;

    @ViewChild('mapThreeLayer') o_mapThreeLayer:MapThreeLayerComponent;

    constructor(public mapService:MapService, 
                public dialogService:DialogManagerService,
                private alertService:AlertService,
                private messageService: MessageService,
                private accountService:AccountService,
                private accountDataService:AccountDataService,
                private eventService:EventService,
                private utilService:UtilService,
                private route:ActivatedRoute,
               ) { 
       this.o_clickMarker = L.marker();
       this.eventService.eventClicked.subscribe(
           this.eventClicked.bind(this));

        this.o_contextMenu = new ContextMenu(this.mapService);
        this.mapService.e_menuEvent.subscribe(this.onMenuEvent.bind(this));

        const s_popup = this.route.snapshot.paramMap.get('popup');

  }

  ngOnInit() {
      const s_command = this.route.snapshot.paramMap.get('command');
      console.log('command', s_command);
      this.processCommand(s_command);
      this.eventService.eventOpened.subscribe(
          o_event => {
            this.map.panTo(
                L.latLng(o_event.lat, o_event.lng));
          }
      );
  }

  processCommand(s_command) {
      if ( s_command === 'login' ) {
          this.dialogService.open(SigninComponent, {
              header: '로그인',
              width: '70%',
          }

         );
      }
  }

  init():void {

      let that = this;


      const iconRetinaUrl = 'assets/marker-icon-2x.png';
      const iconUrl = 'assets/marker-icon.png';
      const shadowUrl = 'assets/marker-shadow.png';
      const iconDefault = icon({
          iconRetinaUrl,
          iconUrl,
          shadowUrl,
          iconSize: [25, 41],
          iconAnchor: [12, 41],
          popupAnchor: [1, -34],
          tooltipAnchor: [16, -28],
          shadowSize: [41, 41]
      });
      Marker.prototype.options.icon = iconDefault;


      this.dialogService.o_dialogEvent.subscribe(
          s_event => {
              this.a_menuItems = this._a_menuItems; 
          }
      );

      this.utilService.loadJson('../../assets/geo-simplified.json')
        .subscribe(
            (s_json) => {
                this.s_boundaries = s_json;
                this.processBoundaries();
            }
        );

      this.accountDataService.user.subscribe(
          x => { 
              this.user = x;
              if ( this.user ) {
                this.processBoundaries();
              }

          }
      );
      this.mapService.modeSubject.subscribe(
          s_val => {
              this.s_mode = s_val;
              if ( this.o_mode ) {
                  this.o_mode.onExit();
              }
              this.o_mode = this.m_modes[this.s_mode];
              this.o_mode.onEnter();
              this.a_menuItems = this.o_contextMenu.getModeItems(this.s_mode);
          });


  }

  ngAfterViewInit():void {
      this.initMap();
      const eventMode = new EventMode(this.eventService, this.mapService, this.map, this.o_mapThreeLayer);
      const villageMode = new VillageMode(this.mapService)
      this.m_modes[ModeEnum.event] = eventMode;
      this.m_modes[ModeEnum.village] = villageMode;
      this.init();
      this.map.locate({setView: true, maxZoom: 16});

      this.mapService.b_geojsonBordersSubject.subscribe(
          b_set => {this.updateGeojson(); }
      );
  }

  updateGeojson() {
      if ( this.mapService.b_geojsonBorders) {

                  if ( this.geoJsonBorders ) {
                      this.geoJsonBorders.addTo(this.map);
                  }
              }
              else {
                  this.geoJsonBorders.removeFrom(this.map);
              }
          }

  private initMap(): void {
      this.map = L.map('map', {
          center: [ 37.5665, 126.9780 ],
          zoom: this.mapService.sub_zoomLevel.value,
          zoomControl : false
      });
      const zoomControl = L.control.zoom({position:'bottomright'});
      this.map.addControl(zoomControl);

      //this.labelsPane = this.map.createPane('labels');
      //this.labelsPane.style.zIndex = 650;
      //this.labelsPane.style.pointerEvents = 'none';

      const tiles = L.tileLayer('/hot/{z}/{x}/{y}.png', {
          maxZoom: 18,
          minZoom: 3,
          attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      });
      tiles.addTo(this.map); 

      this.map.on('locationfound', this.onLocationFound);
      this.map.on('locationerror', this.onLocationError);
      this.map.on('zoomend', this.onZoomEnd);
      this.map.on('zoomstart', this.onZoomStart);
      this.map.on('movestart', this.onMoveStart);
      this.map.on('mousemove', this.onMouseMove);
      this.map.on('moveend', this.onMoveEnd);
      this.map.on('move', this.onMove);
      this.map.on('contextmenu', this.onContextMenu);
      this.map.on('click', this.onClick);

      this.mapService.boundsValue = this.map.getBounds();

  }

  onMenuEvent(e_event:MenuEvent) {
      console.log('onMenuEvent', e_event);
      if (e_event == MenuEvent.add_meetup ) {
          this.dialogService.open(CreateEventComponent, {
              header: '모임 만들기',
              width: '70%',
              data:{
                  'type': EventEnum.meetup,
                  'latlng':this.o_clickMarker.getLatLng()
              },
          });
      }
      else if (e_event == MenuEvent.add_group ) {
          this.dialogService.open(CreateEventComponent, {
              header: '그룹 만들기',
              width: '70%',
              data:{
                  'type': EventEnum.group,
                  'latlng':this.o_clickMarker.getLatLng()
              },
          });
      }
  }

  onContextMenu= e => {
      //L.marker(e.latlng).addTo(this.map);
      this.o_clickMarker.setLatLng(e.latlng);
      if ( this.map.hasLayer(this.o_clickMarker) == false ) {
          this.o_clickMarker.addTo(this.map);

      }
  }
  onContextHide= e => {
      if ( this.map.hasLayer(this.o_clickMarker)  ) {
          this.o_clickMarker.removeFrom(this.map);
      }
  }
  onClick= e => {
      //L.marker(e.latlng).addTo(this.map);
      this.o_mode.onClick(e);
  }

  onMove= e=> {
      this.o_mode.onMove(e);
      this.o_mapThreeLayer.onMapMove(this.map.getBounds());
  }

  onMoveEnd = e => {
      this.mapService.boundsValue = this.map.getBounds();
      this.o_mode.onMoveEnd(e);
  }
  onMoveStart = e => {
  }
  onMouseMove = e => {
      this.o_mapThreeLayer.onMouseMove(e);
  }

  onZoomEnd = e => {
      const d_zoomLevel = this.map.getZoom();
      this.mapService.sub_zoomLevel.next(d_zoomLevel);
      console.log('zoomlevel ', d_zoomLevel);
      for ( let i=0; i < this.a_tooltipDivs.length; ++i ) {
          const o_div = this.a_tooltipDivs[i];
          o_div.style.fontSize = Math.round(d_zoomLevel * 1.3) + 'px';
      }
      this.o_mode.onZoomEnd(d_zoomLevel);
  }
  onZoomStart = e => {
      this.o_mapThreeLayer.onZoomStart(e);
      const d_zoomLevel = this.map.getZoom();
      this.o_mode.onZoomStart(d_zoomLevel);
  }

    onLocationFound = e => {
        var radius = e.accuracy;
        L.marker(e.latlng).addTo(this.map)
            .bindPopup('내 위치').openPopup();
        L.circle(e.latlng, radius).addTo(this.map);
        this.map.panTo( e.latlng);
    }

    onLocationError = e => {
    }


    processBoundaries() {

        let that = this;
        this.a_tooltipDivs = [];
        let o_userTown = null;

        function f_onEachFeature(o_feature, o_layer) {
            o_layer.on({
                mouseover: f_highlightFeature,
                mouseout: f_resetHighlight,
                click: f_click
            });
            let s_name = that.getName(o_feature.properties);
            let o_div = document.createElement('div');
            that.a_tooltipDivs.push(o_div);
            o_div.textContent = s_name;
            o_div.style.fontSize = '25px';
            o_div.style.color = '#333';
            o_div.style.fontWeight = 'bold';
            //o_div.className = 'my-map-label';
            o_layer.bindTooltip(o_div, {
                permanent: true,
                direction: 'center'
            });
            if (that.user && that.user.town_sig_id == o_feature.properties.SIG_CD) {
                o_userTown = o_layer;
            }
        }

        function f_style(o_feature) {
            if ( that.user ) {
            }
            if (that.user && that.user.town_sig_id == o_feature.properties.SIG_CD) {
                o_userTown = o_feature;
                return {
                    fillColor: '#FFFFFF',
                    weight: 7,
                    opacity: 1,
                    color: 'red',
                    dashArray: '3',
                    fillOpacity: 0.0
                };
            }
            return {
                fillColor: '#FC4E2A',
                weight: 3,
                opacity: .0,
                color: 'white',
                fillOpacity: 0.0
            };
        }
		function f_highlightFeature(e) {
			var layer = e.target;

			layer.setStyle({
                fillColor: 'silver',
				weight: 5,
                opacity: .7,
				color: '#fff',
				dashArray: '',
				fillOpacity: 0.6
			});

			if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
				layer.bringToFront();
			}
		}

        function f_resetHighlight(e) {
            that.geoJsonBorders.resetStyle(e.target);
        }
        function f_click(e) {
            that.map.fitBounds(e.target.getBounds());
            that.showArea(e.target);
        }

        if (this.geoJsonBorders) {
            this.geoJsonBorders.remove(this.map);
        }
        this.geoJsonBorders = L.geoJSON(this.s_boundaries, {
            pointToLayer: function(feature, latlng) {
                return null;
            },
            onEachFeature: f_onEachFeature,
            style: f_style
        });
        this.updateGeojson();
        if ( o_userTown ) {
            that.map.fitBounds(o_userTown.getBounds());
        }
    }

    getName(o_properties) {
         let s_name = o_properties.SIG_KOR_NM;
         return s_name;
    }

    showArea(o_area) {
        let o_properties = o_area.feature.properties;
        this.dialogService.open(VillagePaneComponent, {
            header: this.getName(o_properties),
            width: '70%',
            data:{
                'properties':o_properties
            },
        });
    }

    updateMode() {
    } 

    eventClicked(o_event:Event) {
        console.log(o_event);
        //this.map.panTo( L.latLng(o_event.lat, o_event.lng));
        this.eventService.openEvent(o_event);
    }

}

import { Component, OnInit } from '@angular/core';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { UserProfileComponent } from '@app/profile/user-profile.component';
import { FriendsService } from '@app/_services/friends.service';
import { Friend, FriendRequest } from '@app/_models/friend';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {

    a_requests:FriendRequest[] = [];
    a_friends:Friend[] = [];
    a_voters:number[] = [];
    a_myvoters:number[] = [];
  constructor(
        private dialogManagerService:DialogManagerService,
      private messageService: MessageService,
      private friendsService:FriendsService
  ) { }

  ngOnInit(): void {
      this.friendsService.a_requests.subscribe(
          a_requests => {
              this.a_requests = a_requests;
          }
      );
      this.friendsService.a_friends.subscribe(
          a_friends => {
              this.a_friends = a_friends;
          }
      );
      this.friendsService.a_myvotersSubject.subscribe(
          a_voters => {
              this.a_myvoters = a_voters;
          }
      );
      this.friendsService.a_votersSubject.subscribe(
          a_voters => {
              this.a_voters = a_voters;
          }
      );
  }

  acceptFriend(d_userId) {
      this.friendsService.acceptRequest(d_userId).subscribe(
         () => {
             this.messageService.add({severity: 'success', summary: '성공', detail:'친구 등록이 되었습니다.'}); 
         }
      );
  }

  denyFriend(d_userId) {
      this.friendsService.denyRequest(d_userId).subscribe(
         () => {
             this.messageService.add({severity: 'success', summary: '성공', detail:'친구 거절 완료하였습니다.'}); 
         }
      );
  }

  requestClick(d_userId) {

        this.dialogManagerService.open(UserProfileComponent, {
            header: '사용자 정보',
            width: '70%',
            data: {
                user_id: d_userId
            }
        });
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapComponent } from './map/map.component';
import { TestComponent } from '@app/test/test.component';
import { EmailValidatedComponent } from './email-validated/email-validated.component';
import { SigninComponent } from '@app/signin/signin.component';
import { Pwdreset2Component } from '@app/pwdreset2/pwdreset2.component';

const routes: Routes = [
    { path: '', redirectTo: '/map', pathMatch: 'full' },
    { path: 'map', component: MapComponent },
    { path: 'map/:command', component: MapComponent },
    { path: 'validated', component: EmailValidatedComponent },
    { path: 'pwdreset/:token', component: Pwdreset2Component },
    { path: 'test', component: TestComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

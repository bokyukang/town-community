import { Component, OnInit, OnDestroy, Input, HostListener, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import { ChatService } from '@app/_services/chat.service';
import { MyMessageService } from '@app/_services/my-message.service';
import { User } from '@app/_models/user';
import { SocketListener } from '@app/_services/websocket.service';
import { AccountDataService } from '@app/_services/account-data.service';
import { UserProfileComponent } from '@app/profile/user-profile.component';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { UtilService } from '@app/_services/util.service';
import { ImageComponent } from '@app/common/image/image.component';

class Entry {
    user_id: number;
    type:string;
    content: string; 
    written_at: string;
};

@Component({
  selector: 'app-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.css']
})
export class ChatRoomComponent implements OnInit, AfterViewInit, SocketListener {

    @Input() d_id = null;

    s_chatMsg:string = null;
    m_users: { [index: number ] : User } = {};
    a_entries: Entry[] = [];
    m_entryUsers: { [ index: number ] : User } = {};
    o_user = null;
    observer;
    a_imgs = [];
    o_privilege = null;

    @ViewChild('entries_container') o_entriesContainer;


    constructor(
          public config: DynamicDialogConfig ,
          private chatService:ChatService,
        private accountDataService:AccountDataService,
        private dialogManagerService:DialogManagerService,
        private utilService:UtilService,
        private myMessageService:MyMessageService,
    ) { 
        if ( this.config.data && this.config.data['id'] ) {
            this.d_id = this.config.data['id'];
            this.o_privilege = this.config.data['privilege'];
        }
        this.o_user = this.accountDataService.userValue;
    }

    ngOnInit(): void {
        this.chatService.subscribe(this.d_id, this);
        this.chatService.enter(this.d_id);
    }

    ngAfterViewInit():void {
    }

    scrollToBottom():void {
        const that = this;
        setTimeout( () => {
            let d_scrollHeight = that.o_entriesContainer.contentViewChild.nativeElement.scrollHeight;
            that.o_entriesContainer.scrollTop(d_scrollHeight);
        }, 200);
    }

    ngOnDestroy(): void {
        this.chatService.exit(this.d_id);
        this.chatService.unsubscribe(this.d_id, this);
    }

    @HostListener('window:unload', [ '$event' ])
    unloadHandler(event) {
        this.chatService.exit(this.d_id);
    }

    onContent(event):void {
        let s_chatMsg = this.s_chatMsg;
        /*
        for ( let i=0;i <  this.a_imgs.length; ++i ) {
            s_chatMsg += '<img src="' + this.a_imgs[i] + '" class="chat_content" (click)="chatImgClick($event)"/>';
        }
        */
        this.chatService.content(this.d_id, s_chatMsg, this.a_imgs);
        this.a_imgs = [];
        this.s_chatMsg = '';
    }

    onMessage(o_data):void {

        console.log(JSON.stringify(o_data));

        if ( o_data['chat_op'] == 'init' ) {
            this.m_users = Object.assign({}, this.m_users, o_data['users']);
            this.m_entryUsers = Object.assign({}, this.m_entryUsers,o_data['entry_users']);
            this.a_entries = o_data['entries'].reverse();
        }
        else if ( o_data['chat_op'] == 'content' ) {
            o_data['written_at'] = this.utilService.formatDate(new Date(Date.now()));
            o_data['type'] = 'msg';
            this.a_entries.push( o_data );
        }
        else if ( o_data['chat_op'] == 'enter' ) {
            let o_user = o_data['user'];
            o_data['type'] = 'info';
            this.m_users[o_user['id']] = o_user;
            this.m_entryUsers[o_user['id']] = o_user;
            this.a_entries.push( o_data );

            this.myMessageService.showInfo('입장', o_user['name'] + '님이 입장하였습니다.');
        }
        else if ( o_data['chat_op'] == 'exit' ) {
            let o_user = o_data['user'];
            o_data['type'] = 'info';
            delete this.m_users[o_user['id']];
            //this.m_entryUsers[o_user['id']] = o_user;
            this.a_entries.push( o_data );
        }
        this.scrollToBottom();
    }


    userClick(d_userId): void {
        this.dialogManagerService.open(UserProfileComponent, {
            header: this.config.header + ' > 사용자 정보',
            width: '70%',
            data: {
                user_id: d_userId
            }
        });
    }

    uploadImg():void {
        let input = document.createElement('input');
        input.type = 'file';
        input.accept = 'image/*';
        input.onchange = () => {
            let files = Array.from(input.files);
            this.chatService.uploadImg(files[0]).subscribe(
                (a_files) => {
                    for ( let i=0;i < a_files.length; ++i ) {
                        this.a_imgs.push(a_files[i]);
                    }
                }
            );
        };
        input.click();
    }
    chatImgClick(event, s_img) {
        this.dialogManagerService.open(ImageComponent, {
            header: this.config.header + ' > 이미지',
            width: '70%',
            data: {
                src: s_img
            }
        });
    }
}


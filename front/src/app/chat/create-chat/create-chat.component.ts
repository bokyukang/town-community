import { Component, OnInit} from '@angular/core';
import { ChatService } from '@app/_services/chat.service';
import { MessageService } from 'primeng/api';
import { ChatRoom } from '@app/_models/chat-room';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { DialogManagerService } from '@app/_services/dialog-manager.service';

@Component({
  selector: 'app-create-chat',
  templateUrl: './create-chat.component.html',
  styleUrls: ['./create-chat.component.css'],
  providers: [MessageService]
})
export class CreateChatComponent implements OnInit {

    s_title:string = '';
    s_backImg:string = null;
    s_description:string = '';
    d_typeId:number;
    s_type:string;

  constructor(
      public config:DynamicDialogConfig,
      private chatService:ChatService,
      private messageService:MessageService,
      private dialogService:DialogManagerService
  )
  { 
      this.d_typeId = this.config.data['type_id'];
      this.s_type = this.config.data['type'];
  }

  ngOnInit(): void {
  }


    uploadImg():void {
        let input = document.createElement('input');
        input.type = 'file';
        input.accept = 'image/*';
        input.onchange = () => {
            let files = Array.from(input.files);
            this.chatService.uploadImg(files[0]).subscribe(
                (a_files) => {
                    console.log('returned ' + JSON.stringify(a_files));
                    this.s_backImg = a_files[0];
                }
            );
        };
        input.click();
    }

    descriptionType(event) {
    }

    submit(event) {
        if ( this.s_title.trim() == '' ) {
            this.messageService.add({severity: 'fail', summary: '필수항목 누락', detail:'제목을 입력해 주세요.'});
            return;
        }
        let o_chatRoom:ChatRoom = {
            id: -1,
            title: this.s_title,
            description: this.s_description,
            img_url:this.s_backImg,
            type_id:this.d_typeId,
            type:this.s_type,
            user_id:null,
            tag:'public'
        };
        this.chatService.createChatRoom(o_chatRoom).subscribe(
            (o_result) => {
                console.log('returned');
                this.messageService.add({severity: 'success', summary: '개설 완료', detail:'대화방이 개설되었습니다.'});
                setTimeout(() => {
                    this.dialogService.close();
                }, 2000);
            }
        );
    }

}

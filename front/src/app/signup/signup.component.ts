import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AccountService } from '../_services/account.service';
import { MessageService } from 'primeng/api';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { UtilService } from '@app/_services/util.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [MessageService]
})
export class SignupComponent implements OnInit {

    emailForm:FormControl = new FormControl('');
    pwdForm:FormControl = new FormControl('');
    pwdRepeatForm:FormControl = new FormControl('');
    userNameForm:FormControl = new FormControl('');
    birthDateForm:FormControl = new FormControl('');
    signupForm:FormGroup = new FormGroup({
        email: this.emailForm,
        pwd: this.pwdForm,
        pwdRepeat : this.pwdRepeatForm,
        userName : this.userNameForm,
        birthDate : this.birthDateForm
    });
    o_defaultDate = new Date('1981-01-01');

  constructor(
      private accountService:AccountService,
      private messageService: MessageService,
      private dialogManagerService:DialogManagerService,
      private utilService:UtilService
  ) { }

  ngOnInit(): void {
  }

    onSubmit():void {
        console.log(this.signupForm);        
        if ( this.signupForm.status == 'VALID' ) {
            if ( this.pwdForm.value != this.pwdRepeatForm.value ) {
                         this.messageService.add({severity: 'error', summary: '실패', detail:'비밀번호 확인란이 일치하지 않습니다.'}); 
                return;
            }
            let s_email = this.signupForm.controls.email.value;
            let s_pwd = this.signupForm.controls.pwd.value;
            let s_userName = this.userNameForm.value;
            let o_birthDate = this.birthDateForm.value;
            let s_birthDate = null;
            if ( o_birthDate ) {
                s_birthDate = this.utilService.formatDate(o_birthDate);
            }
            this.accountService.signUp(s_email, s_pwd, s_userName, s_birthDate).subscribe(
                {
                    next: (data) => {
                        console.log('signup result ' + data);
                         this.messageService.add({severity: 'success', summary: '성공', detail:'계정이 생성되었습니다. 이메일 인증해 주세요.'}); 
                    },
                    error: error => {
                        console.log('error' + JSON.stringify(error));
                        if ( error.error['statusText'] == 'DUPLICATE' ) {
                             this.messageService.add({severity: 'error', summary: '실패', detail:'동일한 email로 이미 가입 돼 있습니다.'}); 
                        }
                        else {
                             this.messageService.add({severity: 'error', summary: '실패', detail:'회원가입에 실패하였습니다.'}); 
                        }
                    }

                });
        }
    }

    onCancel():void {
        this.dialogManagerService.close();
    }
}

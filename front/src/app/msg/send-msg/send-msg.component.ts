import { AfterViewInit, ViewChild, ElementRef, Component, OnInit } from '@angular/core';
import { tap, map, catchError } from 'rxjs/operators';
import { ForumService } from '@app/_services/forum.service';
import { Observable, throwError } from 'rxjs';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import { FormGroup, FormControl } from '@angular/forms';
import ImageUploader from 'quill-image-uploader';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { MessageService } from 'primeng/api';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import * as Quill  from 'quill';
import { NoteMessageService } from '@app/_services/note-message.service';

@Component({
  selector: 'app-send-msg',
  templateUrl: './send-msg.component.html',
  styleUrls: ['./send-msg.component.css'],
  providers: [MessageService]
})
export class SendMsgComponent implements OnInit {

    @ViewChild('editor')
    editor:ElementRef;
    private quill:any;
    private d_receiverId:number = -1;

  constructor(
      private dialogManagerService:DialogManagerService,
        private ref: DynamicDialogRef,
      public config: DynamicDialogConfig,
      private noteMessageService:NoteMessageService,
      private messageService:MessageService,
  ) { 
      this.d_receiverId = this.config.data['receiver_id'];
  }

    ngOnInit(): void {

        Quill.register('modules/imageUploader', ImageUploader);

        this.quill = new Quill('#editor', 
            {
                modules: { 
                    'toolbar': '#toolbar-container',
                    imageUploader: {
                        upload: (o_file) => {
                            console.log('image file ' + o_file.name);
                            return new Promise((resolve, reject) => {
                                console.log('inside promise');
                                this.noteMessageService.uploadImg(o_file)
                                    .pipe(
                                        catchError(
                                            error => {
                                                console.log('error' + error);
                                                reject(error);
                                                return throwError('erro');
                                            })
                                    ).subscribe(
                                            (s_file) => {
                                                console.log('return ' + s_file);
                                                resolve( s_file);
                                                return s_file;
                                            }
                                    );
                            });
                        }
                    }
                },

                theme: 'snow'
            },
        );
        this.load();


    }

    load():void {
    }

    onSubmit():void {
        let o_editor = this.editor.nativeElement.getElementsByClassName("ql-editor")[0];
        let s_content = o_editor.innerHTML;
        let s_text = o_editor.innerText || o_editor.textContent;
        if (s_text.trim() == '' ) {
             this.messageService.add({severity: 'fail', summary: '실패', detail:'내용을 입력 해 주세요.'});
            return;
        }
        let that = this;
        if ( s_content ) {
            this.noteMessageService.sendMsg(s_content, this.d_receiverId)
                .subscribe(
                () => {
                     this.messageService.add({severity: 'success', summary: '성공', detail:'쪽지가 보내졌습니다.'});
                     setTimeout( () => {
                         this.ref.close();
                     }, 1000);

                    //this.dialogManagerService.replace( ForumContentViewComponent, {
                    //    data: {
                    //        id: d_id,
                    //        forum_list_id:this.d_forum_list_id
                    //    },
                    //    header:that.config.header,
                    //    width:'70%'
                    //});
                }
            )
            ;
        }
    }


}

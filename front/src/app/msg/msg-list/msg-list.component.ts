import { Component, OnInit } from '@angular/core';
import { NoteMessageService } from '@app/_services/note-message.service';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { NoteMessage } from '@app/_models/note-message';
import { MsgViewComponent } from '@app/msg/msg-view/msg-view.component';

@Component({
  selector: 'app-msg-list',
  templateUrl: './msg-list.component.html',
  styleUrls: ['./msg-list.component.css']
})
export class MsgListComponent implements OnInit {

    a_msgs:NoteMessage[] = null;
    d_totalRecords = 0;
    d_rows = 3;

  constructor(
      private noteMessageService:NoteMessageService,
      private dialogService:DialogManagerService,
      public config: DynamicDialogConfig
  ) { }

  ngOnInit(): void {
      this.load(this.d_rows, 0);
  }
  load(d_rows, d_page) {
      this.noteMessageService.getMsgs(d_rows, d_page)
          .subscribe(
              (m_result) => {
                  const a_msgs =m_result['msgs'];
                  this.d_totalRecords = m_result['totalRecords'];
                  console.log(JSON.stringify(m_result));
                  this.a_msgs = a_msgs;
                  for ( let i=0;i < a_msgs.length; ++i ) {
                      a_msgs[i].content = a_msgs[i].content.replace(/<\/?[^>]+(>|$)/g, "");
                  }
              });
  }

  onPageChange(event):void {
      console.log('onpagechange'  + JSON.stringify(event));
      this.load(event.rows, event.page);
  }
  openMsg(o_msg) {
      this.dialogService.open(MsgViewComponent, {
          header: this.config.header + ' > 쪽지 보기',
          width: '70%',
          data: {
              id:o_msg.id,
              mark_opened:true
          }
      }).subscribe(
          (s_cmd) => {
              if ( s_cmd == 'close' ) {
                  o_msg.opened=1;
              }

          });
  }
}

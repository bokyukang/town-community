import { Component, OnInit } from '@angular/core';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { UserProfileComponent } from '@app/profile/user-profile.component';
import {DynamicDialogRef} from 'primeng/dynamicdialog';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { NoteMessage } from '@app/_models/note-message';
import { NoteMessageService } from '@app/_services/note-message.service';

@Component({
  selector: 'app-msg-view',
  templateUrl: './msg-view.component.html',
  styleUrls: ['./msg-view.component.css']
})
export class MsgViewComponent implements OnInit {

    d_id:number = 0;
    o_msg:NoteMessage = null;
    b_markOpened = false;

  constructor(
      private noteMessageService:NoteMessageService,
        private dialogManagerService:DialogManagerService,
        public config: DynamicDialogConfig,
  ) { 
      this.d_id = this.config.data['id'];
      this.b_markOpened = this.config.data['mark_opened'];
  }

  ngOnInit(): void {
      this.noteMessageService.getMsg(this.d_id, this.b_markOpened)
        .subscribe(
            o_msg => {
                this.o_msg = o_msg;
                console.log(JSON.stringify(o_msg));
            });
  }

  userClick(d_userId) {
        this.dialogManagerService.open(UserProfileComponent, {
            header: this.config.header + ' > 사용자 정보',
            width: '70%',
            data: {
                user_id: d_userId
            }
        });
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VillagePaneComponent } from './village-pane.component';

describe('VillagePaneComponent', () => {
  let component: VillagePaneComponent;
  let fixture: ComponentFixture<VillagePaneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VillagePaneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VillagePaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ViewChild, Input, Component, OnInit } from '@angular/core';
import { SigninComponent } from '@app/signin/signin.component';
import { UtilService } from '@app/_services/util.service';
import { MyMessageService } from '@app/_services/my-message.service';
import { User } from '@app/_models';
import { TownService } from '../_services/town.service';
import { AccountService } from '../_services/account.service';
import { AccountDataService } from '@app/_services/account-data.service';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { ForumArticleListComponent } from '@app/forum/forum-article-list/forum-article-list.component';
import { ChatRoomComponent } from '@app/chat/chat-room/chat-room.component';
import { PrimeNGConfig } from 'primeng/api';
import { CreateChatComponent } from '@app/chat/create-chat/create-chat.component';
import { CreateForumComponent } from '@app/forum/create-forum/create-forum.component';
import { ChatRoom} from '@app/_models/chat-room';
import { Forum } from '@app/_models/forum';
import { MenuItem } from 'primeng/api';
import { ConfirmComponent } from '@app/common/confirm/confirm.component';

@Component({
  selector: 'app-village-pane',
  templateUrl: './village-pane.component.html',
  styleUrls: ['./village-pane.component.scss'],
  providers: []
})
export class VillagePaneComponent implements OnInit {

    d_defaultChatNo:number = -1;
    d_defaultForumNo:number = -1;
    d_townId:number;
    a_townForums:Forum[];
    a_townChats:ChatRoom[];
    b_sidebarDisplay=false;
    s_townName = '';
    b_showHelp = true;

    menuItems: MenuItem[];

    @ViewChild('appConfirm')
    c_confirmComponent:ConfirmComponent;

  constructor(private townService:TownService,
      public config:DynamicDialogConfig,
      private dialogService:DialogManagerService,
      private myMessageService:MyMessageService,
      private accountService:AccountService,
      private accountDataService:AccountDataService,
      private primengConfig:PrimeNGConfig,
      private utilService:UtilService,
  ) { }

  ngOnInit(): void {

      this.menuItems = [
          {
              icon: 'pi pi-fw pi-home',
              label: '이 지역 주민임',
              command:() => {
                  this.iLiveHere();
              }
          },
          {
              label: '대화방 만들기',
              command:() => {
                  this.createChatClick(null);
              }
          },
          {
              label: '게시판 만들기',
              command:() => {
                  this.createForumClick(null);
              }
          }
      ];

      let o_properties = this.config.data['properties'];
      console.log('properties : ' + JSON.stringify(o_properties));
      this.s_townName = o_properties['SIG_KOR_NM'];
      let that = this;
      this.townService.onLoad(o_properties).subscribe( (data) => {
          console.log('load success ' + JSON.stringify(data));
          that.d_townId = data['town_id'];
          that.a_townForums = data['town_forums'];
          that.a_townChats = data['town_chats'];

          console.log('town_forums' + JSON.stringify(that.a_townForums));

          for ( let i=0;i < this.a_townChats.length; ++i ) {
              console.log(JSON.stringify(this.a_townChats[i]));
              if (this.a_townChats[i].type == 'default' ) {
                  this.d_defaultChatNo = this.a_townChats[i].id;
              }
          }

          for ( let i=0;i < this.a_townForums.length; ++i ) {
              console.log(JSON.stringify(this.a_townForums[i]));
              if (this.a_townForums[i].type == 'default' ) {
                  this.d_defaultForumNo = this.a_townForums[i].id;
              }
          }

      });
      this.primengConfig.ripple = true;
      this.b_sidebarDisplay=true;
  }

  chatClick(d_roomId): void {
      let d_townId = this.d_townId;
      console.log('townid ' + d_townId);

      if ( this.utilService.checkLogin( this.dialogService ) ) {
          this.dialogService.open(ChatRoomComponent, {
              header: this.config.header + ' > 채팅방',
              width: '70%',
              data: {
                  id:d_roomId,
                  privilege : {
                      write: true
                  }
              }
          });
      }
  }

  createForumClick(event):void {
      if ( this.utilService.checkLogin( this.dialogService ) ) {
          this.dialogService.open(CreateForumComponent, {
              header: this.config.header + ' > 게시판 개설',
              width: '70%',
              data: {
                  type_id:this.d_townId,
                  type:'town',
              }
          }).subscribe( (s_cmd) => {
              console.log('createforum ' + s_cmd);
              if ( s_cmd == 'close' ) {
                  this.ngOnInit();
              }
          });
      }
  }
  createChatClick(event):void {
      if ( this.utilService.checkLogin( this.dialogService ) ) {
      this.dialogService.open(CreateChatComponent, {
          header: this.config.header + ' > 채팅방 개설',
          width: '70%',
          data: {
              type_id:this.d_townId,
              type:'town',
          }
      }).subscribe( (s_cmd) => {
          console.log('createchat ' + s_cmd);
          if ( s_cmd == 'close' ) {
              this.ngOnInit();
          }
      });
      }
  }

  forumClick(forumId): void {
      let d_townId = this.d_townId;
      console.log('townid ' + d_townId);
      this.dialogService.open(ForumArticleListComponent, {
          header: this.config.header + ' > 기본게시판',
          width: '70%',
          data: {
              id:forumId
          }
      });
  }

  iLiveHere():void {
      if ( this.utilService.checkLogin( this.dialogService ) ) {
          let o_user:User = this.accountDataService.userValue;
          if ( o_user && o_user.town_id == this.d_townId ) {
              this.myMessageService.showErr( '실패', '이미 이 타운으로 설정되어 있습니다.');
              return;
          }
          this.c_confirmComponent.decrUserVariable('town-change', 2, '타운 변경 시 2일 후에 변경 할 수 있습니다. 변경 하시겠 습니까?').subscribe((b_result) => {
              if ( b_result ) {
              this.townService.onILiveHere(this.d_townId).subscribe( 
                  {
                      next: (data) => {
                          console.log('success');
                          this.myMessageService.showSuccess('성공', '성공적으로 지역 설정을 하였습니다');
                          this.accountService.reloadUser();
                      },
                      error: error => {
                          console.log('ilivehere error ' + error);
                      }
                  });
              }
          });
      }
  }

}

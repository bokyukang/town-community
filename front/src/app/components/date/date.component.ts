import { Input, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css']
})
export class DateComponent implements OnInit {

    @Input()
    s_date = null;

    @Input()
    d_date = null;

    @Input()
    o_date = null;

  constructor() { }

  ngOnInit(): void {
      console.log('date', this.s_date);
      if ( this.s_date ) {
          this.o_date = new Date(this.s_date);
      }
      else if ( this.d_date ) {
          this.o_date = new Date(this.d_date);
      }
  }

}

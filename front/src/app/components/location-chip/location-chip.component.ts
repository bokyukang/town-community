import { Input, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-location-chip',
  templateUrl: './location-chip.component.html',
  styleUrls: ['./location-chip.component.css']
})
export class LocationChipComponent implements OnInit {

    @Input()
    f_lat = null;
    @Input()
    f_lng = null;

  constructor() { }

  ngOnInit(): void {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationChipComponent } from './location-chip.component';

describe('LocationChipComponent', () => {
  let component: LocationChipComponent;
  let fixture: ComponentFixture<LocationChipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationChipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationChipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

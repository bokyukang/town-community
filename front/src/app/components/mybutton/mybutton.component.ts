import { EventEmitter, Output, Input, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mybutton',
  templateUrl: './mybutton.component.html',
  styleUrls: ['./mybutton.component.css']
})
export class MybuttonComponent implements OnInit {

    @Input()
    s_label

    @Output()
    on_click = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  onClick(event) {
      this.on_click.emit(event);
  }

}

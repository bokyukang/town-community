import {Input, Component, OnInit } from '@angular/core';
import { UserProfileComponent } from '@app/profile/user-profile.component';
import { DialogManagerService } from '@app/_services/dialog-manager.service';
import { User } from '@app/_models/user';
import { AccountService } from '@app/_services/account.service';

@Component({
  selector: 'app-user-chip',
  templateUrl: './user-chip.component.html',
  styleUrls: ['./user-chip.component.css']
})
export class UserChipComponent implements OnInit {

    @Input()
    o_user:User;

    @Input()
    s_header = '';

    @Input()
    s_userName = '';

    @Input()
    s_userPicUrl = '';

    @Input()
    d_userId = -1;

  constructor(
        private dialogManagerService:DialogManagerService,
        private accountService:AccountService,
  ) { }

  ngOnInit(): void {
      if ( this.o_user == null ) {
          this.accountService.getUser(this.d_userId)
            .subscribe(
                o_user => {
                    this.o_user = o_user;
                }
            );
      }
  }

    userClick(): void {
        const d_userId = this.o_user? this.o_user.id : this.d_userId;
        console.log('userClic', d_userId, this.o_user);
        this.dialogManagerService.open(UserProfileComponent, {
            header: this.s_header + ' > 사용자 정보',
            width: '70%',
            data: {
                user_id: d_userId,
            }
        });
    }

}

import { Input, Component, OnInit } from '@angular/core';
import { UserProfileComponent } from '@app/profile/user-profile.component';
import { User } from '@app/_models';
import { AccountService } from '@app/_services/account.service';
import { DialogManagerService } from '@app/_services/dialog-manager.service';

@Component({
  selector: 'app-participant',
  templateUrl: './participant.component.html',
  styleUrls: ['./participant.component.css']
})
export class ParticipantComponent implements OnInit {

    @Input()
    d_userId:number = null;
    
    @Input()
    o_user:User = null;

  constructor(private accountService:AccountService,
      private dialogService:DialogManagerService,
  ) { 
  }

  ngOnInit(): void {
      if ( this.o_user == null && this.d_userId) {
          this.accountService.getUser(this.d_userId)
            .subscribe( o_user => this.o_user = o_user );
      }
  }

  onClick(event): void {
      const d_userId = this.d_userId ? this.d_userId : this.o_user.id;
        this.dialogService.open(UserProfileComponent, {
            header: ' > 사용자 정보',
            width: '70%',
            data: {
                user_id: d_userId,
            }
        });
  }

}

import {  Output, Input, Component, OnInit , AfterViewInit} from '@angular/core';


@Component({
  selector: 'app-room-item',
  templateUrl: './room-item.component.html',
  styleUrls: ['./room-item.component.css']
})
export class RoomItemComponent implements OnInit, AfterViewInit {

    @Input()
    s_imgUrl:string = null;

    @Input()
    s_title:string = null;

    @Input()
    s_color = 'bg-green';

    @Input()
    s_tooltip = null;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit():void {
  }

}

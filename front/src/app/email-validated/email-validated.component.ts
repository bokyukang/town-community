import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-email-validated',
  templateUrl: './email-validated.component.html',
  styleUrls: ['./email-validated.component.css']
})
export class EmailValidatedComponent implements OnInit {

  constructor(
      private router:Router
  ) { }

  ngOnInit(): void {
  }

  gotoSite(event): void {
      this.router.navigate(['/map/login']);
  }

}

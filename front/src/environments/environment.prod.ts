export const environment = {
    production: true,
    ip: 'town.okaybokyu.com',
    socket_url: 'wss://town.okaybokyu.com/socket',
}

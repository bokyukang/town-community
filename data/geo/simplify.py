import json
import math

f = open('geo-full2-utf8.json', encoding='utf-8')

data = json.load(f)

MIN_DIST = 0.008
AREA_OVER = 0.002
AREA_UNDER = 0.0001

coordSet = set()
d_hashCount = 0
d_orgCount =0
d_newCount = 0
d_areaUnder = 0
d_areaOver = 0
d_coordsCount = 0

def f_min_dist(f_area):
    global MIN_DIST
    return max(math.log(f_area / AREA_OVER, 1.7) * MIN_DIST, MIN_DIST)

def get_area(corners):
    n = len(corners) # of corners
    area = 0.0
    for i in range(n):
        j = (i + 1) % n
        area += corners[i][0] * corners[j][1]
        area -= corners[j][0] * corners[i][1]
    area = abs(area) / 2.0
    return area

def dist(c1, c2):
    return math.sqrt(pow((c1[0] - c2[0]), 2) + pow((c1[1] - c2[1]), 2))

def getHash(c):
    return c[0] + c[1]

def f_round(c):
    c[0] = round(c[0], 4)
    c[1] = round(c[1], 4)
    return c

def process(coordsGroup):

    global d_hashCount
    global d_orgCount
    global d_newCount
    global d_areaUnder
    global d_areaOver
    global d_coordsCount

    a_removeFrom = []
    for coords in coordsGroup:
        d_coordsCount += 1
        f_area = get_area(coords)
        if  f_area < AREA_UNDER:
            d_areaUnder += 1
            a_removeFrom.append(coords)
            continue
        if f_area > AREA_OVER:
            d_areaOver += 1
        
        newCoords = [f_round(coords[0])]
        lastCoord = f_round(coords[0])
        for i in range(1, len(coords)):
            coord = f_round(coords[i])
            d_dist = dist(lastCoord, coord)
            f_hash = getHash(coord)
            if d_dist > f_min_dist(f_area) or f_hash in coordSet:
                newCoords.append(coord)
                lastCoord = coord
                if f_hash not in coordSet:
                    coordSet.add(f_hash)
                else:
                    d_hashCount += 1

        d_orgCount += len(coords)
        coords.clear()
        coords.extend(newCoords)
        d_newCount += len(coords)
    for item in a_removeFrom:
        coordsGroup.remove(item)

for feature in data['features']:
    if feature['geometry']['type'] == 'MultiPolygon':
        for group in feature['geometry']['coordinates']:
            process(group)
    else:
        process(feature['geometry']['coordinates'])


with open('geo-simplified.json', 'w', encoding='utf-8') as f:
    json.dump(data, f, ensure_ascii=False)

print('hash count', d_hashCount)
print('org count', d_orgCount)
print('new count', d_newCount)
print('coords count', d_coordsCount)
print('area under count', d_areaUnder)
print('area over count', d_areaOver)


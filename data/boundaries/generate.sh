rm simple_geojson* simplified.*
mapshaper -i *.shp encoding=euc-kr -simplify weighted percentage=0.5% keep-shapes -o format=shapefile force simplified.shp
ogr2ogr -f GeoJSON -t_srs crs:84 -lco COORDINATE_PRECISION=3 "simple_geojson.json" "simplified.shp"
iconv -f euc-kr -t utf-8 simple_geojson.json -o simple_geojson_utf8.json
cp simple_geojson_utf8.json ../../front/src/app/

